# Science Perl Committee

We are the official democratically-elected organization for oversight of science in Perl.

## Vision Statement

Our vision is a world where Perl is the dominant language in science, technology, engineering, and mathematics.

## Mission Statement

Our mission is to develop and promote ethical scientific software in Perl.
