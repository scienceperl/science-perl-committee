# Science Perl Committee
# Bylaws

## June 25th, 2024

* Article 1 - Name & Charter

    * The name of the committee shall be the Science Perl Committee.  The committee shall be chartered and sponsored by the Public Enrichment and Robotics Laboratories (PERL) nonprofit organization, and shall operate under the direct auspices of PERL exclusively.

* Article 2 - Object

    * The objective of the committee, and the reason behind its creation, shall be to implement our Vision and Mission Statements as specified below.  This objective includes the pursuit of major ongoing scientific Perl accomplishments by our committee.

    * Vision Statement
        * Our vision is a world where Perl is the dominant language in science, technology, engineering, and mathematics.

    * Mission Statement
        * Our mission is to develop and promote ethical scientific software in Perl.

* Article 3 - Members

    * Qualifications for membership include nomination by an existing member, approval by the officers, and simple majority vote in a quorum.

    * Perl community members are invited to apply for membership in the committee.

* Article 4 - Officers

    * The committee is governed by democratically-elected leadership, and all major decisions are decided by the vote of a quorum which shall be defined as no less than five members in good standing.

    * The Chairman is the lead officer of the organization, and is responsible for presiding over all meetings and activities whenever possible.  (All references to "Chairman" may be replaced by "Chairwoman" as appropriate.)

    * The 1st Vice Chairman is responsible for assisting the Chairman in his duties, and presiding in his absence.

    * The 2nd Vice Chairman is responsible for assisting the 1st Vice Chairman in his duties, and presiding in his absence and that of the Chairman.

    * The Secretary is responsible for keeping and distributing the meeting minutes as appropriate.

    * The Treasurer is responsible for keeping and disbursing the committee's funds as appropriate.

* Article 5 - Meetings

    * Meetings are held every other Thursday from 1:00pm to 1:45pm central time zone. The first meeting date of 2024 is 1/11.

    * Hackathons are held every 6 weeks on the Wednesday night before a meeting from 7:00pm to 9:00pm central time zone (unless otherwise stated).

    * Committee meetings have openly-available agendas and minutes for the public record.

* Article 6 - Executive Board

    * The Executive Board shall be comprised of the Chairman, 1st Vice Chairman, and 2nd Vice Chairman.

    * The Executive Board shall be empowered to act in accordance with and on behalf of the committee, as directed during or necessitated by committee meeting business.

* Article 7 - Subcommittees

    * The Editorial Review Subcommittee shall be responsible for reviewing, requesting revisions of, approving, and rejecting all academic papers, posters, and other scientific content submitted to the committee.

    * The Proceedings Subcommittee shall be responsible for compilation, formatting, and publication of the Science Perl Journal proceedings of the Science Perl Track.

    * The Marketing Subcommittee shall be responsible for promoting the committee and engendering goodwill among the greater Perl and scientific communities.

    * The Fundraising Subcommittee shall be responsible for securing donations to finance official committee projects and operations.

* Article 8 - Parliamentary Authority

    * The committee shall be governed according to Robert's Rules of Order 12th Edition (or newer), Robert's Rules of Order Newly Revised In Brief 3rd Edition (or newer), or Robert's Rules of Order QuickStudy Reference Guide 2011 Edition (or newer), in that order.

* Article 9 - Amendment of Bylaws

    * The committee shall consider all amendments or other modifications to the bylaws during the annual convention.

    * All bylaws modifications must be approved by a two-thirds super-majority vote of a quorum.
