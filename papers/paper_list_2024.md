# Science Perl Committee
# Paper List 2024

## Confirmed

* none


## In Review

* none


## Submitted

* Will Braswell (Navi AI)
* Brett Estrade (OpenMP; hurricane prediction, software forecasts storm surge caused by hurricane, numerical model is Fortran, remaining is 75% Perl & 25% Bash)
* George Baugh (mathematical conjectures recently disproven) [ Brett NEED CHECK IN ]
* Dr. Christos Argyropoulos (kidney medicine & surgery; Nextflow; TensorFlow Recommender)  [ WB NEED CONTACT FOR 2025 ]
* Dr. Adam Russell (computer scientist, leads group United Health Group subsidiary OptumAI, developing new products to bring AI & ML to admin workers)  [ WB CONTACTED 20240404 ] 
* Jan Stepanek
* Antanas Vaitkus (crystallography)  [ WB CONTACTED 20240404 ]
* Manickam Thanneermalai (cross-compiling Perl for specialized hardware microcontrollers & microprocessors) [ WB CONTACTED 20240404 ]

## Proposed

* Drew O'Neil (chemometrics)  [ WB CONTACTED 20240404 ]
* Zakariyya Mughal (TensorFlow; PDL; connecting Perl & R)  [ Brett NEED ASK IF STILL INTERESTED ]
* John Napiorkowski [ WB CONTACTED 20240404 ]
* Ricardo Filipo, learning mathematics with Perl [ WB CONTACTED 20240404 ]
* Dr. Marc Perry (BioPerl, bioniformatics & genomics)
    * Lessons Learned from Scientific Data Wrangling in Perl 2025, example of "lessons learned" paper  [ Brett NEED CONTACT 2025 ]
        * Torsten Seemann, Ten Recommendations for Creating Usable Bioinformatics Command Line Software
        * https://gigascience.biomedcentral.com/articles/10.1186/2047-217X-2-15
* Minas Polychronidis, medical data for healthcare organizations [ WB CONTACTED 20240404 ]
* Joshua Day, IO::Async & TCP, also Intellexer  [ Brett NEED CONTACT ]
* Jakub Pawlowski, Perl AI integration w/ .NET & Mono???  ( no microphone, unable to confirm )  [ WB CONTACTED 20240404 ]
* CY Fung, wants to review quantum computing module(s) on CPAN, including Quantum::Superpositions  [ WB CONTACTED 20240404 ]
* Alex Gomez, "Asymptoptic Analysis for Jigsaw Puzzles", National University of Mexico Autonomous, CS master's degree, should finish in ~1 month, contains ~200 lines of Perl code, World Federation of Competitive Jigsaw Puzzles, 500 pieces solved in 30-40 mins, why are some puzzles faster to solve than others?  [ WB CONTACTED 20240404 ]
* Mike Flannigan, Geographic Information Systems (GIS) 2025, Perl cartography & mapping, create GeoJSON & plotting maps of automobile tracks & hiking tracks  [ Brett NEED CONTACT ]
* Lewis Johanne, grew up on a farm, idea is to have agricultural commodity prices available in real time, we have it for gold and stock market but not for food commodities such as tomatoes and lettuce etc  [ WB CONTACTED 20240404 ]

## Invited

* Robbie Hatley (astronomy, needs astronomy partner, possibly Dimitrios Kechagias???)  [ WB CONTACTED 20240404 ]
* Duong Vu (data engineering)  [ WB CONTACTED 20240404 ]
* Dimitrios Kechagias (astronomy)  [ Brett NEED CONTACT ]
* Dr. Ruey-Cherng Yu (dental medicine & surgery; Raspberry Pi)  [ WB CONTACTED 20240404 ]
* Bartosz Jarzyna (cryptocurrency)  [ WB CONTACTED 20240404 ]
* Chris Prather 2025  [ Brett NEED CONTACT ]
* Justin Kelly, space science & outer space 2025 [ WB CONTACTED 20240404 ]
* Joshua Oliva, scientific breakthroughs, health topics, physics 2025 [ WB CONTACTED 20240404 ]


## Need Invite

* Mickey Macten, online security and multimedia, how does streaming video work?
* Tyler Bird, just learning about bioinformatics


## Delayed


## Declined

* none
