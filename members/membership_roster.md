# Science Perl Committee
# Membership Roster

## Officers

* Brett Estrade, Chairman, elected 8/24/23, re-elected 6/25/24
* Dr. Andrew O'Neil, 1st Vice Chairman, elected 6/25/24
* Will Braswell, 2nd Vice Chairman, elected 8/24/23, re-elected 6/25/24


## Current

* Scott Adams
* Achmad Yusri Afandi
* Nikolay Arbatov
* Christos Argyropoulos, MD, PhD
* Jeb Bacon
* Kai Baker
* Myo Bandar
* Jay Battikha
* George Baugh
* Krasimir Berov (AKA Красимир Беров)
* Tyler Bird
* Wil Blake
* Will Braswell, Co-Founder
* Anne Brown
* Julian Brown
* Guido Brugnara
* Nick Busigin
* Ryan Cabaneles
* Alex Casamassima
* Mitchko Christophe
* Egon Choroba
* Smart Code
* Bar Bojan Coders
* Andrass Ziska Davidsen
* Joshua Day
* Jacob Dev
* Roger Doss
* Boyd Duffee
* Brett Estrade, Co-Founder
* Ricardo Filipo
* Mike Flannigan
* Krzysztof Flis
* Cheok Yin "CY" Fung
* Kieren Goldfish
* Alex Gomez
* Leo Grapendaal
* Henrique Gusmao
* Sangyong Gwak
* Dean Hamstead, Co-Founder
* Zahirul Haq
* Dan Harris
* Robbie Hatley
* Glenn Holmgren
* Stefan Hornburg
* Nedzad Hrnjica
* Keith Hsieh
* Tracey Jacoby
* Anuj Jain
* Witold Janczar
* Bartosz Jarzyna
* Sumedha Jeewan
* Eric Jeskey
* Lewis Johanne
* Lee Johnson
* John D Jones III
* Dimitris Kechagias
* Justin Kelly
* Vlado Keselj
* Krzysztof Kielak
* Hansom Kim
* John Kirk
* Venkatesh Kumar
* Dirk Linder
* Abraham Llave (AKA إبراهيم مفتاح)
* Wang Lun
* Marco Aurelio Macae
* Mickey Macten
* Reinier Maliepaard (AKA Barbe Vivien)
* Rohit Manjrekar
* John Martinez
* Daniel Mera
* Paul Millard
* Venkataramana Mokkapati
* Ali Moradi
* Daryl Morning
* Su Mu
* Zakariyya Mughal, Co-Founder
* Fernando Munguia
* Hisham Musa
* Dharmendra Namdeo
* John Napiorkowski, Co-Founder
* Connie New
* Joshua C Oliva
* Drew O'Neil
* John P
* Sarswati Kumar Pandey
* Sibananda Pani
* Vaibhav Patil (AKA वैभव पाटील)
* James Pattie
* Jakub Pawlowski
* Rui Pereira
* Marc Perry, PhD
* Jason Philbin
* Minas Polychronidis
* Josh Rabinowitz
* Yulian Radev
* Brian Marquez Inca Roca
* Steve Rogerson
* Laurent Rosenfeld
* Stephane Roux
* Adam Russell, PhD
* Robert Ryley
* Siddharth Satyapriya
* Rajan Shah
* Prasanna Silva
* Saman Senju Simorangkir
* Douglas Spore
* Benjamin Paul Suntrup
* Manickam Thanneermalai
* Thuyein Thet
* Jovan Trujillo
* Antony Use
* Antanas Vaitkus
* Duong Vu
* Somanath Wagh
* Bo Cheng Wei
* Jeff Yoak
* Ruey-Cherng Yu
* Francisco Zarabozo
* Emanuele Zeppieri
* Mohammed Zia


## Invited

* Haider Ali
* Packy Anderson
* John Calley
* Camila Chavarro
* Rick Croote
* Rob De la Cruz
* Erki Ferenc
* Sven Gelbhaar
* Ankita Ghadge-More
* Robert Grimes
* Roy Hubbard
* Abraham Key
* Rajesh Kumar Mallah
* Kusa Manohar
* Anatol Mazur
* Jennifer Meneghin
* Daniel Mita
* Su Mu
* Saiful Islam Musafir
* Emil Perhinschi
* Vugar Bakhshaliyev (AKA Rituda Perl)
* Matthew Price
* Mohd Rashid
* Hila Sapirstein
* Mike Schienle
* Daniel Sherer
* Ludovic Tolhurst-Cleaver
* Brandon Wood


## Need Invite

* Gene Boggs (AKA ology)
* Prashant Deore
* Bruce Gray
* Gerard Onerom


## Declined

* none
