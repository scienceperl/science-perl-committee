# Science Perl Committee
# Meeting Agenda & Minutes

## March 7th, 2024
## Sixteenth Official Meeting

* Opening, 1:05pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Zakariyya Mughal (BioPerl, PDL, previously biomedical imaging, currently computational toxicology)
    * Will Braswell (computer scientist, creator of Navi AI)
    * John Napiorkowski (maintainer of Catalyst)
    * Marc Perry, PhD (biologist, used to have wet lab, now doing bioinformatics & genomics using Perl 15 years, Senior Bioinformatician at Genomics Institute of UC Santa Cruz)
    * Jakub Pawlowski (wants to integrate Perl AI with .NET & Mono)
    * Mike Flannigan (interested in math & science in general, also astronomy & space program)
    * Antanas Vaitkus (Lithuania, background in bioinformatics, working in crystallography for 10 years, open database software in Perl to collect all small molecule structures)
    * Lewis Johanne
    * Jovan Truillo
    * John D Jones III
    * Mickey Macten

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 2/22/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Marc Perry to be accepted as corrected, voted unanimously

* Officer Reports
    * Brett reports on the following:
        * none

    * Zaki reports on the following:
        * none

    * Will reports on the following:
        * Published perlcommunity.org/science, see below for details
        * Hackathon last night, see below for details

* Old Business

    * TPC 2024 Science Track

        * General Next Steps  [ NOT UPDATED ON 3/7/24 ]
            * Fix OJS e-mail de-authentication issue
            * Create timeline of each step of the paper submission / review / approval process
            * Publish Science CFP on perlcommunity.org/science
            * Distribute Science CFP via e-mail, Facebook, Twitter, IRC, etc
            * Start receiving & reviewing paper submissions in OJS
            * Finish reviewing papers in OJS 
            * Send TPC Planning Committee our list of approved science papers ASAP, so they can review & coordinate with other track scheduling

        * Remote Presentations
            * none

        * Call For Papers  [ NOT UPDATED ON 3/7/24 ]
            * TPF sent our Science CFP as part of main CFP
            * We want abstracts to be submitted by April 5th
            * We want final papers & posters to be submitted by May 15th
            * Brett created infographic for CFP
                * https://www.facebook.com/groups/scienceperl/posts/917497506595730
            * Time to go full steam ahead on promoting Science CFP, list of distribution channels:
                * E-mail from Brett, people who responded to the Science Track Survey [ DONE ]
                * E-mail from Brett, universities (LSU, UT, BYU) [ DONE ]
                * E-mail from Brett, computational oceanographers [ DONE ]
                * E-mail from Brett, Perl Monger leaders
                * E-mail from Zaki, BioPerl & PDL mailing lists
                * Social media from Will, Facebook [ DONE ]
                    * https://www.facebook.com/groups/perlprogrammers/posts/7567632603269462
                    * https://www.facebook.com/wnbjr/posts/pfbid023mjrkJaE7yY3EhdF9S81237vxaPdM3d1cJEjAk7BofcbpsEn3ceBh8scrk3siR4El
                    * https://www.facebook.com/perlcommunity/posts/pfbid0Vu2kdYRLgJJhWwSEyEu9uuDzkDKczE3FdgJU4yibepBdnPvf6hy9YJgoxPrpVaTWl
                * Social media from Will, Twitter [ DONE ]
                    * https://twitter.com/perlcommunity/status/1760842963125403904
                * Social media from Zaki, Mastodon
                * Social media from Zaki, LinkedIn
                * IRC.perl.org MAGnet chat from Zaki
                * IRC.libera chat from Zaki
                * TPRF Slack chat from Brett
                * HPC Social Discord chat from Brett
                * Perl Discord chat from Brett
                * Blogs.perl.org blogs from Brett [ DONE ]
                    * https://blogs.perl.org/users/oodler_577/2024/02/repost-tprc-2024-call-for-papers-is-now-open.html
                * Dev.to blogs from Zaki
                * Reddit website from Zaki
                * Perl Monks website from Brett
                * TPF website from Brett [ DONE ]
                    * https://news.perlfoundation.org/post/cfp2024
                * Perl Planet aggregator from Brett
                * Perl Weekly Newsletter aggregator from Brett [ DONE ]
                    * https://perlweekly.com/archive/656.html

        * Editorial Review Subcommittee
            * none

        * Proceedings
            * none

        * Websites
            * Homepage
                * perlcommunity.org/science
                * Now published, need to present to committee & officially release
            * OJS Subdomain
                * science.perlcommunity.org/spj
                * Need e-mail deauthentication fix

        * Paper Ideas
            * Alex Gomez, "Asymptoptic Analysis for Jigsaw Puzzles", National University of Mexico Autonomous, CS master's degree, should finish in ~1 month, contains ~200 lines of Perl code, World Federation of Competitive Jigsaw Puzzles, 500 pieces solved in 30-40 mins, why are some puzzles faster to solve than others?  [ NOT DISCUSSED ON 3/7/24 ]

    * RoboPerl Nonprofit
        * none

    * Science Perl Hackathon
        * Every 6 weeks
        * Hackathon last night 3/6/24  [ NOT DISCUSSED ON 3/7/24 ]
            * Focus on science CFP promotion & discussion
            * Everyone had a good time, we talked about the Science Perl Track & Science Perl Journal with interested Perl programmers
            * Receive new paper idea from Alex Gomez
        * Next event 4/17/24, 7-9pm Central time zone, theme is TBD???  [ NOT DISCUSSED ON 3/7/24 ]

* New Business

    * Minimum Quorum Requirements
        * Chairman Brett Estrade calls for an official vote to determine the minimum number of committee members required for a voting quorum
        * Vote for minimum quorum of 5 people, 11 people, or 50% of total committee membership
        * Will Braswell gave recommendation for 5 option only
        * John Napiorkowski recommends 5 as well, except major decisions can go into FB group for official poll
        * Zaki Mughal tallies votes
            * Antanas Vaitkus votes for 5
            * Jakub Pawlowski votes for 5
            * Lewis Johanne votes for 5
            * Jovan Trujillo votes for 5
            * John D Jones votes for 5
            * Marc Perry votes for 5
            * Mickey Macten votes for 5
            * Mike Flannigan votes for 5
            * John Napiorkowski votes for 5
            * Will Braswell votes for 5
            * Zaki Mughal votes for 5
            * Brett Estrade abstains from voting due to his personal choice as chairman not to vote unless needed for a tie breaker
        * Vote results are 100% unanimous in favor of 5 minimum committee members required for an official voting quorum from now on

* Closing, 1:36pm
