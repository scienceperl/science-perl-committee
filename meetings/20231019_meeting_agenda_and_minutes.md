# Science Perl Committee
# Meeting Agenda & Minutes

## October 19th, 2023
## Sixth Official Meeting

* Opening, 1:03pm

* Attendance & Introductions
    * Brett Estrade (high-speed computing)
    * Zakariyya Mughal (biomedical imaging, BioPerl, PDL, toxicology research)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Yussuf Arifin AKA Zahirul Haq (from Malaysia, new to coding, wants to learn more about Perl)
    * Jakub Pawlowski (wants to integrate Perl AI with .NET & Mono)
    * Minas Polychronidis (in Greece, owns think.gr software company)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 10/5/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Zaki Mughal to be accepted as read, voted unanimously

* Old Business

    * TPC 2024 Science Track

        * TPF & TPC Science Track Proposal report
            * We did not attend the 10/18 TPC planning meeting, nothing to report

        * Call For Papers
            * Everyone should keep working on paper ideas
            * CFP will be opened when TPF gives official approval
            * Should be shortly after survey is complete

        * Survey
            * TPF wants to know if we have enough support from the Perl community to make the Science Track successful
            * We reviewed the entire survey, made some minor corrections, added Ricardo's question "How are you using Perl?" question from last meeting
            * Brett will now submit the final Google survey to Dean & Amber at TPF, to be sent out to Perl community

        * Editorial Review Subcommittee
            * This is an Ad Hoc subcommittee, not a standing subcommittee
            * Subcommittee will not be officially formed until TPF approves Science Track
            * Science Perl Committee officers will automatically be on this subcommittee for the time being
            * If we get a lot of paper submissions, then we will need other committee members to be on this subcommittee
            * We would like to have minimum 5 people on this subcommittee
            * When a subcommittee member submits a paper, they will recuse themself from reviewing their own paper
            * We don't tell paper submitters the list of subcommittee members who are reviewing their paper, we keep it anonymous to avoid bribes or favoritism etc

        * Proceedings
            * no comments

        * Websites
            * Will Braswell needs to update perlcommunity.org

        * Paper ideas
            * Brett Estrade, software forecasts storm surge caused by hurricane, numerical model is Fortran, remaining is 75% Perl & 25% Bash
            * Minas Polychronidis, medical data for healthcare organizations

    * Science Perl Hackathon
        * Every 6 weeks
        * Next event 11/1/23, 7-9pm Central time zone, theme is CFP & Paper Reviews

* New Business
    * none

* Closing, 1:46pm
