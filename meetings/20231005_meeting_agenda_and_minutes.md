# Science Perl Committee
# Meeting Agenda & Minutes

## October 5th, 2023
## Fifth Official Meeting

* Opening, 1:05pm

* Attendance & Introductions
    * Will Braswell (computer scientist, creator of Navi AI)
    * Zakariyya Mughal (biomedical imaging, BioPerl, PDL, toxicology research)
    * Marc Perry, PhD (biologist, used to have wet lab, now doing bioinformatics & genomics using Perl 15 years, Senior Bioinformatician at Genomics Institute of UC Santa Cruz)
    * Yussuf Arifin AKA Zahirul Haq (from Asia, new to coding, wants to learn more about Perl)
    * Ricardo Filipo AKA "Monsenhor Filipo" (Perl programmer currently in Orlando, also from Rio de Janeiro, Brazil; working with CalTech in LA, project from NASA for database machine, writing code for data interpreter & web services & AI & science, Algebraic Query Language AQL)
    * Jakub Pawlowski (wants to integrate Perl AI with .NET & Mono)
    * Justin Kelly (web developer from Dublin, wants to get back into Perl, started w/ Perl CGI years ago, loves Perl)

* Announcements
    * Chairman Brett Estrade unavailable, 1st Vice Chairman Zaki Mughal presiding
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 9/21/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Zaki Mughal to be accepted as read, voted unanimously

* Old Business

    * TPC 2024 Science Track

        * TPF & TPC Science Track Proposal report
            * Zaki gave a report about the TPC planning meeting
            * TPF wants to develop a survey, Brett has created a draft, Zaki & Brett & Will need to meet to fix typos etc.
                * https://github.com/oodler577/tprc-2024-science-track-survey/blob/main/survey.md
                * Ricardo says we need to add a question "How are you using Perl currently?  In science?"
            * TPF wants Science Perl presenters with posters to give Lightning Talks w/out a gong
            * TPF prefers in-person presenters, remote talks can be accepted if a physical proxy person in available on-site
            * TPF wants people to have 6 months to work on papers, so we need to open CFP as soon as new survey receives responses
            * TPF's next TPC planning meeting is October 18th, 4pm Central time zone
            * Is the Science Track going to be 1 day, multiple days, or every day of TPC?

        * Call For Papers
            * Open as soon as TPF survey is complete, hopefully w/in next 1 month?
            * https://gist.github.com/zmughal/5047eb588e8b3cb6de9b12864e99e97b

        * Editorial Review Committee
            * no comments

        * Proceedings
            * no comments

        * Websites
            * Will Braswell needs to update perlcommunity.org

        * Paper ideas
            * Ricardo Filipo, learning mathematics with Perl
            * Marc Perry, lessons learned from scientific data wrangling in Perl
                * Example of "lessons learned" paper
                    * Torsten Seemann, Ten Recommendations for Creating Usable Bioinformatics Command Line Software
                    * https://gigascience.biomedcentral.com/articles/10.1186/2047-217X-2-15

    * Science Perl Hackathon
        * Every 6 weeks
        * Next event 11/1/23, 7-9pm Central time zone, theme is CFP & Paper Reviews

* New Business
    * none

* Closing, 1:47pm
