# Science Perl Committee
# Meeting Agenda & Minutes

## December 14th, 2023
## Tenth Official Meeting

* Opening, 1:04pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Zakariyya Mughal (BioPerl, PDL, previously biomedical imaging, currently computational toxicology)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Joshua Day (industrial tooling)

* Announcements
    * Chairman Brett Estrade requests 1st Vice Chairman Zaki Mughal to preside
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 11/30/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Zaki Mughal to be accepted as read, voted unanimously

* Officer Reports
    * Brett reports on the following:
        * Peter from TPF says they will approve the Science Track, number of days will be based on number of science papers approved
        * PaperCall.io will have 2 additional talk types, science paper & science poster, those will be sent to us for entry into OJS
        * TPF will release CFP next week, we will wait until after to make our announcements, full speed ahead after that

    * Zaki reports on the following:
        * Hackathon & OJS software
            * We successfully set up a local copy of OJS
            * Work flow: set up journal manager & journal editor, outside person submits paper, assign paper to be reviewed, outside person reviews paper
            * Need to check if e-mail functionality works
            * OJS can publish website version of proceedings, each conference gets published as an issue of the Science Perl Proceedings
            * Need specific reviewer guidelines
            * For papers, need investigate LaTex document class, so submitters have a template to use for their papers
            * For posters, need to either chop up or reformat in order to fit into printed proceedings, or put abstract plus QR code for full version online
                * For online proceedings, just use PDF

    * Will reports on RoboPerl nonprofit

* Old Business

    * TPC 2024 Science Track

        * TPF & TPC Science Track Proposal report
            * TPF approval is now as official as we expect to get, see officer report from Brett above

        * Call For Papers
            * CFP will be opened in a week or two, probably after the holidays
            * Will will contact people from committees
            * Zaki will contact people from survey
                * Zaki will talk to Brett about export survey results into ODS or CSV, to be committed to private GitLab repo for posterity
            * Josh will contact CPAN authors

        * Editorial Review Subcommittee
            * Start with committee officers only, expand to include people with approved papers
            * Use OJS to ask new reviewers which topics they are willing and able to review

        * Proceedings
            * none

        * Websites
            * Will Braswell will update perlcommunity.org after TPF publicly announces the Science Track
            * science.perl.org if possible, generic one-page landing point with link to other pertient Science Perl sites
            * perlcommunity.org/science, more in-depth multi-page website
            * science.perlcommunity.org, OJS web UI

        * Paper Ideas
            * Joshua Day, IO::Async & TCP

    * Science Perl Hackathon
        * Every 6 weeks
        * Next event 1/24/24, 7-9pm Central time zone, theme is Website Development

* New Business

    * RoboPerl Nonprofit
        * Public Enrichment and Robotics Laboratories AKA RoboPerl
        * RoboPerl.org placeholder website
        * Donations now accepted and initial TensorFlow grants underway

* Closing, 1:57pm
