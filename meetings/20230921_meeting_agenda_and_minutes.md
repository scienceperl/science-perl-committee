# Science Perl Committee
# Meeting Agenda & Minutes

## September 21st, 2023
## Fourth Official Meeting

* Opening, 1:02pm

* Attendance & Introductions
    * Will Braswell (computer scientist, creator of Navi AI)
    * Brett Estrade (independent Perl contractor, helping scientists in HPC, oceanography & storm surge modeling using finite element 2-D modeling)
    * Zakariyya Mughal (biomedical imaging, BioPerl, PDL, toxicology research)
    * Adam Russell, PhD (computer scientist, leads group United Health Group subsidiary OptumAI, developing new products to bring AI & ML to admin workers)
    * Antanas Vaitkus (Lithuania, background in bioinformatics, working in crystallography for 10 years, open database software in Perl to collect all small molecule structures)
    * Justin Kelly (web developer from Dublin, wants to get back into Perl, started w/ Perl CGI years ago, loves Perl)
    * Marc Perry, PhD (biologist, used to have wet lab, now doing bioinformatics & genomics using Perl 15 years, Senior Bioinformatician at Genomics Institute of UC Santa Cruz)
    * Robbie Hatley (EE & computer programming in Perl for 8 years)
    * Venkataramana Mokkapati AKA "Ramana" (works for large semiconductor company, build CPAN-like repository of EE components, elliptical curve cryptography)
    * Yussuf Arifin AKA Zahirul Haq (from Asia, new to coding, wants to learn more about Perl)
    * Jakub Pawlowski (wants to integrate Perl AI with .NET & Mono)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 9/7/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Marc Perry to be accepted as read, voted unanimously

* Old Business

    * TPC 2024 3rd Track
        * Call For Papers, can't be published until Science Track is approved by TPF, no change
            * https://gist.github.com/zmughal/5047eb588e8b3cb6de9b12864e99e97b
        * Science Perl Hackathon
            * Report by Zaki
                * TPC Science Track Proposal created during first Science Perl Hackathon last night 9/20
            * Create regular event
                * Repeat every 6 weeks
                * Next event 11/1/23, 7-9pm Central time zone, theme is Science Track Planning?
                * Moved by Will Braswell, seconded by Zaki Mughal, voted unanimously
        * TPF report
            * Brett gave an update, TPF is getting their ducks in a row, they have made a call for people who want to participate in conference planning, they are very interested in exploring Science Track w/ us, Zaki is working on draft CFP, posted on TPF Slack, got positive feedback w/ lots of questions, generally well received, Brett & Zaki both requested to join TPC planning team, planning meeting on Sept 27th
        * Editorial Review Committee
            * We require a paper or poster, just an abstract is not enough
        * Proceedings
            * Adam Russell says if you publish a paper in Perl Science Proceedings, then you may not be able publish it in a big fancy journal; there is no shortage of academic publishers who might like to help publish the Science Perl Proceedings
            * Will Braswell says a pre-print can be published in arXiv and the full paper can go into the Perl Science Proceedings
            * Marc Perry says bioRxiv and medRxiv also work the same as arXiv, you don't even need to tell them what journal you'll publish it in after them
            * arXiv sites give us DOI (digital object identifier), we also need to be able to assign our own DOI numbers to the papers published in our Perl Science Proceedings
            * Zaki will ask the JOSS how they handle assigning DOIs
        * Websites
            * Will Braswell needs to update perlcommunity.org
        * Paper ideas
            * no comments
        * TPC Science Track Proposal
            * Google document reviewed by Zaki
            * https://docs.google.com/document/d/1rV5iKYrYOWmD9_1QgDtkx1FQczmIMABCGLTTCnXETms/edit 
            * Benefits to Perl community
            * Benefits to Perl scientists
            * Attracting new Perl users
            * Papers & posters should be copyright in author's name, published under Creative Commons

* New Business
    * none

* Closing, 1:55pm
