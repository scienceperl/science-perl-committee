# Science Perl Committee
# Meeting Agenda & Minutes

## April 18th, 2024
## Nineteenth Official Meeting

* Opening, 1:06pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Manickam Thanneermalai (EE design engineer, designing chips, Perl automates work in semiconductor industry, Synopsis & Cadence tool vendors have AI algorithms, building ARM-based system-on-chip for automotive AI)
    * Adam Russell, PhD (computer scientist, leads group United Health Group subsidiary OptumAI, developing new products to bring AI & ML to admin workers)
    * Drew O'Neil (AKA Andrew, uses Perl AI for chemometrics)
    * Mickey Macten (online security and streaming multimedia)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 4/4/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Brett Estrade to be accepted as modified, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of two officers only, not full voting quorum

* Officer Reports
    * Brett reports on the following:
        * Hackathon last night, approved all papers, good that we have some for 2025 as well
        * Original CFP ended 4/5, TPC extended their CFP through 4/20 and asked if we would do the same, we agreed

    * Will reports on the following:
        * Hackathon (see below)
        * Paper idea list (see below)

* Old Business

    * TPC 2024 Science Track

        * General Next Steps
            * Fix OJS e-mail de-authentication issue
            * Create timeline of each step of the paper submission / review / approval process  [ DONE ]
            * Publish Science CFP on perlcommunity.org/science   [ DONE ]
            * Distribute Science CFP via e-mail, Facebook, Twitter, TPF announcements, etc   [ DONE ]
            * Distribute Science CFP via IRC (Magnet #perl & #pdl, Libera #perl), Reddit, and remaining avenues [ DONE ]
            * Promoting & requesting paper submissions from those on the Paper Idea list [ DONE ]
            * Start receiving & reviewing paper submissions in OJS [ ONGOING ]
            * Finish reviewing papers in OJS 
            * Send TPC Planning Committee our list of approved science papers ASAP, so they can review & coordinate with other track scheduling

        * Remote Presentations
            * NEED GET TPF APPROVAL for Dr. Christos remote talk 2024

        * Paper Submission Timeline
            * Abstracts submitted by April 5th
            * Abstracts approved or declined by April 15th
            * Extended CFP, abstracts submitted by April 20th
            * Extended CFP, abstracts approved or declined by April 30th
            * 1st drafts of papers & posters submitted by May 15th
            * 1st drafts feedback sent back to submitters by May 31st
            * Final drafts submitted by June 7th
            * Final drafts approved or declined by June 15th

        * Call For Papers
            * Brett created blogs.perl.org post about extended CFP deadlines, got distributed on Perl Weekly Newsletter
            * NEED UPDATE OJS & WEBSITE, submitters must create their presentation slides to match their paper or poster content

        * Editorial Review Subcommittee
            * NEED BYLAW, delay any formal motion to officially create a subcommittee until in-person convention
            * Currently accepting input from all committee members, only committee officers may vote to approve/reject, need full quorum for tiebreakers
            * Unanimously approved all submissions so far as of 4/17 Hackathon

        * Proceedings Subcommittee
            * NEED BYLAW, same as Editorial Review Subcommittee above
            * Current authors will be offered a printed copy of SPJ at cost
            * Non-authors will be offered a printed copy of SPJ at a fair market mark-up as part of our annual fundraising, to finance printing next year
            * Those who sign up for Science Perl mailing list will get free PDF copy of SPJ 
            * Put PDF copy of SPJ online for free download, with prominent donate button for anyone who is not a student, works based on the honor system
            * We should use the RoboPerl nonprofit bank accounts as needed, can open a separate Science Perl account if necessary

        * Websites  [ NO CHANGE 20240418 ]
            * Homepage
                * perlcommunity.org/science
                * NEED UPDATE WEBSITE WITH PAPER SUBMISSION TIMELINE DATES
            * OJS Subdomain
                * science.perlcommunity.org/spj
                * NEED E-MAIL DEAUTHENTICATION FIX

        * Mailing List  [ NO CHANGE 20240418 ]
            * One-way announcements only
            * Sendy, self-hosted by Brett
            * Request all committee members to register
                * perlcommunity.org/science#mailing_list

        * Paper Ideas
            * Drew J. O'Neil, chemometrics and Innospectrum/TI spectrometer control, identify counterfeit vs genuine medicine, uses Perl Tk, use Perl to call Octave

    * RoboPerl Nonprofit
        * none

    * Science Perl Hackathon
        * Hackathon last night 4/17, went great, had full quorum present, reviewed & updated & approved every single submission so far
        * Every 6 weeks
        * Next event 5/29/24, 7-9pm Central time zone, theme is Final Paper Approval

* New Business

    * 1st Annual Perl Committee Convention
        * During TPC in Las Vegas, time & date TBD
        * Need to create & ratify bylaws
        * Celebrate at after-party dinner
        * NEED ASK TPC for BoF room reservation

* Closing, 2:08pm
