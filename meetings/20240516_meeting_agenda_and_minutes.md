# Science Perl Committee
# Meeting Agenda & Minutes

## May 16th, 2024
## Twenty-First Official Meeting

* Opening, 1:06pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Manickam Thanneermalai (EE design engineer, Perl automates work in semiconductor industry, building ARM-based system-on-chip for automotive AI)
    * Robbie Hatley (EE & computer programming in Perl)
    * Drew O'Neil (AKA Andrew, uses Perl AI for chemometrics)
    * Joshua Oliva (interested in scientific breakthroughs, physics, health topics)
    * Josh Rabinowitz
    * John Napiorkowski (maintainer of Catalyst)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 5/2/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Robbie Hatley to be accepted as corrected, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of one officer only, not full voting quorum

* Officer Reports
    * Brett reports on the following:
        * E-mailed draft Science Track schedule to paper authors today, need authors to approve and register before finalizing with TPF
        * Waiting for Todd to tell us who registered with the Science Track speaker code
        * Paper deadline extended from May 15th to May 20th
        * Convention probably Tuesday night, Conference banquet is already set for Wednesday night
        * Will use Latex default article class for papers, will be 1 column instead of 2 columns for now

    * Will reports on the following:
        * Sent a message to Dr. Adam telling him that he can attend for only one day for his talk on 6/25 if that helps his work schedule
        * Jan Štěpánek (legal name) is the same person as Egon Choroba (commonly-used name)

* Old Business

    * TPC 2024 Science Track

        * General Next Steps  [ NO CHANGE 20240516 ]
            * Fix OJS e-mail de-authentication issue
            * Create timeline of each step of the paper submission / review / approval process  [ DONE ]
            * Publish Science CFP on perlcommunity.org/science   [ DONE ]
            * Distribute Science CFP via e-mail, Facebook, Twitter, TPF announcements, etc   [ DONE ]
            * Distribute Science CFP via IRC (Magnet #perl & #pdl, Libera #perl), Reddit, and remaining avenues [ DONE ]
            * Promoting & requesting paper submissions from those on the Paper Idea list [ DONE ]
            * Start receiving & reviewing paper submissions in OJS [ ONGOING ]
            * Finish reviewing papers in OJS 
            * Send TPC Planning Committee our list of approved science papers ASAP, so they can review & coordinate with other track scheduling

        * Remote Presentations
            * STILL NEED TPF APPROVAL for Dr. Christos remote talk 2024
            * Will not attempt to get TPF approval for Dr. O'Neil remote talk 2024, will keep that as emergency backup plan only

        * Paper Submission Timeline
            * Abstracts submitted by April 5th  [ DONE ]
            * Abstracts approved or declined by April 15th  [ DONE ]
            * Extended CFP, abstracts submitted by April 20th  [ DONE ]
            * Extended CFP, abstracts approved or declined by April 30th  [ DONE ]
            * Extended 1st drafts of papers & posters submitted by May 20th
            * 1st drafts feedback sent back to submitters by May 31st
            * Final drafts submitted by June 7th
            * Final drafts approved or declined by June 15th

        * Call For Papers  [ NO CHANGE 20240516 ]
            * CFP now closed

        * Editorial Review Subcommittee
            * NEED BYLAW, delay any formal motion to officially create a subcommittee until in-person convention
            * NEED UPDATE OJS & WEBSITE, submitters must create their presentation slides to match their paper or poster content
            * NEED OJS ADMIN ACCOUNT for Dr. Adam Russell to review papers
            * Accepted input from all committee members, only committee officers may vote to approve/reject, need full quorum for tiebreakers
            * Unanimously approved all paper submissions, final count is 9 papers
            * Manickam's Paper
                * We briefly reviewed Manickam's current submission
                * The tone is that of a technical overview, written in a somewhat friendly and casual manner
                * NEED to delete 2nd paper submission in OJS, upload updated PDF into 1st submission
                * NEED to copy all references to the end of the paper

        * Proceedings Subcommittee
            * NEED CREATE PLAN FOR PRINTING BEFORE TPC
            * NEED BYLAW, same as Editorial Review Subcommittee above
            * Current authors will be offered a printed copy of SPJ at cost
            * Non-authors will be offered a printed copy of SPJ at a fair market mark-up as part of our annual fundraising, to finance printing next year
            * Those who sign up for Science Perl mailing list will get free PDF copy of SPJ 
            * Put PDF copy of SPJ online for free download, with prominent donate button for anyone who is not a student, works based on the honor system
            * Funding
                * We should use the RoboPerl nonprofit bank accounts as needed, can open a separate Science Perl account if necessary
                * Dr. Christos is requesting a refund from his paid ticket, will use the free registration code and donate the refund money to the Science Committee for use in publishing our first issue of the Science Perl Journal
            * Latex Formatting
                * Per Dr. O'Neil's recommendation, we will use the "Vancouver" style reference formatting using numbers (not "Harvard" style)
                    * \bibliographystyle{plain}

        * Websites  [ NO CHANGE 20240516 ]
            * Homepage
                * perlcommunity.org/science
                * NEED UPDATE WEBSITE WITH PAPER SUBMISSION TIMELINE DATES
            * OJS Subdomain
                * science.perlcommunity.org/spj
                * NEED E-MAIL DEAUTHENTICATION FIX

        * Mailing List  [ NO CHANGE 20240516 ]
            * One-way announcements only
            * Sendy, self-hosted by Brett
            * Request all committee members to register
                * perlcommunity.org/science#mailing_list

        * Paper Ideas  [ NO CHANGE 20240516 ]
            * none

    * 1st Annual Perl Committee Convention
        * NEED ASK TPC for BoF room reservation
        * NEED DRAFT BYLAWS
        * During TPC in Las Vegas, confirmed date Tuesday 6/25/2024, TIME TBD
        * Need to create & ratify bylaws
        * Celebrate at after-party dinner

    * RoboPerl Nonprofit
        * none

    * Science Perl Hackathon
        * Every 6 weeks
        * Next event 5/29/24, 7-9pm Central time zone, theme is Final Paper Approval

* New Business
    * none

* Closing, 2:00pm
