# Science Perl Committee
# Meeting Agenda & Minutes

## August 10th, 2023
## First Official Meeting

* Opening, 2:20pm

* Attendance & Introductions (already done in preceding meeting)
    * Brett Estrade, Co-Founder & Acting Chairman
    * Zakariyya Mughal, Co-Founder & Acting 1st Vice Chairman
    * Will Braswell, Co-Founder & Acting 2nd Vice Chairman & Acting Secretary
    * David Warner
    * Robbie Hatley
    * Darren Duncan
    * Nedzad Hrnjica
    * Rohit Manjrekar
    * Paul Millard
    * Duong Vu
    * Daniel Mera (no intro)
    * Tyler Bird (recap meeting only)
    * Joshua Day (recap meeting only)
    * Rajan Shah (recap meeting only) 

* Announcements
    * Meet every 2 weeks on Thursday 2:00-2:30pm Central time zone
        * Immediately following 1:00pm AI Perl Committee meeting
        * Immediately followed by 2:30pm Perl::Types Committee meeting
    * Recap meeting 8/10 9:30pm tonight
    * Elections 8/24 at next meeting; Chair, 1st & 2nd Vice Chairs, Secretary / Treasurer
    * Bylaws discussion 9/7 at meeting after next

* Read previous meeting's minutes
    * Unofficial meeting #0, attended by Co-Founders Brett & Zaki & Will only
    * Read by Acting Secretary Will Braswell

* Old Business
    * none

* New Business

    * TPC 2024 3rd Track
        * Call For Papers, will be published soon
            * https://gist.github.com/zmughal/5047eb588e8b3cb6de9b12864e99e97b
        * Editorial Review Committee
            * Must review, edit, and approve all papers and posters
        * Proceedings
            * Published in book or magazine form after each conference
        * TPF contact
            * Brett Estrade & Dean Hamstead
        * Websites
            * Overview on science.perl.org
            * Details on perlcommunity.org
        * Paper ideas
            * Zaki Mughal: TensorFlow; PDL; connecting Perl & R
            * Brett Estrade: OpenMP
            * Will Braswell: AI
            * Duong Vu: data engineering
            * Robbie Hatley: astronomy

    * TPF Grants Committee population
        * Need knowledgeable volunteers to join the committee

    * Monthly AI Perl Hackathon
        * Piggyback on this AI Perl Committee activity to work on Science Perl talks, etc?

    * Navi machine group purchase
        * Piggyback on this AI Perl Committee activity to run Science Perl projects, etc?

* Closing, 2:50pm
