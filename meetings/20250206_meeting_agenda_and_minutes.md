# Science Perl Committee
# Meeting Agenda & Minutes

## February 6th, 2025
## Thirty-Eighth Official Meeting

* Opening, 1:30pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Mickey Macten (online security and streaming multimedia)
    * John Napiorkowski (maintainer of Catalyst)
    * Joshua Oliva (interested in scientific breakthroughs, physics, health topics)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 1/23/25 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Brett Estrade to be accepted as read, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of one officer only, not full voting quorum

* Officer Reports
    * Brett reports on the following:
        * STILL NEED check Papercall.io to see if we can create a new conference account for PCC Summer 2025 CFP
        * OJS issue uploading PDF, Dr. Christos unable to upload?
            * STILL NEED FOLLOW UP W/ DR. CHRISTOS
            * Brett will upload PDFs from Overleaf to OJS for now, until bug is fixed etc
        * Overleaf
            * Final SPJ is now created automatically by combining PDFs directly inside Overleaf
            * Attempting to combine Latex directly caused problems with relative image links
        * Waiting on papers from Will, John, Dr. O'Neil
        * STILL NEED merge list of papers from these 2/6/2025 meeting minutes into spreadsheet
            * Already gave Will access to Google docs spreadsheet of paper ideas
        * Mohammed Zia did post flyers for SPJ B&N purchase link at SUNY Buffalo campus in at least one location
            * NEED FOLLOW UP FOR MORE INFO ABOUT LOCATION POSTED ETC.

    * Dr. O'Neil reports on the following:
        * Still working on SPJ #2 paper, short paper 5 - 10 pages w/ lots of color images
        * Plans to work on PCC Winter 2024 video editing

    * Will reports on the following:
        * PCC Summer 2025
            * AI & Science Reception, Evening of July 3rd (and December 17th for PCC Winter 2025)
                * Invitations first
                * Need find famous Perl people
                * Need find donors
        * PPA
            * Have signed a work agreement with Perl programmer for Perl job service
            * Working on ShinyCMS first
        * SPJ issue #2, working on 2 papers (Perl::Types & State of Noonien Address)
        * SPJ publication
            * SPJ issue #1 black & white edition, NEED FINISH
                * ISBN cost ~$100, need to have to continue as our own publisher, US corporate monopoly Bowker
                * Barnes & Noble offers free ISBNs but you are no longer the publisher, it is now B&N, so we will not use that
                * Will use ISBN previously purchased for eBook, will need to purchase new ISBN when ready for eBook
                * Used GhostScript to convert color text and images to grayscale, without any other modification
        * NEED FINISH Crossref DOI registration, $275 per year pro-rated
            * WAITING FOR BARNES & NOBLE DEPOSITS to have enough money
            * WAITING FOR LOGIN ACCOUNT IN ORDER TO PAY THEM
        * Still removing all undesirable logos
            * Need to register the trademarks for all our logos

* Old Business

    * Science Track

        * General Next Steps  [ NO CHANGE 20250206 ]
            * Create timeline of each step of the paper submission / review / approval process  [ DONE ]
            * Publish Science CFP on perlcommunity.org/science   [ DONE ]
            * Distribute Science CFP via e-mail, Facebook, Twitter, TPF announcements, etc   [ DONE ]
            * Distribute Science CFP via IRC (Magnet #perl & #pdl, Libera #perl), Reddit, and remaining avenues [ DONE ]
            * Promoting & requesting paper submissions from those on the Paper Idea list [ DONE ]
            * Start receiving & reviewing paper submissions in OJS [ DONE ]
            * Finish reviewing papers in OJS
            * Publish preprint edition
            * Finalizing papers for SPJ full edition
            * Publish SPJ full edition

        * Perl Community Conference, Summer 2025
            * HYBRID IN-PERSON & VIRTUAL CONFERENCE
            * Registration not yet open
                * NEED MEETUP EVENT LINK
                * NEED FACEBOOK EVENT LINK
            * USA's 249th Birthday 7/3/25 - 7/4/25
            * NEED NAME as Conference MC
            * One track only, one speaker at a time
            * NEED TICKET PRICE
                * Accepting donations via Meetup PayPal
                * For those that Meetup PayPal doesn't work, we can accept payment via Square, Venmo, PayPal
            * Use Google Meet w/ maximum 100 participants, ask for feedback after conference
            * Allow 3 different talk lengths, corresponding to 3 different paper lengths
                * Full-length talks, 50 minutes
                * Short-length talks, 20 minutes
                * Lightning talks, 5 minutes
            * Use Papercall.io as registration for Normal Track conference presenters
            * Use OJS as registration for Science Track conference presenters
            * Use Meetup & Facebook as registration for conference attendees
            * Use existing mailing list to send info to conference attendees
            * Extra Activities for Summer 2025 Conference
                * AI & Science Reception, July 3rd evening
                * Lunch, probably pizza or sandwiches at Hackerspace
                * Dinner, possible after-party at nearby restaurant
                * Bad Movie Night, virtual only online meetup after dinner
                * Fireworks Family Fun, July 4th evening
            * Possible Extra Activities for Summer 2025 Conference
                * Mini Job Fair
                * Mini Investor Pitch?
            * NEED REVIEW 7/17/24 HACKATHON NOTES FOR 2024 CONVENTION DEBRIEF

        * Paper Submission Timeline, Summer 2025
            * Abstracts submitted by NEED DATE
            * Abstracts approved or declined by NEED DATE
            * 1st drafts of papers & posters submitted by NEED DATE
            * 1st drafts feedback sent back to submitters by NEED DATE
            * Final drafts submitted by NEED DATE
            * Final drafts approved or declined by NEED DATE

        * Paper Ideas, Winter 2024 & Summer 2025  [ NO CHANGE 20250206 ]
            * NEED MOVE TO PAPER TRACKER GOOGLE SPREADSHEET
            * Will, PCC Winter 2024 Full Talk, Perl::Types
            * Will, PCC Winter 2024 Short Talk, State of the Noonien Address
            * Will, PCC Summer 2025 Full Talk, PerlGPT Part 2
            * Will, PCC Summer 2025 Lightning Talk, Navi AI
            * Brett, 2024 Lightning Talk, 3 Perl Virtues
            * Brett, 2024 Medium Talk, OpenMP Thread Safety
            * Brett, 2024 Medium Talk, Perl Sequential Consistency
            * Brett, 2025 Full Talk, OpenMP Read-Only API for Perl Data Structures
            * Dimitris Kechagias, iOS weather app for astronomers Xasteria, proxy servers it uses for the weather forecasts are in Perl
            * Dr. Russell, 2025 Full Talk, genetic algorithms, currently changing to new job w/out previous job restriction
            * Dr. Russell, 2024 Medium Talk?, Cowl ontology for CPAN
            * Dr. Christos, using Perl to memory management for buffers in other languages such as assembly (lightning talk); also another talk about FOO (20 min talk)
            * John Napiorkowski, 2024 LPW Lightning Talk, Catalyst Valiant To-Do List Application, Beta Demo
            * John Napiorkowski, 2024 Advent & PCC Lightning Talk, Catalyst Valiant To-Do List Application, Full Demo
            * John Napiorkowski, 2025 Advent & PCC Lightning Talk, Catalyst Valiant To-Do List Application, AI Demo
            * Dr. Luis Mochan, 2024 LPW Lightning Talk & PCC Full Talk, Photonic
            * Dr. O'Neil, 2024 PCC Extended Abstract / Lightning Talk (Possible Short Paper), Comparing Machine Learning Clustering Algorithms for Spectrographic Data
            * Dr. Nancy Lo Man Hung, 2024 PCC Extended Abstract / Lightning Talk, Spider Silk (NEED FULL NAME)
            * Dr. Boyd Duffee, 2024 LPW Extended Abstract / Lightning Talk, "PDL for the Impatient"

        * Editorial Review Subcommittee  [ NO CHANGE 20250206 ]
            * Reviewers
                * Currently reviewers are Dr. Adam Russell, Dr. Casiano Rodriguez Leon, Will Braswell, and Brett Estrade
                * Need more reviewers for 2025, at least 2 per paper
            * OJS
                * Need fix OJS e-mail de-authentication issue, old bug found by Brett & Zaki
                * NEED BRETT & DR. RUSSELL TO REPRODUCE THE BUGS BELOW
                * upload errors, 2 different problems where upload buttons do or do not appear, affecting Manickam & Will & Dr. Russell, full report sent to Brett for further investigation into server logs and GitHub bug reports
            * Editor's Choice Award
                * We will use Dr. Perry's spreadsheet w/ initial selection rubric for Winter 2024 and update as needed
            * Short papers
                * Yes we will accept both short papers (2 - 9 pages) and even extended abstracts (1 page)
                * IEEE guidelines for full-length is 20 to 25 pages, no more than 35 pages including references
                * NEED DECIDE NUMBER OF REFERENCES FOR SHORT PAPERS & EXTENDED ABSTRACTS

        * Proceedings Subcommittee  [ NO CHANGE 20250206 ]
            * Authors get a free printed copy of SPJ, others can purchase a printed copy
            * Science Perl mailing list new members will get free PDF copy of SPJ, others can download PDF with prominent donation button
            * Funding
                * Donation ware online copy
                * Paid physical copies
                * Special signed copies
            * Latex Formatting
                * Brett will create a Latex template for everyone to use in 2025
                * Brett will try to find a way to format the Perl code nicer than it is now
            * Publication
                * We made Conference Preprint Edition in limited run
                * We offered signed copies for sale at higher price
                * NEED FINISH FINAL EDITION
            * Barnes & Noble
                * Now approved for self-publishing nonprofit org account 
                * Price much lower per copy than any other option
            * Need to have committee representative selling SPJ copies at all Perl conferences:
                * TPC::NA
                    * http://tprc.us
                    * 6/??/2025
                    * Need revisit after TPF problems are addressed
                * YAPC::Japan
                    * https://yapcjapan.org
                    * 10/5/2024
                    * George Baugh 2025
                    * NEED JOURNAL COPIES TO SELL
                    * NEED SCIENCE PERL TRACK
                * LPW
                    * https://act.yapc.eu/lpw2024
                    * 10/26/2024
                    * Dr. O'Neil
                    * Science Perl Talks approved
                    * NEED JOURNAL COPIES TO SELL
                * FOSDEM
                    * https://fosdem.org
                    * 2/??/2025
                    * Will & Brett
                    * NEED MONEY FOR TICKET
                    * NEED PERL BOOTH
                    * NEED SUBMIT TALKS
                    * NEED SCIENCE PERL TRACK
                * YAPC::India
                    * NEED CREATE
                * YAPC::Asia (DEFUNCT)
                    * https://yapcasia.org
                    * Last conference 2015
                    * Need revive?
                * YAPC::EU (DEFUNCT)
                    * http://www.yapceurope.org/events/conferences.html
                    * Last Conference 2019
                    * Need revive?
                * YAPC::Russia (DEFUNCT)
                    * https://yapcrussia.org
                    * Last Russian Perl Workshop 2019
                    * Need revive?
                * YAPC::Australia (DEFUNCT)
                    * https://osdc.com.au/
                    * Last Conference 2004?
                    * Need revive?

        * Websites  [ NO CHANGE 20250206 ]
            * Homepage
                * perlcommunity.org/science
                * perlcommunity.org/awards  NEED FINISH
                * NEED UPDATE HACKATHON DATES 7/10 should be 7/17
                * NEED UPDATE 2025 PAPER DATES
            * OJS Subdomain
                * science.perlcommunity.org/spj
                * Updated OJS website with 2025 paper submission timeline dates
                * NEED E-MAIL DEAUTHENTICATION FIX

        * Mailing List  [ NO CHANGE 20250206 ]
            * One-way announcements only
            * Sendy, self-hosted by Brett
            * Request all committee members to register
                * perlcommunity.org/science#mailing_list

    * Science Perl Hackathon
        * Every 6 weeks
        * Last event 2/5/25, 7:00-9:00pm Central time zone, theme is PAPER REVIEW
            * Worked on Overleaf margins and SPJ #1 black & white edition
        * Next event 3/19/25, 7:00-9:00pm Central time zone, theme is PAPER REVIEW

    * Treasurer's Report  [ NO CHANGE 20250206 ]
        * We have enough money for Crossref
        * Need the Fundraising Committee to start collecting donations for 2025 activities
        * NEED PAY PRORATED CROSSREF
        * NEED BRETT RECEIPTS
        * NEED UPDATE SPREADSHEET WITH SPJ SALES & TPC RECEIPTS

    * RoboPerl Nonprofit
        * none

* New Business

    * none

* Closing, 2:13pm
