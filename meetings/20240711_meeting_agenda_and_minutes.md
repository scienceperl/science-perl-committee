# Science Perl Committee
# Meeting Agenda & Minutes

## July 11th, 2024
## Twenty-Fourth Official Meeting

* Opening, 1:07pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Steven McDougall (1980's medical ultrasound technology, 1990's genomic sequencing, using Perl to work on scientific problems)
    * Joshua Oliva (interested in scientific breakthroughs, physics, health topics)
    * Adam Russell, PhD (computer scientist, leads group United Health Group subsidiary OptumAI, developing new products to bring AI & ML to admin workers)
    * John Napiorkowski (maintainer of Catalyst)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 6/13/24 meeting minutes
    * 6/25/24 convention minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Brett Estrade to be accepted as corrected, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of one officer only, not full voting quorum

* Officer Reports
    * Brett reports on the following:
        * SPJ papers need to be finalized for full edition 
        * No good deed goes unpunished by the bad guys, we will give more details at the next committee meeting

    * Will reports on the following:
        * Meetup groups created for Austin and Dallas Perl Mongers, with recurring Meetup events for Perl Committee Meetings

* Old Business

    * TPC 2024 Science Track

        * General Next Steps
            * Fix OJS e-mail de-authentication issue
            * Create timeline of each step of the paper submission / review / approval process  [ DONE ]
            * Publish Science CFP on perlcommunity.org/science   [ DONE ]
            * Distribute Science CFP via e-mail, Facebook, Twitter, TPF announcements, etc   [ DONE ]
            * Distribute Science CFP via IRC (Magnet #perl & #pdl, Libera #perl), Reddit, and remaining avenues [ DONE ]
            * Promoting & requesting paper submissions from those on the Paper Idea list [ DONE ]
            * Send TPC Planning Committee our list of approved science papers ASAP, so they can review & coordinate with other track scheduling [ DONE ]
            * Start receiving & reviewing paper submissions in OJS [ DONE ]
            * Finish reviewing papers in OJS  [ DONE ]
            * Publish preprint edition [ DONE ]
            * Finalizing papers for full edition
            * Publish full edition

        * Remote Presentations
            * We had 3 remote presentations for the Science Perl Track: Dr. Christos, Dr. O'Neil, Manickam

        * Paper Submission Timeline
            * Abstracts submitted by April 5th  [ DONE ]
            * Abstracts approved or declined by April 15th  [ DONE ]
            * Extended CFP, abstracts submitted by April 20th  [ DONE ]
            * Extended CFP, abstracts approved or declined by April 30th  [ DONE ]
            * Extended 1st drafts of papers & posters submitted by May 20th  [ DONE ]
            * 1st drafts feedback sent back to submitters by May 31st [ DONE ]
            * Final drafts submitted by June 7th [ DONE ]
            * Final drafts approved or declined by June 15th [ DONE]
            * Final drafts for full papers by July 18th

        * Call For Papers
            * 2025 CFP now open, unofficial soft open
            * Will do 2024 Science Track & Journal & CFP debrief at next Science Perl Hackathon on 7/17/24
            * NEED DECIDE
                * Short papers?
                * GitLab + OJS + OverLeaf ?

        * Editorial Review Subcommittee
            * NOW AN OFFICIAL SUBCOMMITTEE VIA NEW BYLAWS
            * NEED UPDATE OJS & WEBSITE, submitters must create their presentation slides to match their paper or poster content
            * Reviewers
                * Currently reviewers are Dr. Marc Perry, Dr. Adam Russell, Will Braswell, and Brett Estrade
                * One reviewer is okay if we're not rejecting papers for 2024
                * We need more reviewers for 2025, at least 2 per paper
            * OJS upload errors, 2 different problems where upload buttons do or do not appear, affecting Manickam & Will & Dr. Russell, full report sent to Brett for further investigation into server logs and GitHub bug reports
            * Editor's Choice Award
                * We now have Dr. Perry's spreadsheet w/ initial selection rubric for 2025 and following years
            * Short papers
                * Dr. Christos suggests we allow short papers next year
                * IEEE guidelines is 20 to 25 pages, no more than 35 pages including references
                * Need to make our own guidelines for short papers?

        * Proceedings Subcommittee
            * NOW AN OFFICIAL SUBCOMMITTEE VIA NEW BYLAWS
            * Current authors will be offered a printed copy of SPJ at cost
            * Non-authors will be offered a printed copy of SPJ at a fair market mark-up as part of our annual fundraising, to finance printing next year
            * Those who sign up for Science Perl mailing list will get free PDF copy of SPJ 
            * Put PDF copy of SPJ online for free download, with prominent donate button for anyone who is not a student, works based on the honor system
            * Funding
                * Donation ware online copy
                * Paid physical copies
                * Special signed copies
            * Latex Formatting
                * Brett will make the final Latex edits
                * Single column, default format, only change is decrease margins
                * Want to upload everyone's Latex source to OJS as well as PDFs
            * Publication
                * Pefect binding is okay, no need for spiral
                * We will need to make Conference Preprint Edition in limited run, then fully finalized edition after all papers are pristine
                * Offer signed copies for sale at higher price
            * Comments made during the conference talks can optionally be incorporated into the papers
                * This is how the old academic groups like The Royal Society worked
            * Announce that the final edition will be released in August or September with all edits and accepted comments
            * Need to have committee representative selling SPJ copies at all Perl conferences:
                * TPC NA
                * YAPC EU
                * YAPC Asia
                * LPW
                * FOSDEM

        * Websites
            * Homepage
                * perlcommunity.org/science
                * perlcommunity.org/awards  NEED FINISH
                * NEED UPDATE HACKATHON DATES 7/10 should be 7/17
                * NEED UPDATE WEBSITE WITH 2025 PAPER SUBMISSION TIMELINE DATES
            * OJS Subdomain
                * science.perlcommunity.org/spj
                * NEED E-MAIL DEAUTHENTICATION FIX

        * Mailing List  [ NO CHANGE 20240711 ]
            * One-way announcements only
            * Sendy, self-hosted by Brett
            * Request all committee members to register
                * perlcommunity.org/science#mailing_list

        * Paper Ideas
            * Will, PerlGPT Part 2 or Navi AI
            * Brett, OpenMP Read-Only API for Perl Data Structures

    * 1st Annual Perl Committee (Constitutional!) Convention
        * Debrief during 7/17/24 Science Perl Hackathon

    * RoboPerl Nonprofit
        * none

    * Science Perl Hackathon
        * Every 6 weeks
        * Last event 5/29/24, 7-9pm Central time zone, theme was Final Paper Approval
        * Next event 7/17/24, SPECIAL DATE, 7:00-9:00pm Central time zone, theme is Science Track 2024 Debrief

    * Treasurer's Report
        * We have enough money for Crossref
        * Need the Fundraising Committee to start collecting donations for 2025 activities
        * NEED PAY PRORATED CROSSREF
        * NEED BRETT RECEIPTS
        * NEED UPDATE SPREADSHEET WITH SPJ SALES & TPC RECEIPTS

    * Marketing Subcommittee
        * Social Media ads
            * Facebook
            * Twitter
            * Mastodon
            * etc
        * Science Track Talk
            * NEED SUBMIT TALK LINKS to news outlets
                * Hacker News
                * Slashdot
                * Reddit
        * Need workflow for jnap to build Catalyst software which can automate social media marketing redistribution

* New Business

    * Fundraising Committee (not Subcommittee)
        * Dr. Christos, potential committee chair
        * SPJ Sales
            * Dr. Russell, contacting libraries
            * NEED POST PREPRINT ON FACEBOOK

* Closing, 2:11pm
