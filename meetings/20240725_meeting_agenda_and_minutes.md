# Science Perl Committee
# Meeting Agenda & Minutes

## July 25th, 2024
## Twenty-Fifth Official Meeting

* Opening, 1:04pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Tom Bodine (started Perl at UT in 1994, GPS sattelite mapping applications using Perl; server penetration monitoring at Caribbean Online; memory tests at AMD)
    * Dr. Drew O'Neil (AKA Andrew, uses Perl AI for chemometrics)
    * Manickam Thanneermalai (EE design engineer, designing chips, building ARM-based system-on-chip for automotive AI)
    * Mohammed Zia (Perl for devops and one-liners)
    * Joshua Oliva (interested in scientific breakthroughs, physics, health topics)
    * John Napiorkowski (maintainer of Catalyst)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 7/11/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Brett Estrade to be accepted as corrected, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of one officer only, not full voting quorum

* Officer Reports
    * Brett reports on the following:
        * Distracted recently due to hurricane work & illness
        * Main goal is SPJ final edition by this weekend
        * SPJ 2025 paper submission dates are updated on OJS
        * TPC 2025, likely end of June in South Carolina

    * Dr. O'Neil reports on the following:
        * Planning to attend LPW 2024

    * Will reports on the following:
        * Meetup groups created for Austin and Dallas Perl Mongers, with recurring Meetup events for Perl Committee Meetings, yes people are slowly joining via Meetup
        * need finish SPJ preprint sales on FB
        * Brett & Will currently dealing with problematic TPF people, will give update when available

* Old Business

    * TPC 2024 Science Track

        * General Next Steps
            * Fix OJS e-mail de-authentication issue
            * Create timeline of each step of the paper submission / review / approval process  [ DONE ]
            * Publish Science CFP on perlcommunity.org/science   [ DONE ]
            * Distribute Science CFP via e-mail, Facebook, Twitter, TPF announcements, etc   [ DONE ]
            * Distribute Science CFP via IRC (Magnet #perl & #pdl, Libera #perl), Reddit, and remaining avenues [ DONE ]
            * Promoting & requesting paper submissions from those on the Paper Idea list [ DONE ]
            * Send TPC Planning Committee our list of approved science papers ASAP, so they can review & coordinate with other track scheduling [ DONE ]
            * Start receiving & reviewing paper submissions in OJS [ DONE ]
            * Finish reviewing papers in OJS  [ DONE ]
            * Publish preprint edition [ DONE ]
            * Finalizing papers for full edition, NEED FINISH BY THIS WEEKEND IF POSSIBLE
            * Publish full edition

        * Remote Presentations
            * Related to TPF issues mentioned in officer report above
            * We will do everything possible to bring Science Track talks in person, and when not possible will allow remote talks

        * Paper Submission Timeline
            * Abstracts submitted by April 5th  [ DONE ]
            * Abstracts approved or declined by April 15th  [ DONE ]
            * Extended CFP, abstracts submitted by April 20th  [ DONE ]
            * Extended CFP, abstracts approved or declined by April 30th  [ DONE ]
            * Extended 1st drafts of papers & posters submitted by May 20th  [ DONE ]
            * 1st drafts feedback sent back to submitters by May 31st [ DONE ]
            * Final drafts submitted by June 7th [ DONE ]
            * Final drafts approved or declined by June 15th [ DONE]
            * Final drafts for full papers by July 28th, NEED BRETT TO FOLLOW UP WITH REMAINING AUTHORS

        * Call For Papers, 2025
            * 2025 CFP now open, unofficial soft open
            * Did 2024 Science Track & Journal & CFP debrief at Science Perl Hackathon on 7/17/24
            * NEED DECIDE
                * Short papers?  (see Editorial Review Subcommittee below)
                * Keep using workflow of GitLab + OJS + OverLeaf ?

        * Paper Ideas, 2025
            * Will, PerlGPT Part 2 or Navi AI
            * Brett, OpenMP Read-Only API for Perl Data Structures
            * Dimitris Kechagias, iOS weather app for astronomers Xasteria, proxy servers it uses for the weather forecasts are in Perl

        * Editorial Review Subcommittee
            * NEED UPDATE OJS & WEBSITE, submitters must create their presentation slides to match their paper or poster content
            * Reviewers
                * Currently reviewers are Dr. Marc Perry, Dr. Adam Russell, Will Braswell, and Brett Estrade
                * Need more reviewers for 2025, at least 2 per paper
            * OJS
                * NEED BRETT & DR. RUSSELL TO REPRODUCE THE BUGS BELOW
                * upload errors, 2 different problems where upload buttons do or do not appear, affecting Manickam & Will & Dr. Russell, full report sent to Brett for further investigation into server logs and GitHub bug reports
                * Brett started watching OJS tutorial videos
            * Editor's Choice Award
                * We now have Dr. Perry's spreadsheet w/ initial selection rubric for 2025 and following years
            * Short papers
                * Dr. Christos suggests we allow short papers next year
                * IEEE guidelines is 20 to 25 pages, no more than 35 pages including references
                * Need to make our own guidelines for short papers, including length & number of references?

        * Proceedings Subcommittee
            * Authors get a free printed copy of SPJ, others can purchase a printed copy
            * Science Perl mailing list new members will get free PDF copy of SPJ, others can download PDF with prominent donation button
            * Funding
                * Donation ware online copy
                * Paid physical copies
                * Special signed copies
            * Latex Formatting
                * Brett will create a Latex template for everyone to use in 2025
            * Publication
                * We made Conference Preprint Edition in limited run
                * We offered signed copies for sale at higher price
                * NEED FINISH FINAL EDITION
            * Barnes & Noble
                * Now approved for self-publishing nonprofit org account 
                * Price much lower per copy than any other option
            * Need to have committee representative selling SPJ copies at all Perl conferences:
                * TPC::NA
                    * http://tprc.us
                    * 6/??/2025
                    * Need revisit after TPF problems are addressed
                * YAPC::Japan
                    * https://yapcjapan.org
                    * 10/5/2024
                    * George Baugh
                    * NEED JOURNAL COPIES TO SELL
                    * NEED SCIENCE PERL TRACK
                * LPW
                    * https://act.yapc.eu/lpw2024
                    * 10/26/2024
                    * Dr. O'Neil
                    * NEED FOLLOW UP TO GET TALK SUBMITTED
                    * NEED JOURNAL COPIES TO SELL
                    * NEED SCIENCE PERL TRACK
                * FOSDEM
                    * https://fosdem.org
                    * 2/??/2025
                    * Will & Brett
                    * NEED MONEY FOR TICKET
                    * NEED PERL BOOTH
                    * NEED SUBMIT TALKS
                    * NEED SCIENCE PERL TRACK
                * YAPC::India
                    * NEED CREATE
                * YAPC::Asia (DEFUNCT)
                    * https://yapcasia.org
                    * Last conference 2015
                    * Need revive?
                * YAPC::EU (DEFUNCT)
                    * http://www.yapceurope.org/events/conferences.html
                    * Last Conference 2019
                    * Need revive?
                * YAPC::Russia (DEFUNCT)
                    * https://yapcrussia.org
                    * Last Russian Perl Workshop 2019
                    * Need revive?
                * YAPC::Australia (DEFUNCT)
                    * https://osdc.com.au/
                    * Last Conference 2004?
                    * Need revive?

        * Websites
            * Homepage
                * perlcommunity.org/science
                * perlcommunity.org/awards  NEED FINISH
                * NEED UPDATE HACKATHON DATES 7/10 should be 7/17
                * NEED UPDATE 2025 PAPER DATES
            * OJS Subdomain
                * science.perlcommunity.org/spj
                * Updated OJS website with 2025 paper submission timeline dates
                * NEED E-MAIL DEAUTHENTICATION FIX

        * Mailing List  [ NO CHANGE 20240725 ]
            * One-way announcements only
            * Sendy, self-hosted by Brett
            * Request all committee members to register
                * perlcommunity.org/science#mailing_list

    * 2nd Annual Perl Committee Convention
        * NEED DETERMINE DATE & LOCATION
        * NEED REVIEW 7/17/24 HACKATHON NOTES FOR 2024 CONVENTION DEBRIEF

    * RoboPerl Nonprofit
        * none

    * Science Perl Hackathon
        * Every 6 weeks
        * Last event 7/17/24, 7-9pm Central time zone, theme was Science Perl Track 2024 Debrief
        * Next event 8/21/24, SPECIAL DATE, 7:00-9:00pm Central time zone, theme is Conference Representatives Planning

    * Treasurer's Report [ NO CHANGE 20240725 ]
        * We have enough money for Crossref
        * Need the Fundraising Committee to start collecting donations for 2025 activities
        * NEED PAY PRORATED CROSSREF
        * NEED BRETT RECEIPTS
        * NEED UPDATE SPREADSHEET WITH SPJ SALES & TPC RECEIPTS

    * Marketing Subcommittee
        * Social Media ads
            * Facebook
                * NEED CONTINUE RELEASING TALK LINKS
            * Twitter
            * Mastodon
            * etc
        * News Outlets
            * Reddit
                * NEED CONTINUE RELEASING TALK LINKS
            * Hacker News
            * Slashdot
        * Need workflow for jnap to build Catalyst software which can automate social media marketing redistribution

    * Fundraising Committee (not Subcommittee)  [ NO CHANGE 20240725 ]
        * Monday 8/19/24 10am Central time zone, first meeting
        * Dr. Christos, founding committee chair
        * SPJ Sales
            * Dr. Russell, contacting libraries
            * NEED POST PREPRINT ON FACEBOOK

* New Business
    * none

* Closing, 2:01pm
