# Science Perl Committee
# Meeting Agenda & Minutes

## November 16th, 2023
## Eighth Official Meeting

* Opening, 1:03pm

* Attendance & Introductions
    * Brett Estrade (high-speed computing)
    * Zakariyya Mughal (biomedical imaging, BioPerl, PDL, toxicology research)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Adam Russell, PhD (computer scientist, leads group United Health Group subsidiary OptumAI, developing new products to bring AI & ML to admin workers)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 11/2/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Zaki Mughal to be accepted as read, voted unanimously

* Officer Reports
    * Zaki is sending out the survey to the PDL & BioPerl e-mail lists
    * Will filled out the survey live during airing of Perl Town Hall episode #100 & sent to Austin Perl Mongers e-mail list

* Old Business

    * TPC 2024 Science Track

        * TPF & TPC Science Track Proposal report
            * TPF approval is pending until survey is complete

        * Call For Papers
            * Everyone should keep working on paper ideas
            * CFP will be opened when TPF gives official approval
            * Should be shortly after survey completes

        * Survey
            * Currently have received 31 responses
            * We want to end the survey in approximately 1 week from today
            * Dean & Peter from TPF have given little feedback since our last meeting
            * Brett shared survey to HPC groups at Texas A&M, LSU, University of Texas TACC
            * Bruce Gray suggested sending survey to 207 Perl Mongers contacts, 30 bounced, the rest went through
            * We reviewed the results from the latest 11 responses, all generally positive as before
            * Brett will make a blog post and ask the current respondents to share the survey
            * Brett is optimistic that we will hopefully be able to get TPF's approval based on the survey

        * Editorial Review Subcommittee
            * We reviewed the OpenConf & pretalx demos
                * These are both conference management platforms, which we don't want because we need to use TPF's conference management system (sched?)
            * Will Braswell moved to vote for approval of OJS, Zaki seconded, unanimously approved
            * We will use OJS to accept & review abstracts for papers & posters

        * Proceedings
            * We will use OJS to publish our proceedings

        * Websites
            * Will Braswell will update perlcommunity.org after TPF approves the Science Track

        * Paper Ideas
            * none

    * Science Perl Hackathon
        * Every 6 weeks
        * Next event 12/13/23, 7-9pm Central time zone, theme is Call for Papers & Editorial Review Subcommittee

* New Business
    * none

* Closing, 1:48pm
