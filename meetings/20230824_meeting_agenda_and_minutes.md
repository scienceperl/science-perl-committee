# Science Perl Committee
# Meeting Agenda & Minutes

## August 24th, 2023
## Second Official Meeting

* Opening, 2:03pm

* Attendance & Introductions (already done in preceding meeting)
    * Will Braswell (creator of Navi AI)
    * Drew O'Neil (AKA Andrew, uses Perl AI for chemometrics)
    * David Warner (AI sales & marketing)
    * Brett Estrade (high-speed computing)
    * Antanas Vaitkus (University in Lithuania, crystallography, wants to learn AI)
    * Somanath Wagh (Mumbai, India; wants to learn AI)
    * Leo Grapendaal (Dutchman living in Germany, interested in AI generating text for publications)
    * Glenn Holmgren (no introduction, audio problems)
    * Manickam Thanneermalai (EE design engineer, designing chips, Perl automates work in semiconductor industry, Synopsis & Cadence tool vendors have AI algorithms, building ARM-based system-on-chip for automotive AI)
    * Zakariyya Mughal (biomedical imaging, BioPerl, PDL)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings
    * Bylaws discussion starting 9/7/23 at next meeting

* Previous Meeting's Minutes
    * 8/10/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Leo G. to be accepted as read, voted unanimously

* Old Business

    * TPC 2024 3rd Track
        * Call For Papers, will be published soon
            * https://gist.github.com/zmughal/5047eb588e8b3cb6de9b12864e99e97b
            * Zaki posted a link to Journal of Open Source Software in gist comment
        * Editorial Review Committee
            * Journal of Open Source Software, they have a review process & checklist, we can use as inspiration
            * https://joss.readthedocs.io/en/latest/review_criteria.html
            * https://joss.readthedocs.io/en/latest/review_checklist.html
        * Proceedings
            * no comments
        * TPF report
            * Brett Estrade & Dean Hamstead presented 3rd Track idea to TPF Community Reps Committee 8/18/23
            * Brett needs to ask TPF for final written approval of 3rd Track, so we can put out the CFP ASAP
        * Websites
            * CFP will be posted on perlcommunity.org
        * Paper ideas
            * Manickam T (cross-compiling Perl for specialized hardware microcontrollers & microprocessors)

* New Business

    * Officer Elections
        * Chairman, Brett Estrade nominated by Will Braswell, no other nominations, elected unanimously
        * 1st Vice Chairman, Zaki Mughal nominated by Brett Estrade, no other nominations, elected unanimously
        * 2nd Vice Chairman, Will Braswell nominated by Antanas Vaitkus, no other nominations, elected unanimously

* Closing, 2:45pm
