# Science Perl Committee
# Meeting Agenda & Minutes

## December 28th, 2023
## Eleventh Official Meeting

* Opening, 1:13pm

* Attendance & Introductions
    * Zakariyya Mughal (BioPerl, PDL, previously biomedical imaging, currently computational toxicology)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Douglas Spore (Perl regular expressions, government classified system, Air Force Defense Support Program)

* Announcements
    * Chairman Brett Estrade allows 1st Vice Chairman Zaki Mughal to preside
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 12/14/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Zaki Mughal to be accepted as read, voted unanimously

* Officer Reports
    * Zaki reports on the following:
        * He will be putting up OJS on his own server soon, to potentially use as either a prototype or the final version

    * Will reports on the following:
        * TPF has not released CFP yet, presumably due to holidays

* Old Business

    * TPC 2024 Science Track

        * TPF & TPC Science Track Proposal report
            * TPF approval is still only partially official until a public statement is made by TPF, presumably as part of the upcoming CFP

        * Call For Papers
            * CFP will be opened hopefully soon, waiting until then to take any further action

        * Editorial Review Subcommittee
            * none

        * Proceedings
            * none

        * Websites
            * none

        * Paper Ideas
            * none

    * RoboPerl Nonprofit
        * none

    * Science Perl Hackathon
        * Every 6 weeks
        * Next event 1/24/24, 7-9pm Central time zone, theme is Website Development

* New Business
    * none

* Closing, 1:33pm
