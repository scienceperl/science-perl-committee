# Science Perl Committee
# Meeting Agenda & Minutes

## January 25th, 2024
## Thirteenth Official Meeting

* Opening, 1:07pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Zakariyya Mughal (BioPerl, PDL, previously biomedical imaging, currently computational toxicology)
    * Jakub Pawlowski (wants to integrate Perl AI with .NET & Mono)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 1/11/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Brett Estrade to be accepted as modified, voted unanimously

* Officer Reports
    * Brett reports on the following:
        * Last night at the Hackathon we got OJS set up
        * Brett will set up e-mail sending & also receiving ability via Gmail
        * Brett will write first draft of CFP
        * Brett posted blog announcement, the most official Science Track announcement possible at this time
            * https://blogs.perl.org/users/oodler_577/2024/01/perl-raku-conference-2024-to-host-a-science-track.html

    * Zaki reports on the following:
        * Last night we worked on setting up the Open Journal Systems on Digital Ocean server, empty setup other than 3 admin accounts for committee officers
            * https://science.perlcommunity.org/spj
        * We set up a to-do list for the OJS server admin
            * https://gitlab.com/scienceperl/conference-submission/-/issues/1
        * Annals and Conference Proceedings (paper & talk) is a subset of the Journal (which includes non-talk papers)
            * May need to accept non-talk papers if somebody can't attend the conference or we have too many talks to fit into the conference
        * Need to set up basic text content for About page, CFP, etc
        * Need to create timeline of each step of the paper submission / review / approval process

    * Will reports on the following:
        * Last night at the Science Perl Hackathon we were visited by Bruce Gray from TPF Board and TPC Planning Committee, we discussed the upcoming CFP issues and came to the following conclusions...

            * Peter already wrote Brett a message in December on TPRF Slack saying the Science Track is approved, and Bruce confirms this is true
            * We will move forward with our Science CFP in approximately one week from now
            * Brett will attend the next TPC Planning Committee meeting in one week from now, to review our Science CFP with them before we publish

            * Basic next steps:
                * Set up OJS
                * Write up Science Perl Track CFP Announcement
                * Send Science CFP to TPC Planning Committee for final review on 1/31/24
                * Publish Science CFP on perlcommunity.org/science
                * Distribute Science CFP via e-mail, Facebook, Twitter, IRC, etc
                * Start receiving & reviewing paper submissions in OJS
                * Finish reviewing papers in OJS 
                * Send TPC Planning Committee our list of approved science papers ASAP, so they can review & coordinate with other track scheduling

            * Science CFP and all our other communications will always include both TPC main link and Science CFP link, to show our official affiliation
                * https://tprc.us/tprc-2024-las
                * https://perlcommunity.org/science  (NOT LIVE YET)

* Old Business

    * TPC 2024 Science Track

        * TPF & TPC Science Track Proposal report
            * approved, no longer a proposal

        * Call For Papers
            * We will get Science CFP ready for Brett to bring to TPC Planning Committee on 1/31
            * We will release Science CFP within approx 3 days after approval
            * We will require abstracts to be done within 1 month after CFP announced
            * We will require all papers to be submitted by May 15th
            * List of Science CFP distribution channels:
                * E-mail from Brett, people who responded to the Science Track Survey
                * E-mail from Brett, Perl Monger leaders
                * E-mail from Zaki, BioPerl & PDL mailing lists
                * Social media from Will, Facebook
                * Social media from Will, Twitter
                * Social media from Zaki, Mastodon
                * Social media from Zaki, LinkedIn
                * IRC.perl.org MAGnet chat from Zaki
                * IRC.libera chat from Zaki
                * TPRF Slack chat from Brett
                * HPC Social Discord chat from Brett
                * Perl Discord chat from Brett
                * Blogs.perl.org blogs from Brett
                * Dev.to blogs from Zaki
                * Reddit website from Zaki
                * Perl Monks website from Brett
                * TPF website from Brett
                * Perl Planet aggregator from Brett
                * Perl Weekly Newsletter aggregator from Brett
            * Brett has Sendy, like a self-hosted MailChimp, sends through Amazon SCS, can be used for advertising and updating everyone on Science Track
                * Sends from FOO@science.perlcommunity.org subdomain
                * People can sign themselves up and also opt out

        * Editorial Review Subcommittee
            * Brett invited Ruth Holloway and Bruce Gray from TPF Board to serve the Editorial Review Subcommittee as Subject Matter Experts AKA Guest Reviewers, without actually being a member of the Science Perl Committee or Editorial Review Subcommittee
            * Guest Reviewers can give feedback but can NOT vote on paper approval
            * Need to have Subcommittee Hackathons starting every 2 weeks once we have our first paper submission
            * Will wait to create Google Form for recruiting Subcommittee members until after we put out the CFP because we may get enough from that

        * Proceedings
            * Wait until after conference
            * Will use OJS

        * Websites
            * Now have subdomain for OJS server
                * science.perlcommunity.org/spj
            * Will started working on updating perlcommunity.org, we will have new webpage
                * perlcommunity.org/science
            * Need to finish basic webpage before CFP review on 1/31

        * Paper Ideas
            * Jakub, Perl AI integration w/ .NET & Mono???  ( no microphone, unable to confirm )

    * RoboPerl Nonprofit
        * none

    * Science Perl Hackathon
        * Every 6 weeks
        * Next event 3/6/24, 7-9pm Central time zone, theme is Paper Review Process

* New Business
    * none

* Closing, 2:09pm
