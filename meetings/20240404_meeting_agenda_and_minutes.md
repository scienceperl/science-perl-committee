# Science Perl Committee
# Meeting Agenda & Minutes

## April 4th, 2024
## Eighteenth Official Meeting

* Opening, 1:05pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Joshua Oliva (interested in scientific breakthroughs, physics, health topics)
    * Justin Kelly (microphone broken, no introduction)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 3/21/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Brett Estrade to be accepted as corrected, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of two officers only, not full voting quorum

* Officer Reports
    * Brett reports on the following:
        * Sent out e-mail blast to everyone who completed the original Science Track survey
        * Created blogs.perl.org post about Science Track submission deadlines, got distributed in Perl Weekly Newsletter
        * Need to get TPF approval for Dr. Christos' submission to be presented remotely

    * Will reports on the following:
        * Dr. Christos submitting abstracts for both 2024 and 2025
        * Want to give TPF list of papers from 2024 and also pending 2025 papers all at once, to improve Science Track legitimacy
        * Paper idea list, need update & contact today

* Old Business

    * TPC 2024 Science Track

        * General Next Steps  [ NO CHANGE 20240404 ]
            * Fix OJS e-mail de-authentication issue
            * Create timeline of each step of the paper submission / review / approval process  [ DONE ]
            * Publish Science CFP on perlcommunity.org/science   [ DONE ]
            * Distribute Science CFP via e-mail, Facebook, Twitter, TPF announcements, etc   [ DONE ]
            * Distribute Science CFP via IRC (Magnet #perl & #pdl, Libera #perl), Reddit, AND REMAINING AVENUES
            * Promoting & requesting paper submissions from those on the Paper Idea list
            * Start receiving & reviewing paper submissions in OJS
            * Finish reviewing papers in OJS 
            * Send TPC Planning Committee our list of approved science papers ASAP, so they can review & coordinate with other track scheduling

        * Remote Presentations
            * Need TPF approval for Dr. Christos remote talk 2024

        * Paper Submission Timeline  [ NO CHANGE 20240404 ]
            * Abstracts submitted by April 5th
            * Abstracts approved or declined by April 15th
            * 1st drafts of papers & posters submitted by May 15th
            * 1st drafts feedback sent back to submitters by May 31st
            * Final drafts submitted by June 7th
            * Final drafts approved or declined by June 15th

        * Call For Papers
            * Sent out e-mail blast to everyone who completed the original Science Track survey
            * Created blogs.perl.org post about deadlines, got distributed on Perl Weekly Newsletter
            * Submitters will be directed to have presentation slides that match their paper or poster content
            * Time's almost up for promoting Science CFP, list of distribution channels:
                * E-mail from Brett, people who responded to the Science Track Survey [ DONE ]
                * E-mail from Brett, universities (LSU, UT, BYU) [ DONE ]
                * E-mail from Brett, computational oceanographers [ DONE ]
                * E-mail from Brett, Perl Monger leaders
                * E-mail from Zaki, BioPerl & PDL mailing lists
                * Social media from Will, Facebook [ DONE ]
                    * https://www.facebook.com/groups/perlprogrammers/posts/7567632603269462
                    * https://www.facebook.com/wnbjr/posts/pfbid023mjrkJaE7yY3EhdF9S81237vxaPdM3d1cJEjAk7BofcbpsEn3ceBh8scrk3siR4El
                    * https://www.facebook.com/perlcommunity/posts/pfbid0Vu2kdYRLgJJhWwSEyEu9uuDzkDKczE3FdgJU4yibepBdnPvf6hy9YJgoxPrpVaTWl
                * Social media from Will, Twitter [ DONE ]
                    * https://twitter.com/perlcommunity/status/1760842963125403904
                * Social media from Zaki, Mastodon
                * Social media from Zaki, LinkedIn
                * IRC.perl.org MAGnet chat from Zaki
                * IRC.libera chat from Zaki
                * TPRF Slack chat from Brett
                * HPC Social Discord chat from Brett
                * Perl Discord chat from Brett
                * Blogs.perl.org blogs from Brett [ DONE ]
                    * https://blogs.perl.org/users/oodler_577/2024/02/repost-tprc-2024-call-for-papers-is-now-open.html
                * Dev.to blogs from Zaki
                * Reddit website from Zaki
                * Perl Monks website from Brett
                * TPF website from Brett [ DONE ]
                    * https://news.perlfoundation.org/post/cfp2024
                * Perl Planet aggregator from Brett
                * Perl Weekly Newsletter aggregator from Brett [ DONE ]
                    * https://perlweekly.com/archive/656.html
                * NEED UPDATE & REVIEW DISTRIBUTION CHANNEL LIST ABOVE, THEN CONTACT ALL CHANNELS

        * Editorial Review Subcommittee  [ NO CHANGE 20240404 ]
            * We may or may not have a formal motion to officially create a subcommittee, depending on how many paper submissions we receive

        * Proceedings  [ NO CHANGE 20240404 ]
            * Same as Editorial Review Subcommittee above

        * Websites
            * Homepage
                * perlcommunity.org/science
                * NEED UPDATE WEBSITE WITH PAPER SUBMISSION TIMELINE DATES
            * OJS Subdomain
                * science.perlcommunity.org/spj
                * NEED E-MAIL DEAUTHENTICATION FIX

        * Mailing List  [ NO CHANGE 20240404 ]
            * One-way announcements only
            * Sendy, self-hosted by Brett
            * Request all committee members to register
                * perlcommunity.org/science#mailing_list

        * Paper Ideas
            * Justin Kelly, space science & outer space 2025
            * Joshua Oliva, scientific breakthroughs, health topics, physics 2025
            * NEED UPDATE & REVIEW PAPER IDEA LIST, THEN CONTACT ALL POTENTIAL SUBMITTERS

    * RoboPerl Nonprofit
        * none

    * Science Perl Hackathon
        * Every 6 weeks
        * Next event 4/17/24, 7-9pm Central time zone, theme is Paper Development

* New Business
    * none

* Closing, 2:03pm
