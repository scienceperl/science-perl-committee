# Science Perl Committee
# Meeting Agenda & Minutes

## February 22nd, 2024
## Fifteenth Official Meeting

* Opening, 1:03pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Zakariyya Mughal (BioPerl, PDL, previously biomedical imaging, currently computational toxicology)
    * Marc Perry, PhD (biologist, used to have wet lab, now doing bioinformatics & genomics using Perl 15 years, Senior Bioinformatician at Genomics Institute of UC Santa Cruz)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 2/8/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Brett Estrade to be accepted as read, voted unanimously

* Officer Reports
    * Brett reports on the following:
        * OJS is working
        * E-mail is working
        * SSL certificate is working
        * Need to distribute CFP via social e-mail, media, etc

    * Zaki reports on the following:
        * OJS only sends one e-mail at a time then de-authenticates, need to fix it

    * Will reports on the following:
        * Working on perlcommunity.org/science, will be posted publicly soon with Overview & Science Track subsections

* Old Business

    * TPC 2024 Science Track

        * General Next Steps
            * Fix OJS e-mail de-authentication issue
            * Create timeline of each step of the paper submission / review / approval process
            * Publish Science CFP on perlcommunity.org/science
            * Distribute Science CFP via e-mail, Facebook, Twitter, IRC, etc
            * Start receiving & reviewing paper submissions in OJS
            * Finish reviewing papers in OJS 
            * Send TPC Planning Committee our list of approved science papers ASAP, so they can review & coordinate with other track scheduling

        * Remote Presentations
            * Peter from TPF says as long as somebody is standing at the podium then we are fine to have remote presenters
            * Peter has not yet given us any specific percentage number
            * Brett will ask Peter if 40% remote talks is acceptable

        * Call For Papers
            * TPF sent our Science CFP as part of main CFP
            * We want abstracts to be submitted by April 5th
            * We want final papers & posters to be submitted by May 15th
            * Brett created infographic for CFP
                * https://www.facebook.com/groups/scienceperl/posts/917497506595730
            * Time to go full steam ahead on promoting Science CFP, list of distribution channels:
                * E-mail from Brett, people who responded to the Science Track Survey [ DONE ]
                * E-mail from Brett, universities (LSU, UT, BYU) [ DONE ]
                * E-mail from Brett, computational oceanographers [ DONE ]
                * E-mail from Brett, Perl Monger leaders
                * E-mail from Zaki, BioPerl & PDL mailing lists
                * Social media from Will, Facebook [ DONE ]
                    * https://www.facebook.com/groups/perlprogrammers/posts/7567632603269462
                    * https://www.facebook.com/wnbjr/posts/pfbid023mjrkJaE7yY3EhdF9S81237vxaPdM3d1cJEjAk7BofcbpsEn3ceBh8scrk3siR4El
                    * https://www.facebook.com/perlcommunity/posts/pfbid0Vu2kdYRLgJJhWwSEyEu9uuDzkDKczE3FdgJU4yibepBdnPvf6hy9YJgoxPrpVaTWl
                * Social media from Will, Twitter [ DONE ]
                    * https://twitter.com/perlcommunity/status/1760842963125403904
                * Social media from Zaki, Mastodon
                * Social media from Zaki, LinkedIn
                * IRC.perl.org MAGnet chat from Zaki
                * IRC.libera chat from Zaki
                * TPRF Slack chat from Brett
                * HPC Social Discord chat from Brett
                * Perl Discord chat from Brett
                * Blogs.perl.org blogs from Brett [ DONE ]
                    * https://blogs.perl.org/users/oodler_577/2024/02/repost-tprc-2024-call-for-papers-is-now-open.html
                * Dev.to blogs from Zaki
                * Reddit website from Zaki
                * Perl Monks website from Brett
                * TPF website from Brett [ DONE ]
                    * https://news.perlfoundation.org/post/cfp2024
                * Perl Planet aggregator from Brett
                * Perl Weekly Newsletter aggregator from Brett [ DONE ]
                    * https://perlweekly.com/archive/656.html

        * Editorial Review Subcommittee
            * Need to start Subcommittee Hackathons every 2 weeks once we have our first paper submission
            * Brett invited George Baugh to become a member of Subcommittee with focus on mathematics, George accepted
            * Brett invited Marc Perry to become a member of Subcommittee with focus on bioinformatics, Marc accepts
                * Marc has experience reviewing papers in scientific journals
                * At normal conferences, everybody wants to give a talk but that is not possible, so everyone else needs to submit 1 or more abstracts
                * Committee selects what is hot, trending, or controversial to give a talk; everybody else gets to give a poster
                * Whoever gets to give a podium talk gets to publish a chapter in the proceedings which are NOT thoroughly peer-reviewed
                * About 1/3 of normal talks are problematic because it isn't ready yet or too low writing quality, and are thus rejected.
                * We want to reject as few talks as possible, by giving people a robust feedback process.
                    * Non-English speakers can use ChatGPT to help with grammar if necessary.
                * We don't have enough people submitting talks to quickly reject any without trying to help fix them up first.
                * We want to be as inclusive as possible to, so we want it to be easy for people to submit and approve papers.

        * Proceedings
            * If somebody submits a Science Perl talk but for some reason can't come in person, do they still get published in the Science Perl Journal?
                * Yes, we will still allow them to be published, with a notation that it was not given as a live presentation at the conference.

        * Websites
            * Homepage
                * perlcommunity.org/science
                * Need to publish
            * OJS Subdomain
                * science.perlcommunity.org/spj
                * Need e-mail deauthentication fix

        * Paper Ideas
            * Mark Perry, bioinformatics, doesn't want to write a paper this year but will serve on Editorial Review Subcommittee
            * Brett invited George Baugh to submit a paper
            * Brett will invite Chris Prather to submit a paper

    * RoboPerl Nonprofit
        * none

    * Science Perl Hackathon
        * Every 6 weeks
        * Next event 3/6/24, 7-9pm Central time zone, theme is Paper Review Process

* New Business
    * none

* Closing, 1:56pm
