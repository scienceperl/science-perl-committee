# Science Perl Committee
# Meeting Agenda & Minutes

## September 5th, 2024
## Twenty-Eighth Official Meeting

* Opening, 1:14pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Christos Argyropoulos, MD, PhD (kidney physician, genomic & biomarker discovery, big data sets, EHR, methodology dev for sequencing, new markers for kidney disease)
    * John Napiorkowski (maintainer of Catalyst)
    * Mickey Macten (online security and streaming multimedia)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 8/22/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Dr. Christos to be accepted as read, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of one officer only, not full voting quorum

* Officer Reports
    * Brett reports on the following:
        * TPF video staff says Dr. Christos' talk video is unusable due to audio reverb, however it may be fixable and Brett will check
        * Brett will also try to recreate the video track of scrolling through Manickam's paper, if the audio reverb can be fixed as w/ Dr. Christos'

    * Dr. O'Neil reports on the following:
        * Not present, sends regards
        * Met again today w/ Will regarding LPW plans
        * Submitting updated copy of chemometrics talk to LPW, along with a proposal for the Science Perl Track
        * Does want to work with Dr. Christos and Will on Grants Subcommittee of Fundraising Committee, for chemistry grant proposals

    * Will reports on the following:
        * Barnes & Noble requires a different file format for the covers & spine, can't use existing files directly, currently reworking
        * One paper required copyright modifications, need attributions for Creative Commons as references which are cited in each figure's caption 
        * Still need to collect more donations for Crossref DOI registration, $275 per year pro-rated
        * Still removing all undesirable logos
            * Dr. Christos says we need to register the trademarks for all our logos
        * NEED FINISH SPJ preprint sales on FB before opening sales of full edition

* Old Business

    * Science Track

        * General Next Steps  [ NO CHANGE 20240905 ]
            * Create timeline of each step of the paper submission / review / approval process  [ DONE ]
            * Publish Science CFP on perlcommunity.org/science   [ DONE ]
            * Distribute Science CFP via e-mail, Facebook, Twitter, TPF announcements, etc   [ DONE ]
            * Distribute Science CFP via IRC (Magnet #perl & #pdl, Libera #perl), Reddit, and remaining avenues [ DONE ]
            * Promoting & requesting paper submissions from those on the Paper Idea list [ DONE ]
            * Send TPC Planning Committee our list of approved science papers ASAP, so they can review & coordinate with other track scheduling [ DONE ]
            * Start receiving & reviewing paper submissions in OJS [ DONE ]
            * Finish reviewing papers in OJS  [ DONE ]
            * Publish preprint edition [ DONE ]
            * Finalizing papers for SPJ full edition, NEED FINISH ASAP
            * Publish SPJ full edition
            * Science Perl Track is a travelling track with its own identity, it can go to any conference world-wide, start booking more conferences

        * Remote Presentations  [ NO CHANGE 20240905 ]
            * We will do everything possible to bring Science Track talks in person, and when not possible will allow remote talks
            * Acceptable reasons for remote talks
                * Legal
                * Medical
                * Financial
            * Need official remote talk policy

        * Perl Community Conference, Winter 2024
            * NEED OFFICIAL CONFERENCE ANNOUNCEMENT
            * Perl Birthday Party 12/18/2024
            * Dr. Christos as Conference MC
            * One day online-only virtual conference
            * One track only, one speaker at a time
            * Free conference, donations only
            * Use Google Meet w/ maximum 100 participants, ask for feedback after conference
            * Allow 3 different talk lengths, corresponding to 3 different paper lengths
                * Full-length talks, 50 minutes
                * Short-length talks, 20 minutes
                * Lightning talks, 5 minutes
            * Use OJS as registration for conference presenters
            * Use existing mailing list as registration for conference attendees

        * Call For Papers, Winter 2024
            * NEED UPDATE PAPER LENGTHS BELOW ON SPJ WEBSITE, ALSO NUMBER OF REFERENCES PER EACH
            * Print all 3 talks lengths from Science Perl Conference
                * Full-length paper, 10 - 35 pages max
                * Short-length paper, 2 - 9 pages
                * Extended abstract, 1 page max

        * Paper Submission Timeline, Winter 2024
            * Abstracts submitted by September 30th
            * Abstracts approved or declined by October 4th
            * 1st drafts of papers & posters submitted by November 4th
            * 1st drafts feedback sent back to submitters by November 15th
            * Final drafts submitted by December 2nd
            * Final drafts approved or declined by December 9th

        * Paper Ideas, Winter 2024 & Summer 2025  [ NO CHANGE 20240905 ]
            * Will, PerlGPT Part 2 or Navi AI
            * Brett, OpenMP Read-Only API for Perl Data Structures
            * Dimitris Kechagias, iOS weather app for astronomers Xasteria, proxy servers it uses for the weather forecasts are in Perl
            * Dr. Russell, genetic algorithms, currently changing to new job w/out previous job restriction
            * Dr. Christos, using Perl to memory management for buffers in other languages such as assembly (lightning talk); also another talk about FOO (20 min talk)
            * John Napiorkowski, Catalyst and/or Valiant
                * Pick an old application and convert into web application
                * Dr. Christos recommends some old C code program called from the command line used for bioinformatics
                    * github.com/jeffdaily/parasail

        * Editorial Review Subcommittee
            * NEED UPDATE OJS & WEBSITE, submitters must create their presentation slides to match their paper or poster content
            * Reviewers
                * Currently reviewers are Dr. Adam Russell, Dr. Casiano Rodriguez Leon, Will Braswell, and Brett Estrade
                * Need more reviewers for 2025, at least 2 per paper
            * OJS
                * Need fix OJS e-mail de-authentication issue, old bug found by Brett & Zaki
                * NEED BRETT & DR. RUSSELL TO REPRODUCE THE BUGS BELOW
                * upload errors, 2 different problems where upload buttons do or do not appear, affecting Manickam & Will & Dr. Russell, full report sent to Brett for further investigation into server logs and GitHub bug reports
            * Editor's Choice Award
                * We will use Dr. Perry's spreadsheet w/ initial selection rubric for Winter 2024 and update as needed
            * Short papers
                * Yes we will accept both short papers (2 - 9 pages) and even extended abstracts (1 page)
                * IEEE guidelines for full-length is 20 to 25 pages, no more than 35 pages including references
                * NEED DECIDE NUMBER OF REFERENCES FOR SHORT PAPERS & EXTENDED ABSTRACTS

        * Proceedings Subcommittee  [ NO CHANGE 20240905 ]
            * NEED BOOK SPINE INFO, print SPJ info on the book spine
            * Authors get a free printed copy of SPJ, others can purchase a printed copy
            * Science Perl mailing list new members will get free PDF copy of SPJ, others can download PDF with prominent donation button
            * Funding
                * Donation ware online copy
                * Paid physical copies
                * Special signed copies
            * Latex Formatting
                * Brett will create a Latex template for everyone to use in 2025
                * Brett will try to find a way to format the Perl code nicer than it is now
            * Publication
                * We made Conference Preprint Edition in limited run
                * We offered signed copies for sale at higher price
                * NEED FINISH FINAL EDITION
            * Barnes & Noble
                * Now approved for self-publishing nonprofit org account 
                * Price much lower per copy than any other option
            * Need to have committee representative selling SPJ copies at all Perl conferences:
                * TPC::NA
                    * http://tprc.us
                    * 6/??/2025
                    * Need revisit after TPF problems are addressed
                * YAPC::Japan
                    * https://yapcjapan.org
                    * 10/5/2024
                    * George Baugh
                    * NEED JOURNAL COPIES TO SELL
                    * NEED SCIENCE PERL TRACK
                * LPW
                    * https://act.yapc.eu/lpw2024
                    * 10/26/2024
                    * Dr. O'Neil
                    * NEED FOLLOW UP TO GET TALK SUBMITTED
                    * NEED JOURNAL COPIES TO SELL
                    * NEED SCIENCE PERL TRACK
                * FOSDEM
                    * https://fosdem.org
                    * 2/??/2025
                    * Will & Brett
                    * NEED MONEY FOR TICKET
                    * NEED PERL BOOTH
                    * NEED SUBMIT TALKS
                    * NEED SCIENCE PERL TRACK
                * YAPC::India
                    * NEED CREATE
                * YAPC::Asia (DEFUNCT)
                    * https://yapcasia.org
                    * Last conference 2015
                    * Need revive?
                * YAPC::EU (DEFUNCT)
                    * http://www.yapceurope.org/events/conferences.html
                    * Last Conference 2019
                    * Need revive?
                * YAPC::Russia (DEFUNCT)
                    * https://yapcrussia.org
                    * Last Russian Perl Workshop 2019
                    * Need revive?
                * YAPC::Australia (DEFUNCT)
                    * https://osdc.com.au/
                    * Last Conference 2004?
                    * Need revive?

        * Websites  [ NO CHANGE 20240905 ]
            * Homepage
                * perlcommunity.org/science
                * perlcommunity.org/awards  NEED FINISH
                * NEED UPDATE HACKATHON DATES 7/10 should be 7/17
                * NEED UPDATE 2025 PAPER DATES
            * OJS Subdomain
                * science.perlcommunity.org/spj
                * Updated OJS website with 2025 paper submission timeline dates
                * NEED E-MAIL DEAUTHENTICATION FIX

        * Mailing List  [ NO CHANGE 20240905 ]
            * One-way announcements only
            * Sendy, self-hosted by Brett
            * Request all committee members to register
                * perlcommunity.org/science#mailing_list

    * Science Perl Hackathon  [ NO CHANGE 20240905 ]
        * Every 6 weeks
        * Last event 8/21/24, 7:30-9:30pm Central time zone, theme was Science Perl Journal Latex
        * Next event 10/3/24, 7:00-9:00pm Central time zone, theme is Conference Representatives Preparation
            * Need George & Dr. O'Neil

    * Treasurer's Report [ NO CHANGE 20240905 ]
        * We have enough money for Crossref
        * Need the Fundraising Committee to start collecting donations for 2025 activities
        * NEED PAY PRORATED CROSSREF
        * NEED BRETT RECEIPTS
        * NEED UPDATE SPREADSHEET WITH SPJ SALES & TPC RECEIPTS

    * 2nd Annual Perl Committee Convention, 2025  [ NO CHANGE 20240905 ]
        * NEED DETERMINE DATE & LOCATION
        * NEED REVIEW 7/17/24 HACKATHON NOTES FOR 2024 CONVENTION DEBRIEF

    * RoboPerl Nonprofit
        * none

* New Business

    * Professional Perl Association
        * Combine Brett's Perl Guild idea into PPA
        * John as Chairman
        * Brett as Vice Chairman of Communications
            * ShinyCMS Forum / Blog / News / Shop etc
                * NEED WORK W/ BRETT TO GET SHINY INSTALLED
            * Discord, use Dr. Russell's group; Discord is invite-only but invite link can be shared by members to non-members
            * Slack, need create new group
            * Reddit, need create new group
            * Facebook, use current groups
            * IRC, need create new channels?
        * Need to get important Perl programmers signed up before public release, can offer them 1-5 year free membership

* Closing, 2:22pm
