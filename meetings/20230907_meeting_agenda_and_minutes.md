# Science Perl Committee
# Meeting Agenda & Minutes

## September 7th, 2023
## Third Official Meeting

* Opening, 1:05pm

* Attendance & Introductions
    * Will Braswell (computer scientist, creator of Navi AI)
    * Zakariyya Mughal (biomedical imaging, BioPerl, PDL)
    * Christos Argyropoulos (kidney physician, genomic & biomarker discovery, big data sets, EHR, methodology development for sequencing, new markers for kidney disease)
    * John Napiorkowski (involved in Perl since 90's, helping Perl make people happy and making Perl competitive)
    * Adam Russell (computer scientist, dissertation in computational geometry in Perl, leads group United Health Group subsidiary OptumAI, business AI automating building processes & reviewing documents)
    * Antanas Vaitkus (Lithuania, informatics & bioinformatics, crystallography, open database software in Perl)
    * John Kirk (no intro, microphone problems)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings
    * Chairman Brett Estrade unable to attend today, 1st Vice Chairman Zaki Mughal presiding

* Previous Meeting's Minutes
    * 8/24/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by John Napiorkowski to be accepted as read, voted unanimously

* Old Business

    * TPC 2024 3rd Track
        * Call For Papers, can't be published until Science Track is approved by TPF
            * https://gist.github.com/zmughal/5047eb588e8b3cb6de9b12864e99e97b
        * TPF report
            * Zaki and Brett talked to TPF, they want us to work with them in the planning, Brett agrees and that is why we are in communication with them
            * We don't want to do all the work and then have TPF cancel on us
            * We are different than normal talks because we are focused on academics, we require the submission of a paper or poster, and we will publish proceedings
        * BOSC 2023 Conference
            * https://www.open-bio.org/events/bosc-2023/
            * Chris Fields on BOSC committee is also maintainer of BioPerl
            * Zaki is on BioPerl mailing list, will contact Chris Fields when we are ready for CFP
            * Not a stand-alone conference, always run alongside another conference such as ISMB (Intelligent Systems in Molecular Biology)
            * Need to learn from BOSC conference organization, etc
            * Having a special conference like our Science Perl track is not a new, can show TPF that BOSC is successful
        * Editorial Review Committee
            * no comments
        * Proceedings
            * Adam Russell asked if we will publish proceedings, yes we will which is one of the things to differentiate us from normal talks
            * We will make proceedings PDF available to all presenters
            * We can use print-on-demand for people who want to order online
            * We can print about 10 copies and bring for people who want to buy them in person, could be a small fundraiser
            * Adam Russell requests that we have the JOSS (Journal of Open Source Software) publish our proceedings
                * Will make us indexed and more searchable online
                * Will give us DOI (digital object identifier)
        * Websites
            * Will Braswell needs to update perlcommunity.org
        * Paper ideas
            * Christos Argyropoulos (Nextflow)
            * Christos Argyropoulos (TensorFlow Recommender)

* New Business

    * Science Perl Hackathon
        * Weds 9/20 7-9pm Central time zone
        * Moved by Will Braswell, seconded by John Napiorkowski, voted unanimously
        * Will B will create FB event, Brett & Zaki will make comments to create event focus on Science Track
        * Christos asks if we are open to any ideas for the Hackathon, paper over this past summer about recommender systems for biomedical applications, has no dedicated implementation in any language, uses TensorFlow in the paper
        * John Napiorkowski says we will need more time than 2 hours if we have break-out groups, could do week-long projects if enough people join

    * Bylaws Open Discussion
        * Tabled until next meeting

* Closing, 1:50pm
