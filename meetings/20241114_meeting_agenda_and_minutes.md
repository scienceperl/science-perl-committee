# Science Perl Committee
# Meeting Agenda & Minutes

## November 14th, 2024
## Thirty-Third Official Meeting

* Opening, 1:18pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Mickey Macten (online security and streaming multimedia)
    * Dr. Drew O'Neil (AKA Andrew, uses Perl AI for chemometrics)
    * Mohammed Zia (uses Perl for regular expressions etc)
    * Joshua Oliva (interested in scientific breakthroughs, physics, health topics)
    * Joshua Day (industrial tooling)
    * John Napiorkowski (maintainer of Catalyst)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 10/31/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Dr. O'Neil to be accepted as amended, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of one officer only, not full voting quorum

* Officer Reports
    * Brett reports on the following:
        * Sent out e-mails to SPJ issue #2 authors
        * Using new Overleaf template, should make it easier for everyone
        * Hackathon last night, Overleaf using new template for Spider Silk paper, etc.
        * Created new Reddit board
            * https://www.reddit.com/r/perlcommunity/
            * Current mods include Brett & Dr. Christos
            * Invited mods include Dr. Russell & John Napiorkowski
        * Talked with Rex maintainer Erki Ferenc
            * Very supportive of SPJ etc
            * Brett & Erki will collaborate on extended abstract about Rex Beowulf Clusters
        * SPJ promotions
            * News
                * Hacker News
                    * https://news.ycombinator.com/edit?id=42133191
                * Ars Technica
                * Phoronix
                * Slashdot
                * Linux Today
            * Social Media
                * LinkedIn
                    * NEED UPDATE SCIENCE PERL GROUP
                * ... other social media sites
        * NEED TO SEND SPJ FLIER TO MOHAMMED ZIA

    * Dr. O'Neil reports on the following:
        * LPW follow-up
            * Edited Science Talk videos
                * Used Kdenlive for video editing
                * Cut out time gaps and technical difficulties
                * Added cover and closing slides with Science Perl Committee info
                * Lee Johnson also added LPW logo slides & enhanced audio
                * All 6 videos now uploaded to LPW YouTube Channel
                    * Dr. Andrew J. O'Neil
                        * Chemometrics with Perl & Pharmaceutical Applications
                        * https://www.youtube.com/watch?v=1LebIdYAGjI
                    * Dr. Luis Mochán
                        * Simulating Meta-Materials using Photonic in Perl
                        * https://www.youtube.com/watch?v=ejR6_Gw3SQw
                    * John Napiorkowski
                        * Using Catalyst Per Context Components
                        * https://www.youtube.com/watch?v=sn7qO8xrzoI
                    * Dr. Christos Argyropoulos
                        * Performant Data Reductions with Perl
                        * https://www.youtube.com/watch?v=u_CkgLTeR4g
                    * Dr. Boyd Duffee
                        * PDL for the Impatient
                        * https://www.youtube.com/watch?v=mUco0dlxZbI
                    * Brett Estrade
                        * OpenMP & PDL
                        * https://www.youtube.com/watch?v=spA4g9CPpM0
                * Can eventually create our own OBS template
                * Can eventually enhance audio ourselves using Kdenlive
        * Contacted Dr. Duffee about creating "PDL for the Impatient" extended abstract for SPJ issue #2, still waiting for response

    * Will reports on the following:
        * Our nonprofit has now filed an "Assumed Name" AKA "Doing Business As" under the new name "Perl Community"
            *   CORRECT: The Perl Community is foo bar...
            * INCORRECT: The Perl community is foo bar...
            *   CORRECT: Thanks to the Perl Community we foo bar...
            * INCORRECT: Thanks to The Perl Community we foo bar...
            * INCORRECT: Thanks to the Perl community we foo bar...
        * SPJ sales update
            * Working w/ Brett to post announcements on tech news websites & social media, Hacker News so far
            * Working w/ Dr. Russell to work on sales with universities & librairies, he just received his 5 copies in the mail
            * Customers outside of North America must use MyUS.com 3rd-party shipping service
                * SPJ website updated
                * Not going to update B&N product description, don't want to accidentally violate any 3rd-party link violations etc.
        * SPJ publication
            * NEED RELEASE abstracts next via OJS
            * Black & white copy next
            * SPJ issue #2 after that
            * 6 months after initial paper publication of each issue, release eBook & Online Edition via OJS
                * DOES ALLOW PIRACY!!
                * Subscription model
            * 12 months after initial paper publication of each issue, release all papers for free via OJS
        * Still need to collect more donations for Crossref DOI registration, $275 per year pro-rated
        * Still removing all undesirable logos
            * Need to register the trademarks for all our logos

* Old Business

    * Science Track

        * General Next Steps
            * Create timeline of each step of the paper submission / review / approval process  [ DONE ]
            * Publish Science CFP on perlcommunity.org/science   [ DONE ]
            * Distribute Science CFP via e-mail, Facebook, Twitter, TPF announcements, etc   [ DONE ]
            * Distribute Science CFP via IRC (Magnet #perl & #pdl, Libera #perl), Reddit, and remaining avenues [ DONE ]
            * Promoting & requesting paper submissions from those on the Paper Idea list [ DONE ]
            * Start receiving & reviewing paper submissions in OJS [ DONE ]
            * Finish reviewing papers in OJS
            * Publish preprint edition
            * Finalizing papers for SPJ full edition
            * Publish SPJ full edition

        * Perl Community Conference, Winter 2024
            * HYBRID IN-PERSON & VIRTUAL CONFERENCE
            * Registration now open!
                * https://www.meetup.com/austin-perl-mongers/events/304573306/
                * https://www.facebook.com/events/1103216794572416/
            * One day only, 10:30am to 4:30pm Central Time Zone
            * Perl's 37th Birthday Party Wednesday, 12/18/2024
            * See Google Spreadsheet or OJS for list of talks
            * Dr. Christos as Conference MC
            * One track only, one speaker at a time
            * Free conference, donations only
                * NEED UPDATE SPJ WEBSITE W/ SQUARE & VENMO & PAYPAL ETC
            * Use Google Meet w/ maximum 100 participants, ask for feedback after conference
            * Allow 3 different talk lengths, corresponding to 3 different paper lengths
                * Full-length talks, 50 minutes
                * Short-length talks, 20 minutes
                * Lightning talks, 5 minutes
            * Use OJS as registration for conference presenters
            * Use Meetup & Facebook as registration for conference attendees
            * Use existing mailing list to send info to conference attendees
            * Need announce summer 2025 conference during winter 2024 conference
                * NEED CHOOSE SUMMER 2025 DATE & LOCATION (Austin again?)
            * POSSIBLE EXTRA ACTIVITIES FOR SUMMER 2025 CONFERENCE
                * Mini Job Fair
                    * NEED SCHEDULE ONE JOB OPENING FOR AI PERL TENSORFLOW WINTER 2024!
                    * NEED CREATE JOB POSTING FOR COMMISSION-ONLY RECRUITER/STAFFER JOB MANAGER
                    * NEED STATE OF ONION ADDRESS, SPECIFICALLY ADDRESS LACK OF PERL JOBS
                    * NEED CONTACT JOBS.PERL.ORG & GOOGLE OPENINGS
                * Mini Investor Pitch
                    * John & Will did not get any reply from investor contacts
                    * NEED JOHN REPLY FROM POSSIBLE INVESTORS

        * Paper Submission Timeline, Winter 2024  [ NO CHANGE 20241114 ]
            * Abstracts submitted by September 30th
            * Abstracts approved or declined by October 4th
            * 1st drafts of papers & posters submitted by November 4th
            * 1st drafts feedback sent back to submitters by November 15th
            * Final drafts submitted by December 2nd
            * Final drafts approved or declined by December 9th

        * Paper Ideas, Winter 2024 & Summer 2025  [ NO CHANGE 20241114 ]
            * NEED MOVE TO PAPER TRACKER GOOGLE SPREADSHEET
            * Will, Full Talk, PerlGPT Part 2
            * Will, Lightning Talk, Navi AI
            * Brett, 2024 Lightning Talk, 3 Perl Virtues
            * Brett, 2024 Medium Talk, OpenMP Thread Safety
            * Brett, 2024 Medium Talk, Perl Sequential Consistency
            * Brett, 2025 Full Talk, OpenMP Read-Only API for Perl Data Structures
            * Dimitris Kechagias, iOS weather app for astronomers Xasteria, proxy servers it uses for the weather forecasts are in Perl
            * Dr. Russell, 2025 Full Talk, genetic algorithms, currently changing to new job w/out previous job restriction
            * Dr. Russell, 2024 Medium Talk?, Cowl ontology for CPAN
            * Dr. Christos, using Perl to memory management for buffers in other languages such as assembly (lightning talk); also another talk about FOO (20 min talk)
            * John Napiorkowski, 2024 LPW Lightning Talk, Catalyst Valiant To-Do List Application, Beta Demo
            * John Napiorkowski, 2024 Advent & PCC Lightning Talk, Catalyst Valiant To-Do List Application, Full Demo
            * John Napiorkowski, 2025 Advent & PCC Lightning Talk, Catalyst Valiant To-Do List Application, AI Demo
            * Bartosz Jarzyna, 2025 PCC Full Talk?, Bitcoin
            * Dr. Luis Mochan, 2024 LPW Lightning Talk & PCC Full Talk?, Photonic
            * Dr. O'Neil, 2024 PCC Extended Abstract / Lightning Talk (Possible Short Paper), Comparing Machine Learning Clustering Algorithms for Spectrographic Data

        * Editorial Review Subcommittee  [ NO CHANGE 20241114 ]
            * Reviewers
                * Currently reviewers are Dr. Adam Russell, Dr. Casiano Rodriguez Leon, Will Braswell, and Brett Estrade
                * Need more reviewers for 2025, at least 2 per paper
            * OJS
                * Need fix OJS e-mail de-authentication issue, old bug found by Brett & Zaki
                * NEED BRETT & DR. RUSSELL TO REPRODUCE THE BUGS BELOW
                * upload errors, 2 different problems where upload buttons do or do not appear, affecting Manickam & Will & Dr. Russell, full report sent to Brett for further investigation into server logs and GitHub bug reports
            * Editor's Choice Award
                * We will use Dr. Perry's spreadsheet w/ initial selection rubric for Winter 2024 and update as needed
            * Short papers
                * Yes we will accept both short papers (2 - 9 pages) and even extended abstracts (1 page)
                * IEEE guidelines for full-length is 20 to 25 pages, no more than 35 pages including references
                * NEED DECIDE NUMBER OF REFERENCES FOR SHORT PAPERS & EXTENDED ABSTRACTS

        * Proceedings Subcommittee  [ NO CHANGE 20241114 ]
            * Authors get a free printed copy of SPJ, others can purchase a printed copy
            * Science Perl mailing list new members will get free PDF copy of SPJ, others can download PDF with prominent donation button
            * Funding
                * Donation ware online copy
                * Paid physical copies
                * Special signed copies
            * Latex Formatting
                * Brett will create a Latex template for everyone to use in 2025
                * Brett will try to find a way to format the Perl code nicer than it is now
            * Publication
                * We made Conference Preprint Edition in limited run
                * We offered signed copies for sale at higher price
                * NEED FINISH FINAL EDITION
            * Barnes & Noble
                * Now approved for self-publishing nonprofit org account 
                * Price much lower per copy than any other option
            * Need to have committee representative selling SPJ copies at all Perl conferences:
                * TPC::NA
                    * http://tprc.us
                    * 6/??/2025
                    * Need revisit after TPF problems are addressed
                * YAPC::Japan
                    * https://yapcjapan.org
                    * 10/5/2024
                    * George Baugh 2025
                    * NEED JOURNAL COPIES TO SELL
                    * NEED SCIENCE PERL TRACK
                * LPW
                    * https://act.yapc.eu/lpw2024
                    * 10/26/2024
                    * Dr. O'Neil
                    * Science Perl Talks approved
                    * NEED JOURNAL COPIES TO SELL
                * FOSDEM
                    * https://fosdem.org
                    * 2/??/2025
                    * Will & Brett
                    * NEED MONEY FOR TICKET
                    * NEED PERL BOOTH
                    * NEED SUBMIT TALKS
                    * NEED SCIENCE PERL TRACK
                * YAPC::India
                    * NEED CREATE
                * YAPC::Asia (DEFUNCT)
                    * https://yapcasia.org
                    * Last conference 2015
                    * Need revive?
                * YAPC::EU (DEFUNCT)
                    * http://www.yapceurope.org/events/conferences.html
                    * Last Conference 2019
                    * Need revive?
                * YAPC::Russia (DEFUNCT)
                    * https://yapcrussia.org
                    * Last Russian Perl Workshop 2019
                    * Need revive?
                * YAPC::Australia (DEFUNCT)
                    * https://osdc.com.au/
                    * Last Conference 2004?
                    * Need revive?

        * Websites  [ NO CHANGE 20241114 ]
            * Homepage
                * perlcommunity.org/science
                * perlcommunity.org/awards  NEED FINISH
                * NEED UPDATE HACKATHON DATES 7/10 should be 7/17
                * NEED UPDATE 2025 PAPER DATES
            * OJS Subdomain
                * science.perlcommunity.org/spj
                * Updated OJS website with 2025 paper submission timeline dates
                * NEED E-MAIL DEAUTHENTICATION FIX

        * Mailing List  [ NO CHANGE 20241114 ]
            * One-way announcements only
            * Sendy, self-hosted by Brett
            * Request all committee members to register
                * perlcommunity.org/science#mailing_list

    * Science Perl Hackathon
        * Every 6 weeks
        * Last event 11/13/24, 7:00-9:00pm Central time zone, theme was Science Perl Journal Latex & Sales
            * Worked on SPJ Spider Silk latex & tech news sales blurb etc
        * Next event 12/25/24, 7:00-8:00pm Central time zone, theme is CHRISTMAS PARTY!!!

    * Treasurer's Report [ NO CHANGE 20241114 ]
        * We have enough money for Crossref
        * Need the Fundraising Committee to start collecting donations for 2025 activities
        * NEED PAY PRORATED CROSSREF
        * NEED BRETT RECEIPTS
        * NEED UPDATE SPREADSHEET WITH SPJ SALES & TPC RECEIPTS

    * 2nd Annual Perl Committee Convention, 2025  [ NO CHANGE 20241114 ]
        * NEED DETERMINE DATE & LOCATION
        * NEED REVIEW 7/17/24 HACKATHON NOTES FOR 2024 CONVENTION DEBRIEF

    * RoboPerl Nonprofit
        * New DBA filed for "Perl Community"

* New Business

    * none

* Closing, 2:46pm
