# Science Perl Committee
# Meeting Agenda & Minutes

## May 30th, 2024
## Twenty-Second Official Meeting

* Opening, 1:59pm

* Attendance & Introductions
    * Will Braswell (computer scientist, creator of Navi AI)
    * John Napiorkowski (maintainer of Catalyst)
    * Mickey Macten (online security and streaming multimedia)

* Announcements
    * Chairman Brett Estrade requests 2nd Vice Chairman Will Braswell to preside in his absence
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 5/16/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by John Napiorkowski to be accepted as corrected, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of one officer only, not full voting quorum

* Officer Reports
    * Brett reports on the following:
        * Science Perl Track talks are now on the official TPC schedule
            * https://tprc2024.sched.com/
        * Authors, please review and confirm your spot on the schedule ASAP

    * Will reports on the following:
        * Hackathon last night (see Hackathon report below)
        * Dr. Marc Perry added as OJS reviewer on all papers received (see Editorial Review Subcommittee report below)
        * Science Perl Committee bank account opened (see Treasurer's report below)
        * Estimated TPC 2024 budget is $1,200 (see Treasurer's report below)
            * Still working to get better estimates for Science Perl Journal publication, etc.
        * Need to start receiving donations ASAP (see Treasurer's report below)

* Old Business

    * TPC 2024 Science Track

        * General Next Steps  [ NO CHANGE 20240530 ]
            * Fix OJS e-mail de-authentication issue
            * Create timeline of each step of the paper submission / review / approval process  [ DONE ]
            * Publish Science CFP on perlcommunity.org/science   [ DONE ]
            * Distribute Science CFP via e-mail, Facebook, Twitter, TPF announcements, etc   [ DONE ]
            * Distribute Science CFP via IRC (Magnet #perl & #pdl, Libera #perl), Reddit, and remaining avenues [ DONE ]
            * Promoting & requesting paper submissions from those on the Paper Idea list [ DONE ]
            * Start receiving & reviewing paper submissions in OJS [ ONGOING ]
            * Finish reviewing papers in OJS 
            * Send TPC Planning Committee our list of approved science papers ASAP, so they can review & coordinate with other track scheduling

        * Remote Presentations
            * Received official approval for Dr. Christos remote talk 2024
            * Will not attempt to get TPF approval for Dr. O'Neil remote talk 2024, will keep that as emergency backup plan only

        * Paper Submission Timeline
            * Abstracts submitted by April 5th  [ DONE ]
            * Abstracts approved or declined by April 15th  [ DONE ]
            * Extended CFP, abstracts submitted by April 20th  [ DONE ]
            * Extended CFP, abstracts approved or declined by April 30th  [ DONE ]
            * Extended 1st drafts of papers & posters submitted by May 20th  [ DONE ]
            * 1st drafts feedback sent back to submitters by May 31st
            * Final drafts submitted by June 7th
            * Final drafts approved or declined by June 15th

        * Call For Papers  [ NO CHANGE 20240530 ]
            * CFP now closed

        * Editorial Review Subcommittee
            * NEED BYLAW, delay any formal motion to officially create a subcommittee until in-person convention
            * NEED UPDATE OJS & WEBSITE, submitters must create their presentation slides to match their paper or poster content
            * Reviewers
                * Accepted input from all committee members, only committee officers may vote to approve/reject, need full quorum for tiebreakers
                * Currently reviewers are Brett Estrade, Will Braswell, and Dr. Marc Perry
            * George's Paper
                * We fully reviewed & approved the 1st draft during the 5/29 Science Perl Hackathon

        * Proceedings Subcommittee
            * NEED CREATE PLAN FOR PRINTING BEFORE TPC
            * NEED BYLAW, same as Editorial Review Subcommittee above
            * Current authors will be offered a printed copy of SPJ at cost
            * Non-authors will be offered a printed copy of SPJ at a fair market mark-up as part of our annual fundraising, to finance printing next year
            * Those who sign up for Science Perl mailing list will get free PDF copy of SPJ 
            * Put PDF copy of SPJ online for free download, with prominent donate button for anyone who is not a student, works based on the honor system
            * Funding
                * We now have a bank account (see Treasurer's report below)
            * Latex Formatting
                * Per Dr. O'Neil's recommendation, we will use the "Vancouver" style reference formatting using numbers (not "Harvard" style)
                    * \bibliographystyle{plain}
            * Publication
                * Will started looking at where we can print spiral-bound journals

        * Websites  [ NO CHANGE 20240530 ]
            * Homepage
                * perlcommunity.org/science
                * NEED UPDATE WEBSITE WITH PAPER SUBMISSION TIMELINE DATES
            * OJS Subdomain
                * science.perlcommunity.org/spj
                * NEED E-MAIL DEAUTHENTICATION FIX

        * Mailing List  [ NO CHANGE 20240530 ]
            * One-way announcements only
            * Sendy, self-hosted by Brett
            * Request all committee members to register
                * perlcommunity.org/science#mailing_list

        * Paper Ideas  [ NO CHANGE 20240530 ]
            * none

    * 1st Annual Perl Committee Convention
        * NEED ASK TPC for BoF room reservation
        * NEED DRAFT BYLAWS
        * During TPC in Las Vegas, confirmed date Tuesday 6/25/2024, 7-9pm PACIFIC TIME ZONE, TIME TO BE CONFIRMED W/ TPC
        * Need to create & ratify bylaws
        * Celebrating with awards and pizza!

    * RoboPerl Nonprofit
        * Board of Directors approved new savings account for Science Perl Committee (see report below)

    * Science Perl Hackathon
        * Every 6 weeks
        * Last event 5/29/24, 7-9pm Central time zone, theme is Final Paper Approval
            * Several Science Perl authors attended
            * Discussed many topics relating to the Science Perl Track and Science Perl Journal
            * Full in-depth read-through, review, and feedback session for George Baugh's paper about Perl software provability
        * Next event 7/17/24, SPECIAL DATE, 7:00-9:00pm Central time zone, theme is Science Track 2024 Debrief

* New Business

    * Treasurer's Report
        * New savings account created at Amplify Federal Credit Union for the exclusive use of the Science Perl Committee, sponsored (and soon to be officially chartered by) the nonprofit organization Public Enrichment and Robotics Laboratories ("P.E.R.L." or "RoboPerl" for short)
        * Initial $5 deposit made by Will Braswell, required for account to stay open
        * Our estimated budget for The Perl Conference 2024 is very roughly $1200 (including tax & misc), and is comprised of:
            * Digital Object Identifier, $250 + tax
            * Overleaf Latex Platform, $200 + tax
            * Science Perl Journal Publication, $400 + tax
            * Perl Committee Convention
                * Food, $100
                * Awards, $150
        * We are now able to accept your contributions directly via:
            * Wire transfer
            * Check
            * Money order
        * We can accept contributions indirectly via:
            * PayPal
            * Venmo 
            * Zelle 
        * Our preferred method is wire transfer, but any of them will work.

* Closing, 2:24pm
