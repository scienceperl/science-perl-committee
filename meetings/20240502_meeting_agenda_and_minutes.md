# Science Perl Committee
# Meeting Agenda & Minutes

## May 2nd, 2024
## Twentieth Official Meeting

* Opening, 1:06pm

* Attendance & Introductions
    * Will Braswell (computer scientist, creator of Navi AI)
    * Adam Russell, PhD (computer scientist, leads group United Health Group subsidiary OptumAI, developing new products to bring AI & ML to admin workers)
    * Roy Hubbard (user of Catalyst)
    * Mike Flannigan (interested in math & science in general, also astronomy & space program, geographic information systems (GIS))
    * John Napiorkowski (maintainer of Catalyst)

* Announcements
    * Chairman Brett Estrade requests 2nd Vice Chairman Will Braswell to preside in his absence
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 4/18/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Adam Russell to be accepted as modified, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of two (or one???) officers only, not full voting quorum

* Officer Reports
    * Brett reports on the following:
        * Brett is currently away at an Oceanography Boot Camp, he is presenting a talk that will serve as the basis for his Science Track paper

    * Will reports on the following:
        * Will & John attended the TPF community reps meeting Friday 4/19, we once again requested formal approval for Dr. Christos' remote talk, we were once again told to wait while they discuss
        * TPF asked Will to provide a Science Committee write-up for the 2024 TPF Prospectus document, done & submitted with approval of Brett & all PhD/MDs
        * TPF approved Will for non-academic talk at TPC, "State of the Onion AI"

* Old Business

    * TPC 2024 Science Track

        * General Next Steps [ NO CHANGE 20240502 ]
            * Fix OJS e-mail de-authentication issue
            * Create timeline of each step of the paper submission / review / approval process  [ DONE ]
            * Publish Science CFP on perlcommunity.org/science   [ DONE ]
            * Distribute Science CFP via e-mail, Facebook, Twitter, TPF announcements, etc   [ DONE ]
            * Distribute Science CFP via IRC (Magnet #perl & #pdl, Libera #perl), Reddit, and remaining avenues [ DONE ]
            * Promoting & requesting paper submissions from those on the Paper Idea list [ DONE ]
            * Start receiving & reviewing paper submissions in OJS [ ONGOING ]
            * Finish reviewing papers in OJS 
            * Send TPC Planning Committee our list of approved science papers ASAP, so they can review & coordinate with other track scheduling

        * Remote Presentations
            * NEED GET TPF APPROVAL for Dr. Christos remote talk 2024
            * NEED GET TPF APPROVAL for Dr. O'Neil remote talk 2024

        * Paper Submission Timeline
            * Abstracts submitted by April 5th  [ DONE ]
            * Abstracts approved or declined by April 15th  [ DONE ]
            * Extended CFP, abstracts submitted by April 20th  [ DONE ]
            * Extended CFP, abstracts approved or declined by April 30th  [ DONE ]
            * 1st drafts of papers & posters submitted by May 15th
            * 1st drafts feedback sent back to submitters by May 31st
            * Final drafts submitted by June 7th
            * Final drafts approved or declined by June 15th

        * Call For Papers
            * CFP now closed

        * Editorial Review Subcommittee  [ NO CHANGE 20240502 ]
            * NEED BYLAW, delay any formal motion to officially create a subcommittee until in-person convention
            * NEED UPDATE OJS & WEBSITE, submitters must create their presentation slides to match their paper or poster content
            * NEED SEND APPROVAL for extension request from Dr. Christos for May 25th to submit 1st draft of paper
            * NEED OJS ADMIN ACCOUNT for Dr. Adam Russell to review papers
            * Accepted input from all committee members, only committee officers may vote to approve/reject, need full quorum for tiebreakers
            * Unanimously approved all paper submissions, final count is 9 papers

        * Proceedings Subcommittee   [ NO CHANGE 20240502 ]
            * NEED CREATE PLAN FOR PRINTING BEFORE TPC
            * NEED BYLAW, same as Editorial Review Subcommittee above
            * Current authors will be offered a printed copy of SPJ at cost
            * Non-authors will be offered a printed copy of SPJ at a fair market mark-up as part of our annual fundraising, to finance printing next year
            * Those who sign up for Science Perl mailing list will get free PDF copy of SPJ 
            * Put PDF copy of SPJ online for free download, with prominent donate button for anyone who is not a student, works based on the honor system
            * We should use the RoboPerl nonprofit bank accounts as needed, can open a separate Science Perl account if necessary

        * Websites  [ NO CHANGE 20240502 ]
            * Homepage
                * perlcommunity.org/science
                * NEED UPDATE WEBSITE WITH PAPER SUBMISSION TIMELINE DATES
            * OJS Subdomain
                * science.perlcommunity.org/spj
                * NEED E-MAIL DEAUTHENTICATION FIX

        * Mailing List  [ NO CHANGE 20240502 ]
            * One-way announcements only
            * Sendy, self-hosted by Brett
            * Request all committee members to register
                * perlcommunity.org/science#mailing_list

        * Paper Ideas
            * none

    * 1st Annual Perl Committee Convention   [ NO CHANGE 20240502 ]
        * NEED ASK TPC for date of conference banquet to ensure it is not Tuesday
        * NEED ASK TPC for BoF room reservation
        * During TPC in Las Vegas, tentative date Tuesday 6/25/2024, TIME TBD
        * Need to create & ratify bylaws
        * Celebrate at after-party dinner

    * RoboPerl Nonprofit
        * none

    * Science Perl Hackathon
        * Every 6 weeks
        * Next event 5/29/24, 7-9pm Central time zone, theme is Final Paper Approval

* New Business
    * none

* Closing, 1:47pm
