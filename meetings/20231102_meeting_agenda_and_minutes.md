# Science Perl Committee
# Meeting Agenda & Minutes

## November 2nd, 2023
## Seventh Official Meeting

* Opening, 1:04pm

* Attendance & Introductions
    * Brett Estrade (high-speed computing)
    * Zakariyya Mughal (biomedical imaging, BioPerl, PDL, toxicology research)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Yussuf Arifin AKA Zahirul Haq (from Malaysia, new to coding, wants to learn more about Perl)
    * Jakub Pawlowski (wants to integrate Perl AI with .NET & Mono)
    * Marc Perry, PhD (biologist, used to have wet lab, now doing bioinformatics & genomics using Perl 15 years, Senior Bioinformatician at Genomics Institute of UC Santa Cruz)
    * Justin Kelly (web developer from Dublin, wants to get back into Perl, started w/ Perl CGI years ago, loves Perl)
    * Daniel Mera (no intro)
    * Adam Russell, PhD (computer scientist, leads group United Health Group subsidiary OptumAI, developing new products to bring AI & ML to admin workers)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 10/19/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Brett Estrade to be accepted as read, voted unanimously

* Officer Reports
    * Zaki & Will reported on the Science Perl Hackathon (see below)

* Old Business

    * TPC 2024 Science Track

        * TPF & TPC Science Track Proposal report
            * TPF approval is pending until survey is complete

        * Call For Papers
            * Everyone should keep working on paper ideas
            * CFP will be opened when TPF gives official approval
            * Should be shortly after survey is complete

        * Survey
            * Currently have received 20 responses
            * We reviewed the results from the current 20
            * Dean & Peter from TPF can view the results
            * Strong indicators the Science Track would improve attendance
            * Please promote as much as possible, need more responses

        * Editorial Review Subcommittee
            * We reviewed the OJS online demo, created a test abstract, it looks pretty good
            * We will review the OpenConf & pretalx demos in the next meeting, then make a decision

        * Proceedings
            * If we choose the OJS software system, it may help us publish our proceedings

        * Websites
            * Will Braswell will update perlcommunity.org after TPF approves the Science Track

        * Paper Ideas
            * none

    * Science Perl Hackathon
        * 2nd hackthon held last night
            * We looked at different options for software used to review and approve academic abstracts / papers / posters
            * Open Journal Systems (OJS), only free self-hosted edition
            * OpenConf, has a free "community edition" self-hosted edition
            * pretalx, has a free self-hosted edition
            * Others we probably won't use include Oxford, EasyChair, etc.
        * Every 6 weeks
        * Next event 12/13/23, 7-9pm Central time zone, theme is Call for Papers & Editorial Review Subcommittee

* New Business
    * none

* Closing, 1:57pm
