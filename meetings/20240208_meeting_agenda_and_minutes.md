# Science Perl Committee
# Meeting Agenda & Minutes

## February 8th, 2024
## Fourteenth Official Meeting

* Opening, 1:04pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Zakariyya Mughal (BioPerl, PDL, previously biomedical imaging, currently computational toxicology)
    * Cheok Yin "CY" Fung (physics major, interested in math and quantum computing)
    * Mike Flannigan (interested in math & science in general, also astronomy & space program)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 1/25/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Brett Estrade to be accepted as modified, voted unanimously

* Officer Reports
    * Brett reports on the following:
        * We worked on e-mail setup last night, will discuss in New Business
        * Need to write up CFP Announcement ASAP

    * Zaki reports on the following:
        * none

    * Will reports on the following:
        * Added Science Perl Journal front page graphics and logos
        * Announced soft CFP on 2/2/24 Groundhog Day during Perl Town Hall
        * Set up e-mail science@perlcommunity.org to receive paper submissions

* Old Business

    * TPC 2024 Science Track

        * General next steps:
            * Finish setting up OJS e-mail
            * Set up basic text content for OJS About page, etc
            * Create timeline of each step of the paper submission / review / approval process
            * Write up Science Perl Track CFP Announcement
            * Send Science CFP to TPC Planning Committee for final review on 1/31/24
            * Publish Science CFP on perlcommunity.org/science
            * Distribute Science CFP via e-mail, Facebook, Twitter, IRC, etc
            * Start receiving & reviewing paper submissions in OJS
            * Finish reviewing papers in OJS 
            * Send TPC Planning Committee our list of approved science papers ASAP, so they can review & coordinate with other track scheduling

        * Call For Papers
            * Meet this Sunday 2/11 7pm CST for writing CFP
            * TPF will send out main CFP on Monday
            * We want to include our Science CFP in the main CFP
            * We will release Science CFP within approx 3 days after approval
            * We will require abstracts to be done within 1 month after CFP announced
            * We will require all papers to be submitted by May 15th
            * List of Science CFP distribution channels:
                * E-mail from Brett, people who responded to the Science Track Survey
                * E-mail from Brett, Perl Monger leaders
                * E-mail from Zaki, BioPerl & PDL mailing lists
                * Social media from Will, Facebook
                * Social media from Will, Twitter
                * Social media from Zaki, Mastodon
                * Social media from Zaki, LinkedIn
                * IRC.perl.org MAGnet chat from Zaki
                * IRC.libera chat from Zaki
                * TPRF Slack chat from Brett
                * HPC Social Discord chat from Brett
                * Perl Discord chat from Brett
                * Blogs.perl.org blogs from Brett
                * Dev.to blogs from Zaki
                * Reddit website from Zaki
                * Perl Monks website from Brett
                * TPF website from Brett
                * Perl Planet aggregator from Brett
                * Perl Weekly Newsletter aggregator from Brett
            * Brett has Sendy, like a self-hosted MailChimp, sends through Amazon SCS, can be used for advertising and updating everyone on Science Track
                * Sends from FOO@science.perlcommunity.org subdomain
                * People can sign themselves up and also opt out

        * Editorial Review Subcommittee
            * We do NOT want to put out a broad call for people to join the Editorial Review Subcommittee
            * Brett will inform TPF's Ruth Holloway and Bruce Gray that they will be Guest Reviewers without any authority to vote for paper reject/approve
            * Need to have Subcommittee Hackathons starting every 2 weeks once we have our first paper submission

        * Proceedings
            * none

        * Websites
            * Now have subdomain for OJS server
                * Need SSL certificate
                * science.perlcommunity.org/spj
            * Will started working on updating perlcommunity.org, we will have new webpage after CFP
                * perlcommunity.org/science

        * Paper Ideas
            * Mike Flannigan, Perl cartography & mapping, create GeoJSON & plotting maps of automobile tracks & hiking tracks
            * CY Fung, wants to review quantum computing module(s) on CPAN, including Quantum::Superpositions

    * RoboPerl Nonprofit
        * none

    * Science Perl Hackathon
        * Every 6 weeks
        * Next event 3/6/24, 7-9pm Central time zone, theme is Paper Review Process

* New Business

    * E-mail services
        * Zaki & Brett worked on e-mail services last night, got outgoing e-mail to send through OJS
        * We can send e-mails but the spam score is very bad, to fix that we need to do the domain verification keys or similar
        * Dynadot DNS didn't let us set up both the science@perlcommunity.org e-mail address and also the domain verification keys
        * Can't currently send because Gmail and other e-mail servers will automatically filter our outgoing messages
        * Thus, today we purchased the Dynadot Pro E-mail service for pro-rated amount of $17.15 paid by Brett, normal cost is $20/year
            * Gives us SMTP, POP, and IMAP
            * Should solve most or all of our e-mail issues

* Closing, 2:02pm
