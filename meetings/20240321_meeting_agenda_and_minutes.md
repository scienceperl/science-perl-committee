# Science Perl Committee
# Meeting Agenda & Minutes

## March 21st, 2024
## Seventeenth Official Meeting

* Opening, 1:05pm

* Attendance & Introductions (Voting Quorum Met)
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Marc Perry, PhD (biologist, used to have wet lab, now doing bioinformatics & genomics using Perl 15 years, Senior Bioinformatician at Genomics Institute of UC Santa Cruz)
    * Jakub Pawlowski (wants to integrate Perl AI with .NET & Mono)
    * Mike Flannigan (interested in math & science in general, also astronomy & space program, geographic information systems (GIS))
    * Lewis Johanne (computer science, AI, agricultural science, mathematics)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 3/7/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Marc Perry to be accepted as corrected, voted unanimously

* Officer Reports
    * Brett reports on the following:
        * Thanks everyone for patience during vote at last meeting, hopefully it strengthened our operations
        * We will work to create and approve formal bylaws at in-person meeting during TPC 2024 Las Vegas
        * Zaki has resigned as 1st Vice Chairman, we greatly appreciate his service and could not have done it without him
        * We will not hold elections for 1st Vice Chairman until further plans can be made

    * Will reports on the following:
        * Published perlcommunity.org/science, see below for details
        * Hackathon 2 weeks ago, see below for details
        * Paper idea from Alex Gomez, see below for details
        * Overview of the 3 primary officer positions by request of Brett
            * Chairman, presides at meetings, ensure all business is in order
            * 1st Vice Chairman, presides in absence of Chairman, may act as Parlimentarian and/or Treasurer as needed
            * 2nd Vice Chairman, presides in absences of Chairman & 1st Vice, may act as Secretary

* Old Business

    * TPC 2024 Science Track

        * General Next Steps
            * Fix OJS e-mail de-authentication issue
            * Create timeline of each step of the paper submission / review / approval process  [ DONE ]
            * Publish Science CFP on perlcommunity.org/science   [ DONE ]
            * Distribute Science CFP via e-mail, Facebook, Twitter, TPF announcements, etc   [ DONE ]
            * Distribute Science CFP via IRC (Magnet #perl & #pdl, Libera #perl), Reddit, AND REMAINING AVENUES
            * Promoting & requesting paper submissions from those on the Paper Idea list
            * Start receiving & reviewing paper submissions in OJS
            * Finish reviewing papers in OJS 
            * Send TPC Planning Committee our list of approved science papers ASAP, so they can review & coordinate with other track scheduling

        * Remote Presentations
            * none

        * Paper Submission Timeline
            * Abstracts submitted by April 5th
            * Abstracts approved or declined by April 15th
            * 1st drafts of papers & posters submitted by May 15th
            * 1st drafts feedback sent back to submitters by May 31st
            * Final drafts submitted by June 7th
            * Final drafts approved or declined by June 15th

        * Call For Papers
            * TPF sent our Science CFP as part of main CFP
            * Submitters will be directed to have presentation slides that match their paper or poster content
            * Brett created infographic for CFP
                * https://www.facebook.com/groups/scienceperl/posts/917497506595730
            * Time to go full steam ahead on promoting Science CFP, list of distribution channels:
                * E-mail from Brett, people who responded to the Science Track Survey [ DONE ]
                * E-mail from Brett, universities (LSU, UT, BYU) [ DONE ]
                * E-mail from Brett, computational oceanographers [ DONE ]
                * E-mail from Brett, Perl Monger leaders
                * E-mail from Zaki, BioPerl & PDL mailing lists
                * Social media from Will, Facebook [ DONE ]
                    * https://www.facebook.com/groups/perlprogrammers/posts/7567632603269462
                    * https://www.facebook.com/wnbjr/posts/pfbid023mjrkJaE7yY3EhdF9S81237vxaPdM3d1cJEjAk7BofcbpsEn3ceBh8scrk3siR4El
                    * https://www.facebook.com/perlcommunity/posts/pfbid0Vu2kdYRLgJJhWwSEyEu9uuDzkDKczE3FdgJU4yibepBdnPvf6hy9YJgoxPrpVaTWl
                * Social media from Will, Twitter [ DONE ]
                    * https://twitter.com/perlcommunity/status/1760842963125403904
                * Social media from Zaki, Mastodon
                * Social media from Zaki, LinkedIn
                * IRC.perl.org MAGnet chat from Zaki
                * IRC.libera chat from Zaki
                * TPRF Slack chat from Brett
                * HPC Social Discord chat from Brett
                * Perl Discord chat from Brett
                * Blogs.perl.org blogs from Brett [ DONE ]
                    * https://blogs.perl.org/users/oodler_577/2024/02/repost-tprc-2024-call-for-papers-is-now-open.html
                * Dev.to blogs from Zaki
                * Reddit website from Zaki
                * Perl Monks website from Brett
                * TPF website from Brett [ DONE ]
                    * https://news.perlfoundation.org/post/cfp2024
                * Perl Planet aggregator from Brett
                * Perl Weekly Newsletter aggregator from Brett [ DONE ]
                    * https://perlweekly.com/archive/656.html
                * NEED UPDATE & REVIEW DISTRIBUTION CHANNEL LIST ABOVE, THEN CONTACT ALL CHANNELS

        * Editorial Review Subcommittee
            * We may or may not have a formal motion to officially create a subcommittee, depending on how many paper submissions we receive

        * Proceedings
            * Same as Editorial Review Subcommittee above

        * Websites
            * Homepage
                * perlcommunity.org/science
                * Now published, ready to present to committee & officially release
                * Motion made by Will Braswell to approve as official committee website, approved unanimously
                * NEED UPDATE WEBSITE WITH PAPER SUBMISSION TIMELINE DATES
            * OJS Subdomain
                * science.perlcommunity.org/spj
                * NEED E-MAIL DEAUTHENTICATION FIX

        * Mailing List
            * One-way announcements only
            * Sendy, self-hosted by Brett
            * Request all committee members to register
                * perlcommunity.org/science#mailing_list

        * Paper Ideas
            * Alex Gomez, "Asymptoptic Analysis for Jigsaw Puzzles", National University of Mexico Autonomous, CS master's degree, should finish in ~1 month, contains ~200 lines of Perl code, World Federation of Competitive Jigsaw Puzzles, 500 pieces solved in 30-40 mins, why are some puzzles faster to solve than others?  [ NOT DISCUSSED ON 3/7/24 ]
            * Mike Flannigan, Geographic Information Systems (GIS), will work with Will to create abstract and possibly target 2025 for full paper
            * Lewis Johanne, grew up on a farm, idea is to have agricultural commodity prices available in real time, we have it for gold and stock market but not for food commodities such as tomatoes and lettuce etc
            * NEED UPDATE & REVIEW PAPER IDEA LIST, THEN CONTACT ALL POTENTIAL SUBMITTERS

    * RoboPerl Nonprofit
        * none

    * Science Perl Hackathon
        * Every 6 weeks
        * Hackathon two weeks ago 3/6/24
            * Focus on science CFP promotion & discussion
            * Everyone had a good time, we talked about the Science Perl Track & Science Perl Journal with interested Perl programmers
            * Receive new paper idea from Alex Gomez
        * Next event 4/17/24, 7-9pm Central time zone, theme is Paper Development

* New Business
    * none

* Closing, 2:21pm
