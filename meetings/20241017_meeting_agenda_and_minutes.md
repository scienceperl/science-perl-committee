# Science Perl Committee
# Meeting Agenda & Minutes

## October 17th, 2024
## Thirty-First Official Meeting

* Opening, 1:10pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Mickey Macten (online security and streaming multimedia)
    * Dr. Drew O'Neil (AKA Andrew, uses Perl AI for chemometrics)
    * Joshua Oliva (interested in scientific breakthroughs, physics, health topics)
    * Kai Baker (author of Perl Jam web server)
    * John Napiorkowski (maintainer of Catalyst)
    * Robbie Hatley (EE & computer programming in Perl)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 10/03/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Brett to be accepted as corrected, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of one officer only, not full voting quorum

* Officer Reports
    * Brett reports on the following:
        * Congratulations to everyone on publishing the Science Perl Journal, all the authors plus editors Will & Brett
        * Revisit for the Latex template
        * Apologized to our own committee for e-mail blast upset, CAN-SPAM act says we need to include physical address in all future e-mails
            * Eventually need physical mailing address specifically for nonprofit

    * Dr. O'Neil reports on the following:
        * LPW 2024 final plans are now underway for "Science Perl Talks"
            * 1x 20-min, 1:00pm - 1:20pm London time
                * Dr O'Neil, chemometrics
                * https://act.yapc.eu/lpw2024/talk/7936
            * 6x 100-min, 3:30pm - 5:10pm London time
                * https://act.yapc.eu/lpw2024/talk/7939
                * 4x lightning, NEED REPLIES FROM THE FOLLOWING:
                    * Dr. Luis Mochan, Photonic
                    * Dr. Christos, Performance (NEED PRE-RECORDED VIDEO FILE)
                    * Dr. Russell?  Ontology?  (MAY NOT BE AVAILABLE?)
                    * Boyd Duffee, PDL? (NEED CONFIRMATION 10/23)
                    * Will, Science Perl Journal (BACKUP TALK)
                * 1x 20-min
                    * John, Old-School To-Do App in Catalyst / DBIC / Valiant
                * 1x 40-min
                    * Brett, Hurricanes Helene & Milton Operational Update
            * NEED OBS SESSION W/ DR. O'NEIL
            * NEED LAPTOP GOOGLE MEET SESSION W/ BRETT & DR. O'NEIL USING PERLTOWNHALL ACCOUNT
            * NEED BRING MULTIPLE HD CAMERAS
            * NEED COORDINTE W/ VOLUNTEER BOYD DUFFEE
            * NEED SPJ COPIES???

    * Will reports on the following:
        * SPJ update
            * SPJ issue #1 is now available in full-color on B&N
            * eBook next
            * Black & white copy after that
            * SPJ issue #2 after that
        * Still need to collect more donations for Crossref DOI registration, $275 per year pro-rated
        * Still removing all undesirable logos
            * Need to register the trademarks for all our logos

* Old Business

    * Science Track

        * General Next Steps
            * Create timeline of each step of the paper submission / review / approval process  [ DONE ]
            * Publish Science CFP on perlcommunity.org/science   [ DONE ]
            * Distribute Science CFP via e-mail, Facebook, Twitter, TPF announcements, etc   [ DONE ]
            * Distribute Science CFP via IRC (Magnet #perl & #pdl, Libera #perl), Reddit, and remaining avenues [ DONE ]
            * Promoting & requesting paper submissions from those on the Paper Idea list [ DONE ]
            * Send TPC Planning Committee our list of approved science papers ASAP, so they can review & coordinate with other track scheduling [ DONE ]
            * Start receiving & reviewing paper submissions in OJS [ DONE ]
            * Finish reviewing papers in OJS  [ DONE ]
            * Publish preprint edition [ DONE ]
            * Finalizing papers for SPJ full edition [ DONE]
            * Publish SPJ full edition [ DONE ]
            * Science Perl Track is a travelling track with its own identity, it can go to any conference world-wide, start booking more conferences

        * Remote Presentations  [ NO CHANGE 20241017 ]
            * We will do everything possible to bring Science Track talks in person, and when not possible will allow remote talks
            * Acceptable reasons for remote talks
                * Legal
                * Medical
                * Financial
            * Need official remote talk policy

        * Perl Community Conference, Winter 2024
            * OFFICIAL CONFERENCE ANNOUNCEMENT HAS BEEN MADE
            * Perl Birthday Party 12/18/2024
            * Dr. Christos as Conference MC
            * One day online-only virtual conference
            * One track only, one speaker at a time
            * Free conference, donations only
            * Use Google Meet w/ maximum 100 participants, ask for feedback after conference
            * Allow 3 different talk lengths, corresponding to 3 different paper lengths
                * Full-length talks, 50 minutes
                * Short-length talks, 20 minutes
                * Lightning talks, 5 minutes
            * Use OJS as registration for conference presenters
            * Use existing mailing list as registration for conference attendees
            * Lightning 5-min
                * Brett, 3 Perl Virtues
                * Will, Science Perl Journal & Nonprofit Updates
                * May need to use AI to create transcript and format into academic extended abstract
                * NEED BRETT MEME TO SOLICIT 5 MORE LIGHTNING TALKS
            * Medium Length 20-min
                * Boyd Duffee? PDL, EXPANDED FROM LPW LIGHTNING TALK?
            * Full Length 50-min
                * Dr. Christos, Performance, EXPANDED FROM LPW LIGHTNING TALK
                * Dr. Russell, Ontology, EXPANDED FROM LPW LIGHTNING TALK?
                * Dr. Luis Mochan, Photonic, EXPANDED FROM LPW LIGHTNING TALK
                * Brett, Sub::Genius Sequential Consistency ??? ALREADY DONE IN LPW
                * John, Old-School To-Do App in Catalyst / DBIC / Valiant ??? EXPANDED FROM LPW MEDIUM TALK???
                * Will, PerlGPT Update???
            * POSSIBLE EXTRA ACTIVITIES
                * Mini Job Fair
                    * NEED CONTACT JOBS.PERL.ORG & GOOGLE OPENINGS
                * Mini Investor Pitch
                    * NEED JOHN REPLY FROM OLD BOSS
                    * NEED JOHN CREATE LIST OF INVESTOR CONTACTS

        * Call For Papers, Winter 2024  [ NO CHANGE 20241017 ]
            * NEED UPDATE PAPER LENGTHS BELOW ON SPJ WEBSITE, ALSO NUMBER OF REFERENCES PER EACH
            * Print all 3 talks lengths from Science Perl Conference
                * Full-length paper, 10 - 35 pages max
                * Short-length paper, 2 - 9 pages
                * Extended abstract, 1 page max

        * Paper Submission Timeline, Winter 2024  [ NO CHANGE 20241017 ]
            * Abstracts submitted by September 30th
            * Abstracts approved or declined by October 4th
            * 1st drafts of papers & posters submitted by November 4th
            * 1st drafts feedback sent back to submitters by November 15th
            * Final drafts submitted by December 2nd
            * Final drafts approved or declined by December 9th

        * Paper Ideas, Winter 2024 & Summer 2025
            * Will, Full Talk, PerlGPT Part 2
            * Will, Lightning Talk, Navi AI
            * Brett, 2024 Lightning Talk, 3 Perl Virtues
            * Brett, 2024 Medium Talk, OpenMP Thread Safety
            * Brett, 2024 Medium Talk, Perl Sequential Consistency
            * Brett, 2025 Full Talk, OpenMP Read-Only API for Perl Data Structures
            * Dimitris Kechagias, iOS weather app for astronomers Xasteria, proxy servers it uses for the weather forecasts are in Perl
            * Dr. Russell, 2025 Full Talk, genetic algorithms, currently changing to new job w/out previous job restriction
            * Dr. Russell, 2024 Medium Talk?, Cowl ontology for CPAN
            * Dr. Christos, using Perl to memory management for buffers in other languages such as assembly (lightning talk); also another talk about FOO (20 min talk)
            * John Napiorkowski, 2024 LPW Lightning Talk, Catalyst Valiant To-Do List Application, Beta Demo
            * John Napiorkowski, 2024 Advent & PCC Lightning Talk, Catalyst Valiant To-Do List Application, Full Demo
            * John Napiorkowski, 2025 Advent & PCC Lightning Talk, Catalyst Valiant To-Do List Application, AI Demo
            * Bartosz Jarzyna, 2025 PCC Full Talk?, Bitcoin
            * Dr. Luis Mochan, 2024 LPW Lightning Talk & PCC Full Talk?, Photonic

        * Editorial Review Subcommittee  [ NO CHANGE 20241017 ]
            * Reviewers
                * Currently reviewers are Dr. Adam Russell, Dr. Casiano Rodriguez Leon, Will Braswell, and Brett Estrade
                * Need more reviewers for 2025, at least 2 per paper
            * OJS
                * Need fix OJS e-mail de-authentication issue, old bug found by Brett & Zaki
                * NEED BRETT & DR. RUSSELL TO REPRODUCE THE BUGS BELOW
                * upload errors, 2 different problems where upload buttons do or do not appear, affecting Manickam & Will & Dr. Russell, full report sent to Brett for further investigation into server logs and GitHub bug reports
            * Editor's Choice Award
                * We will use Dr. Perry's spreadsheet w/ initial selection rubric for Winter 2024 and update as needed
            * Short papers
                * Yes we will accept both short papers (2 - 9 pages) and even extended abstracts (1 page)
                * IEEE guidelines for full-length is 20 to 25 pages, no more than 35 pages including references
                * NEED DECIDE NUMBER OF REFERENCES FOR SHORT PAPERS & EXTENDED ABSTRACTS

        * Proceedings Subcommittee  [ NO CHANGE 20241017 ]
            * Authors get a free printed copy of SPJ, others can purchase a printed copy
            * Science Perl mailing list new members will get free PDF copy of SPJ, others can download PDF with prominent donation button
            * Funding
                * Donation ware online copy
                * Paid physical copies
                * Special signed copies
            * Latex Formatting
                * Brett will create a Latex template for everyone to use in 2025
                * Brett will try to find a way to format the Perl code nicer than it is now
            * Publication
                * We made Conference Preprint Edition in limited run
                * We offered signed copies for sale at higher price
                * NEED FINISH FINAL EDITION
            * Barnes & Noble
                * Now approved for self-publishing nonprofit org account 
                * Price much lower per copy than any other option
            * Need to have committee representative selling SPJ copies at all Perl conferences:
                * TPC::NA
                    * http://tprc.us
                    * 6/??/2025
                    * Need revisit after TPF problems are addressed
                * YAPC::Japan
                    * https://yapcjapan.org
                    * 10/5/2024
                    * George Baugh 2025
                    * NEED JOURNAL COPIES TO SELL
                    * NEED SCIENCE PERL TRACK
                * LPW
                    * https://act.yapc.eu/lpw2024
                    * 10/26/2024
                    * Dr. O'Neil
                    * Science Perl Talks approved
                    * NEED JOURNAL COPIES TO SELL
                * FOSDEM
                    * https://fosdem.org
                    * 2/??/2025
                    * Will & Brett
                    * NEED MONEY FOR TICKET
                    * NEED PERL BOOTH
                    * NEED SUBMIT TALKS
                    * NEED SCIENCE PERL TRACK
                * YAPC::India
                    * NEED CREATE
                * YAPC::Asia (DEFUNCT)
                    * https://yapcasia.org
                    * Last conference 2015
                    * Need revive?
                * YAPC::EU (DEFUNCT)
                    * http://www.yapceurope.org/events/conferences.html
                    * Last Conference 2019
                    * Need revive?
                * YAPC::Russia (DEFUNCT)
                    * https://yapcrussia.org
                    * Last Russian Perl Workshop 2019
                    * Need revive?
                * YAPC::Australia (DEFUNCT)
                    * https://osdc.com.au/
                    * Last Conference 2004?
                    * Need revive?

        * Websites  [ NO CHANGE 20241017 ]
            * Homepage
                * perlcommunity.org/science
                * perlcommunity.org/awards  NEED FINISH
                * NEED UPDATE HACKATHON DATES 7/10 should be 7/17
                * NEED UPDATE 2025 PAPER DATES
            * OJS Subdomain
                * science.perlcommunity.org/spj
                * Updated OJS website with 2025 paper submission timeline dates
                * NEED E-MAIL DEAUTHENTICATION FIX

        * Mailing List  [ NO CHANGE 20241017 ]
            * One-way announcements only
            * Sendy, self-hosted by Brett
            * Request all committee members to register
                * perlcommunity.org/science#mailing_list

    * Science Perl Hackathon
        * Every 6 weeks
        * Last event 10/2/24, 7:30-9:30pm Central time zone, theme was Science Perl Journal Latex
            * Worked on SPJ content margins
        * Next event 11/13/24, 7:00-9:00pm Central time zone, theme is Science Perl Journal Sales?

    * Treasurer's Report [ NO CHANGE 20241017 ]
        * We have enough money for Crossref
        * Need the Fundraising Committee to start collecting donations for 2025 activities
        * NEED PAY PRORATED CROSSREF
        * NEED BRETT RECEIPTS
        * NEED UPDATE SPREADSHEET WITH SPJ SALES & TPC RECEIPTS

    * 2nd Annual Perl Committee Convention, 2025  [ NO CHANGE 20241017 ]
        * NEED DETERMINE DATE & LOCATION
        * NEED REVIEW 7/17/24 HACKATHON NOTES FOR 2024 CONVENTION DEBRIEF

    * RoboPerl Nonprofit
        * none

* New Business

    * none

* Closing, 2:16pm
