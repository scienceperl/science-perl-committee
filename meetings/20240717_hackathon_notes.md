# Science Perl Committee
# Hackathon Notes

## July 17th, 2024
## 8th Official Hackathon

* Opening, 7:03pm

* Overview of purpose
    * Debrief of Perl Committee Convention, Science Perl Track, and Science Perl Journal Preprint Edition
    * Roses, Thorns, and Buds format

* Dr. Marc Perry, e-mail feedback
    * Diamond PERL Award rubric spreadsheet
        * Need review & discuss w/ Editorial Review Subcommittee
    * Papers or posters come first, talks and powerpoints are optional only and are based directly on original paper or poster
        * Need clarify w/ Dr. Perry & others that papers come first
    * The Science Perl Journal is a peer-reviewed academic journal which serves as the proceedings of the Science Perl Track
        * Need clarify w/ Dr. Perry & others that SPJ is both a journal and a track proceedings
    * Biology paper sections
        * Title, Abstract, Introduction, Results, Discussion, Conclusion, Methods, Acknowledgements, References, Figure Legends, and Figures
    * Latex template
        * JOSS or some other software journal?
        * Default article template from Overleaf?
    * Editorial Review Subcommittee
        * Need more reviewers
        * Minimum 2 reviewers per paper

* Dr. Christos, e-mail feedback
    * JOSS template
    * Code snippets in repos
    * Reviews should be single blind or fully open with review comments published along w/ paper

* Manickam Thanneermalai, e-mail feedback
    * Add missing fields of science to SPJ website, DONE LIVE
        * Space Sciences
        * Physics
        * Semiconductors
        * Robotics
    * Overleaf
        * Text alignment around images was difficult, Brett took care of it

* Jan Stepanek, e-mail feedback
    * Short papers 10 - 15 pages

* Steven McDougall
    * Impressed by energy in the Perl Committee Convention, keeping Perl going
    * Impressed by a whole journal full of articles
    * Convention was good, what happens at the next Convention depends on what is done over the next year
    * Likes to see people using Perl on scientific problems

* Robbie Hatley
    * Attended online only, wishes he could have attended in person
    * Liked the SPJ unboxing

* Brett Estrade
    * Want more authors attending in person
    * SPJ full editing instead of preprint edition
    * Overleaf is good
        * Brett already figured out how to handle page margins, page numbering, and line spacing in Overleaf but backwards by including individual paper PDF's into Latex
        * Need to determine which parts OJS can handle, and which parts need to be in kept in Overleaf
        * When an abstract is accepted in OJS, then we will create a new Overleaf template for the author and send them the Overleaf project URL, then the author will start editing their own paper directly in Overleaf, and it can be properly linked into our master Overleaf project for the entire SPJ issue
    * Working on learning OJS publishing procedure
    * Need to install OJS Crossref plugin for DOI's
    * How to generate ISBN & barcode
        * isbnservices.com

* Will Braswell
    * Liked the SPJ Preprint & banner & awards etc; all the stuff that made us unique and fun
    * Bad guys are thorns in our side, but we are the good guys and we are clearly winning
    * Next year we want 16 papers instead of 8
    * Next year everyone needs to bring 1 friend

* Closing, 8:57pm
