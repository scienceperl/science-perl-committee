# Science Perl Committee
# Meeting Agenda & Minutes

## August 22nd, 2024
## Twenty-Seventh Official Meeting

* Opening, 1:30pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Christos Argyropoulos, MD, PhD (kidney physician, genomic & biomarker discovery, big data sets, EHR, methodology dev for sequencing, new markers for kidney disease)
    * John Napiorkowski (maintainer of Catalyst)
    * Joshua Oliva (interested in scientific breakthroughs, physics, health topics)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 8/8/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Brett Estrade to be accepted as corrected, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of one officer only, not full voting quorum

* Officer Reports
    * Brett reports on the following:
        * Science Perl Hackathon thanks, SPJ marathon last night
        * Need follow up with TPF video staff for Manickam's video
        * Posted list of Science Track videos on PerlMonks
        * brian d foy is continuing to post TPC videos on Reddit

    * Dr. O'Neil reports on the following:
        * Not present, sends regards
        * Met w/ Will regarding LPW plans
        * Need to make a list of the ways that the Science Perl Track can benefit the LPW
            * More talks & speakers
            * More interested attendees
            * More publicity content
        * Need to contact LPW organizers to set up a meeting w/ Science Perl officers
            * Full Science Perl Track this year 2024?
            * Partial track this year 2024, full track next year 2025?
            * If too late for any track this year 2024, Dr. O'Neil's paper and sell SPJ copies only in 2024?

    * Will reports on the following:
        * My SPJ paper is the last one left to be fixed due to recurring margin overflow errors
        * Finished SPJ front cover & front matter & back covers, one back cover ISBN & barcode for print edition, another for the ebook edition
        * Purchased 2 ISBN numbers
        * Spent money previously earmarked for Crossref in order to purchase ISBNs, need to collect more donations for Crossref DOI registration
        * Finished work on new clamshell pearl logo for nonprofit, currently removing all undesirable logos
        * NEED FINISH SPJ preprint sales on FB before opening sales of full edition

* Old Business

    * Science Track

        * General Next Steps
            * Create timeline of each step of the paper submission / review / approval process  [ DONE ]
            * Publish Science CFP on perlcommunity.org/science   [ DONE ]
            * Distribute Science CFP via e-mail, Facebook, Twitter, TPF announcements, etc   [ DONE ]
            * Distribute Science CFP via IRC (Magnet #perl & #pdl, Libera #perl), Reddit, and remaining avenues [ DONE ]
            * Promoting & requesting paper submissions from those on the Paper Idea list [ DONE ]
            * Send TPC Planning Committee our list of approved science papers ASAP, so they can review & coordinate with other track scheduling [ DONE ]
            * Start receiving & reviewing paper submissions in OJS [ DONE ]
            * Finish reviewing papers in OJS  [ DONE ]
            * Publish preprint edition [ DONE ]
            * Finalizing papers for SPJ full edition, NEED FINISH ASAP
            * Publish SPJ full edition
            * Science Perl Track is a travelling track with its own identity, it can go to any conference world-wide, start booking more conferences

        * Remote Presentations  [ NO CHANGE 20240822 ]
            * We will do everything possible to bring Science Track talks in person, and when not possible will allow remote talks
            * Acceptable reasons for remote talks
                * Legal
                * Medical
                * Financial
            * Need official remote talk policy

        * Paper Submission Timeline, 2024  [ NO CHANGE 202408022 ]
            * Abstracts submitted by April 5th  [ DONE ]
            * Abstracts approved or declined by April 15th  [ DONE ]
            * Extended CFP, abstracts submitted by April 20th  [ DONE ]
            * Extended CFP, abstracts approved or declined by April 30th  [ DONE ]
            * Extended 1st drafts of papers & posters submitted by May 20th  [ DONE ]
            * 1st drafts feedback sent back to submitters by May 31st [ DONE ]
            * Final drafts submitted by June 7th [ DONE ]
            * Final drafts approved or declined by June 15th [ DONE]
            * Final drafts for full papers by July 28th [ DONE ]
            * 2024 totally closed

        * Call For Papers, 2025
            * 2025 CFP now open on website only
            * Delay promotion until after Christmas 2024 CFP finishes

        * Paper Ideas, Christmas 2024 & Summer 2025
            * Will, PerlGPT Part 2 or Navi AI
            * Brett, OpenMP Read-Only API for Perl Data Structures
            * Dimitris Kechagias, iOS weather app for astronomers Xasteria, proxy servers it uses for the weather forecasts are in Perl
            * Dr. Russell, genetic algorithms, currently changing to new job w/out previous job restriction
            * Dr. Christos, using Perl to memory management for buffers in other languages such as assembly (lightning talk); also another talk about FOO (20 min talk)
            * John Napiorkowski, Catalyst and/or Valiant
                * Pick an old application and convert into web application
                * Dr. Christos recommends some old C code program called from the command line used for bioinformatics
                    * github.com/jeffdaily/parasail

        * Editorial Review Subcommittee
            * NEED UPDATE OJS & WEBSITE, submitters must create their presentation slides to match their paper or poster content
            * Reviewers
                * Currently reviewers are Dr. Adam Russell, Will Braswell, and Brett Estrade
                * Need more reviewers for 2025, at least 2 per paper
            * OJS
                * Need fix OJS e-mail de-authentication issue, old bug found by Brett & Zaki
                * NEED BRETT & DR. RUSSELL TO REPRODUCE THE BUGS BELOW
                * upload errors, 2 different problems where upload buttons do or do not appear, affecting Manickam & Will & Dr. Russell, full report sent to Brett for further investigation into server logs and GitHub bug reports
            * Editor's Choice Award
                * We will use Dr. Perry's spreadsheet w/ initial selection rubric for 2024 Christmas and update as needed
            * Short papers
                * Yes we will accept both short papers (2 - 9 pages) and even extended abstracts (1 page)
                * IEEE guidelines for full-length is 20 to 25 pages, no more than 35 pages including references
                * NEED DECIDE NUMBER OF REFERENCES FOR SHORT PAPERS & EXTENDED ABSTRACTS

        * Proceedings Subcommittee  [ NO CHANGE 20240822 ]
            * NEED BOOK SPINE INFO, print SPJ info on the book spine
            * Authors get a free printed copy of SPJ, others can purchase a printed copy
            * Science Perl mailing list new members will get free PDF copy of SPJ, others can download PDF with prominent donation button
            * Funding
                * Donation ware online copy
                * Paid physical copies
                * Special signed copies
            * Latex Formatting
                * Brett will create a Latex template for everyone to use in 2025
                * Brett will try to find a way to format the Perl code nicer than it is now
            * Publication
                * We made Conference Preprint Edition in limited run
                * We offered signed copies for sale at higher price
                * NEED FINISH FINAL EDITION
            * Barnes & Noble
                * Now approved for self-publishing nonprofit org account 
                * Price much lower per copy than any other option
            * Need to have committee representative selling SPJ copies at all Perl conferences:
                * TPC::NA
                    * http://tprc.us
                    * 6/??/2025
                    * Need revisit after TPF problems are addressed
                * YAPC::Japan
                    * https://yapcjapan.org
                    * 10/5/2024
                    * George Baugh
                    * NEED JOURNAL COPIES TO SELL
                    * NEED SCIENCE PERL TRACK
                * LPW
                    * https://act.yapc.eu/lpw2024
                    * 10/26/2024
                    * Dr. O'Neil
                    * NEED FOLLOW UP TO GET TALK SUBMITTED
                    * NEED JOURNAL COPIES TO SELL
                    * NEED SCIENCE PERL TRACK
                * FOSDEM
                    * https://fosdem.org
                    * 2/??/2025
                    * Will & Brett
                    * NEED MONEY FOR TICKET
                    * NEED PERL BOOTH
                    * NEED SUBMIT TALKS
                    * NEED SCIENCE PERL TRACK
                * YAPC::India
                    * NEED CREATE
                * YAPC::Asia (DEFUNCT)
                    * https://yapcasia.org
                    * Last conference 2015
                    * Need revive?
                * YAPC::EU (DEFUNCT)
                    * http://www.yapceurope.org/events/conferences.html
                    * Last Conference 2019
                    * Need revive?
                * YAPC::Russia (DEFUNCT)
                    * https://yapcrussia.org
                    * Last Russian Perl Workshop 2019
                    * Need revive?
                * YAPC::Australia (DEFUNCT)
                    * https://osdc.com.au/
                    * Last Conference 2004?
                    * Need revive?

        * Websites  [ NO CHANGE 20240822 ]
            * Homepage
                * perlcommunity.org/science
                * perlcommunity.org/awards  NEED FINISH
                * NEED UPDATE HACKATHON DATES 7/10 should be 7/17
                * NEED UPDATE 2025 PAPER DATES
            * OJS Subdomain
                * science.perlcommunity.org/spj
                * Updated OJS website with 2025 paper submission timeline dates
                * NEED E-MAIL DEAUTHENTICATION FIX

        * Mailing List  [ NO CHANGE 20240822 ]
            * One-way announcements only
            * Sendy, self-hosted by Brett
            * Request all committee members to register
                * perlcommunity.org/science#mailing_list

    * Science Perl Hackathon
        * Every 6 weeks
        * Last event 8/21/24, 7:30-9:30pm Central time zone, theme was Science Perl Journal Latex
        * Next event 10/3/24, 7:00-9:00pm Central time zone, theme is Conference Representatives Preparation
            * Need George & Dr. O'Neil

    * Treasurer's Report [ NO CHANGE 20240822 ]
        * We have enough money for Crossref
        * Need the Fundraising Committee to start collecting donations for 2025 activities
        * NEED PAY PRORATED CROSSREF
        * NEED BRETT RECEIPTS
        * NEED UPDATE SPREADSHEET WITH SPJ SALES & TPC RECEIPTS

    * 2nd Annual Perl Committee Convention, 2025  [ NO CHANGE 20240822 ]
        * NEED DETERMINE DATE & LOCATION
        * NEED REVIEW 7/17/24 HACKATHON NOTES FOR 2024 CONVENTION DEBRIEF

    * RoboPerl Nonprofit
        * Fundraising Committee
            * Provides funding and financial support for all RoboPerl technology committees
            * Now an active committee

* New Business

    * Science Track

        * Science Perl Conference, Perl Birthday Party 12/18/2024
            * Our birthday present to Perl
            * One day online-only virtual conference
            * One track only, one speaker at a time
            * Free conference, donations only
            * Use Google Meet w/ maximum 100 participants, ask for feedback after conference
            * Dr. Christos or Dr. O'Neil as potential conference leads
            * Allow 3 different talk lengths, corresponding to 3 different paper lengths
                * Full-length talks, 50 minutes
                * Short-length talks, 20 minutes
                * Lightning talks, 5 minutes

        * CFP, Christmas 2024
            * NEED OFFICIAL CFP ANNOUNCEMENT
            * Dr. Christos agrees we should release issue #2 this Christmas
            * Keep using workflow of GitLab + OJS + OverLeaf
            * Need new Latex template from Brett
            * NEED UPDATE PAPER LENGTHS BELOW ON SPJ WEBSITE, ALSO NUMBER OF REFERENCES PER EACH
            * Print all 3 talks lengths from Science Perl Conference
                * Full-length paper, 10 - 35 pages max
                * Short-length paper, 2 - 9 pages
                * Extended abstract, 1 page max

* Closing, 2:17pm
