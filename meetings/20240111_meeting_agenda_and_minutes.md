# Science Perl Committee
# Meeting Agenda & Minutes

## January 11th, 2024
## Twelfth Official Meeting

* Opening, 1:24pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Joshua Day (industrial tooling)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 12/28/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Brett Estrade to be accepted as modified, voted unanimously

* Officer Reports
    * Brett reports on the following:
        * Peter from TPF has not replied to recent messages, TPF wanted to release White Camel before CFP, Peter may still be waiting for some unknown info or perhaps just busy?
        * Brett wants to help Zaki w/ OJS server setup

    * Will reports on the following:
        * RoboPerl nonprofit can accept tax-deductible donations for Science Perl Committee projects

* Old Business

    * TPC 2024 Science Track

        * TPF & TPC Science Track Proposal report
            * We can't wait any longer for official approval, because we need more lead time for science talks versus normal talks

        * Call For Papers
            * Need to release pre-CFP if TPF doesn't release main CFP before our next meeting in 2 weeks

        * Editorial Review Subcommittee
            * Brett created Discord server as neutral ground, will help prevent TPF from taking over Science Perl Track
                * https://discord.com/invite/yPjZKDM6C4
            * Need to start recruiting for Editorial Review Subcommittee via Google Form

        * Proceedings
            * none

        * Websites
            * none

        * Paper Ideas
            * none

    * RoboPerl Nonprofit
        * none

    * Science Perl Hackathon
        * Every 6 weeks
        * Next event 1/24/24, 7-9pm Central time zone, theme is Website Development

* New Business
    * none

* Closing, 1:53pm
