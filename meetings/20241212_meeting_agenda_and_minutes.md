# Science Perl Committee
# Meeting Agenda & Minutes

## December 12th, 2024
## Thirty-Fifth Official Meeting

* Opening, 1:12pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Mohammed Zia (uses Perl for regular expressions etc)
    * Kai Baker (author of Perl Jam web server)
    * John Napiorkowski (maintainer of Catalyst)
    * Christos Argyropoulos, MD, PhD (kidney physician, genomic & biomarker discovery, big data sets, EHR, methodology dev for sequencing, new markers for kidney disease)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 11/29/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Mohammed Zia to be accepted as read, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of one officer only, not full voting quorum

* Officer Reports
    * Brett reports on the following:
        * Working on OpenMP paper
            * Need add color pictures & references
        * Working on 3 Virtues talk
        * Sent e-mail to SPJ issue #2 authors, focus on PCC then paper reviews after that
            * Dr. Mochan is ready for PCC, live full-length talk
            * Dr. Lo Man Hung is ready for PCC, pre-recorded Lightning Talk
            * Dr. Christos is ready for review
        * OJS issue uploading PDF, Dr. Christos unable to upload?
            * NEED FIX ASAP (can be after PCC if necessary)
        * Overleaf updates in the works, based on Brett & Dr. Mochan's paper Latex
        * Slashdot account approved, NEED POST SPJ/PCC ANNOUNCEMENT ASAP TODAY
        * Ars Technica, Phoronix, and Linux Today
            * Can't submit our own articles
            * Must ask them to write their own article about us
        * Press release
            * Look into Fiver
            * Release to tech news websites

    * Dr. O'Neil reports on the following:
        * Contacted Dr. Duffee about creating "PDL for the Impatient" extended abstract for SPJ issue #2
            * Dr. Duffee replied that he is busy working on Perl Advent
            * We can use and/or create a transcript from LPW talk for SPJ #2
        * Almost done with SPJ #2 paper, short paper 5 - 10 pages w/ lots of color images
            * Create new Perl for automatic cluster analysis using AI instead of manual interactive process

    * Will reports on the following:
        * PCC promotion update
            * Sent Brett the SPJ/PCC promotion write-up
            * Will continue asking people to make donations for PCC registration
            * ~10 on Meetup, ~20 on Facebook
        * SPJ sales update
            * Waiting for update from Dr. Russell
        * SPJ publication
            * Black & white copy, will work on after PCC
            * SPJ issue #2 after that
            * 6 months after initial paper publication of each issue, release eBook & Online Edition via OJS
                * Per-article and also whole-issue pricing
                * Subscription pricing model optionally available, both virtual & physical
                    * Pay annual ~$50 online subscription, get access to all past issues online
                    * Pay annual ~$100 physical subscription, get every printed issue and access to all past issues online
                * DOES ALLOW PIRACY!!
            * 12 months after initial paper publication of each issue, release all papers for free via OJS
        * Still need to collect more donations for Crossref DOI registration, $275 per year pro-rated
        * Still removing all undesirable logos
            * Need to register the trademarks for all our logos

* Old Business

    * Science Track

        * General Next Steps  [ NO CHANGE 20241212 ]
            * Create timeline of each step of the paper submission / review / approval process  [ DONE ]
            * Publish Science CFP on perlcommunity.org/science   [ DONE ]
            * Distribute Science CFP via e-mail, Facebook, Twitter, TPF announcements, etc   [ DONE ]
            * Distribute Science CFP via IRC (Magnet #perl & #pdl, Libera #perl), Reddit, and remaining avenues [ DONE ]
            * Promoting & requesting paper submissions from those on the Paper Idea list [ DONE ]
            * Start receiving & reviewing paper submissions in OJS [ DONE ]
            * Finish reviewing papers in OJS
            * Publish preprint edition
            * Finalizing papers for SPJ full edition
            * Publish SPJ full edition

        * Perl Community Conference, Winter 2024
            * HYBRID IN-PERSON & VIRTUAL CONFERENCE
            * Registration now open!
                * https://www.meetup.com/austin-perl-mongers/events/304573306/
                * https://www.facebook.com/events/1103216794572416/
            * Perl's 37th Birthday Party Wednesday, 12/18/2024
            * One day only, 10:30am to 4:00pm Central Time Zone
                * 10:30am  Dr. Christos Argyropoulos, Welcome & Schedule, 5 mins
                * 10:35am  Will Braswell, State of the Noonien Address, short talk 20 mins
                * 10:55am  Dr. Luis Mochan, Metamaterial Simulation via Photonic in Perl, full talk 50 mins
                * 11:45am  Dr. Andrew O'Neil, Determination of Pharmaceutical Authenticity in Perl, short talk 20 mins
                * 12:05pm  Will Braswell, Perl Data Type System, short talk 20 mins
                * 12:25pm  John Napiorkowski, Catalyst Valiant To-Do List Application, short talk 20 mins
                * 12:45pm  Dr. Adam Russell, A CPAN Ontology, short talk 20 mins
                *  1:05pm  Brett Estrade, Pushing the Limits of OpenMP in Perl, full talk 50 mins
                *  1:55pm  Dr. Christos Argyropoulos, Foreign Application Memory Management using Perl, full talk 50 mins
                *  2:45pm  Perl Community Leadership Panel, 40 mins
                *  3:25pm  Lightning Talks, 30 mins total
                    * Dr. Nancy Lo Man Hung, Spider Silk Genetic Diversity
                    * Brett Estrade, 3 Virtues of Perl
                    * Will Braswell & John Napiorkowski, PerlGPT AI Update
                *  3:55pm  Dr. Christos Argyropoulos, PCC Summer 2025 Announcement & Closing, 5 mins
                *  4:00pm  End of PCC
            * Dr. Christos as Conference MC
            * One track only, one speaker at a time
            * Free conference, donations only
                * Accepting donations via Meetup PayPal
                * For those that Meetup PayPal doesn't work, we can accept payment via Square, Venmo, PayPal
            * Use Google Meet w/ maximum 100 participants, ask for feedback after conference
            * Allow 3 different talk lengths, corresponding to 3 different paper lengths
                * Full-length talks, 50 minutes
                * Short-length talks, 20 minutes
                * Lightning talks, 5 minutes
            * Use OJS as registration for conference presenters
            * Use Meetup & Facebook as registration for conference attendees
            * Use existing mailing list to send info to conference attendees
            * Need announce summer 2025 conference during winter 2024 conference
                * July 3rd & July 4th, Austin, TX
            * Extra Activities for Winter 2024 Conference
                * 3 Virtues of Perl, non-science talk by Brett
                * State of the Noonien Address, non-science talk by Will
                * Lunch, probably pizza or sandwiches at Hackerspace
                * Dinner, possible after-party at nearby restaurant
                * Bad Movie Night, virtual only online meetup after dinner
            * Possible Extra Activities for Summer 2025 Conference
                * Mini Job Fair, mock interviews
                    * NEED CREATE JOB POSTING FOR COMMISSION-ONLY RECRUITER/STAFFER JOB MANAGER
                * Mini Investor Pitch
                    * John & Will did not get any reply from investor contacts
                    * NEED JOHN REPLY FROM POSSIBLE INVESTORS

        * Paper Submission Timeline, Winter 2024  [ NO CHANGE 20241212 ]
            * Abstracts submitted by September 30th
            * Abstracts approved or declined by October 4th
            * 1st drafts of papers & posters submitted by November 4th
            * 1st drafts feedback sent back to submitters by November 15th
            * Final drafts submitted by December 2nd
            * Final drafts approved or declined by December 9th

        * Paper Ideas, Winter 2024 & Summer 2025
            * NEED MOVE TO PAPER TRACKER GOOGLE SPREADSHEET
            * Will, Full Talk, PerlGPT Part 2
            * Will, Lightning Talk, Navi AI
            * Brett, 2024 Lightning Talk, 3 Perl Virtues
            * Brett, 2024 Medium Talk, OpenMP Thread Safety
            * Brett, 2024 Medium Talk, Perl Sequential Consistency
            * Brett, 2025 Full Talk, OpenMP Read-Only API for Perl Data Structures
            * Dimitris Kechagias, iOS weather app for astronomers Xasteria, proxy servers it uses for the weather forecasts are in Perl
            * Dr. Russell, 2025 Full Talk, genetic algorithms, currently changing to new job w/out previous job restriction
            * Dr. Russell, 2024 Medium Talk?, Cowl ontology for CPAN
            * Dr. Christos, using Perl to memory management for buffers in other languages such as assembly (lightning talk); also another talk about FOO (20 min talk)
            * John Napiorkowski, 2024 LPW Lightning Talk, Catalyst Valiant To-Do List Application, Beta Demo
            * John Napiorkowski, 2024 Advent & PCC Lightning Talk, Catalyst Valiant To-Do List Application, Full Demo
            * John Napiorkowski, 2025 Advent & PCC Lightning Talk, Catalyst Valiant To-Do List Application, AI Demo
            * Dr. Luis Mochan, 2024 LPW Lightning Talk & PCC Full Talk, Photonic
            * Dr. O'Neil, 2024 PCC Extended Abstract / Lightning Talk (Possible Short Paper), Comparing Machine Learning Clustering Algorithms for Spectrographic Data

        * Editorial Review Subcommittee  [ NO CHANGE 20241212 ]
            * Reviewers
                * Currently reviewers are Dr. Adam Russell, Dr. Casiano Rodriguez Leon, Will Braswell, and Brett Estrade
                * Need more reviewers for 2025, at least 2 per paper
            * OJS
                * Need fix OJS e-mail de-authentication issue, old bug found by Brett & Zaki
                * NEED BRETT & DR. RUSSELL TO REPRODUCE THE BUGS BELOW
                * upload errors, 2 different problems where upload buttons do or do not appear, affecting Manickam & Will & Dr. Russell, full report sent to Brett for further investigation into server logs and GitHub bug reports
            * Editor's Choice Award
                * We will use Dr. Perry's spreadsheet w/ initial selection rubric for Winter 2024 and update as needed
            * Short papers
                * Yes we will accept both short papers (2 - 9 pages) and even extended abstracts (1 page)
                * IEEE guidelines for full-length is 20 to 25 pages, no more than 35 pages including references
                * NEED DECIDE NUMBER OF REFERENCES FOR SHORT PAPERS & EXTENDED ABSTRACTS

        * Proceedings Subcommittee  [ NO CHANGE 20241212 ]
            * Authors get a free printed copy of SPJ, others can purchase a printed copy
            * Science Perl mailing list new members will get free PDF copy of SPJ, others can download PDF with prominent donation button
            * Funding
                * Donation ware online copy
                * Paid physical copies
                * Special signed copies
            * Latex Formatting
                * Brett will create a Latex template for everyone to use in 2025
                * Brett will try to find a way to format the Perl code nicer than it is now
            * Publication
                * We made Conference Preprint Edition in limited run
                * We offered signed copies for sale at higher price
                * NEED FINISH FINAL EDITION
            * Barnes & Noble
                * Now approved for self-publishing nonprofit org account 
                * Price much lower per copy than any other option
            * Need to have committee representative selling SPJ copies at all Perl conferences:
                * TPC::NA
                    * http://tprc.us
                    * 6/??/2025
                    * Need revisit after TPF problems are addressed
                * YAPC::Japan
                    * https://yapcjapan.org
                    * 10/5/2024
                    * George Baugh 2025
                    * NEED JOURNAL COPIES TO SELL
                    * NEED SCIENCE PERL TRACK
                * LPW
                    * https://act.yapc.eu/lpw2024
                    * 10/26/2024
                    * Dr. O'Neil
                    * Science Perl Talks approved
                    * NEED JOURNAL COPIES TO SELL
                * FOSDEM
                    * https://fosdem.org
                    * 2/??/2025
                    * Will & Brett
                    * NEED MONEY FOR TICKET
                    * NEED PERL BOOTH
                    * NEED SUBMIT TALKS
                    * NEED SCIENCE PERL TRACK
                * YAPC::India
                    * NEED CREATE
                * YAPC::Asia (DEFUNCT)
                    * https://yapcasia.org
                    * Last conference 2015
                    * Need revive?
                * YAPC::EU (DEFUNCT)
                    * http://www.yapceurope.org/events/conferences.html
                    * Last Conference 2019
                    * Need revive?
                * YAPC::Russia (DEFUNCT)
                    * https://yapcrussia.org
                    * Last Russian Perl Workshop 2019
                    * Need revive?
                * YAPC::Australia (DEFUNCT)
                    * https://osdc.com.au/
                    * Last Conference 2004?
                    * Need revive?

        * Websites  [ NO CHANGE 20241212 ]
            * Homepage
                * perlcommunity.org/science
                * perlcommunity.org/awards  NEED FINISH
                * NEED UPDATE HACKATHON DATES 7/10 should be 7/17
                * NEED UPDATE 2025 PAPER DATES
            * OJS Subdomain
                * science.perlcommunity.org/spj
                * Updated OJS website with 2025 paper submission timeline dates
                * NEED E-MAIL DEAUTHENTICATION FIX

        * Mailing List  [ NO CHANGE 20241212 ]
            * One-way announcements only
            * Sendy, self-hosted by Brett
            * Request all committee members to register
                * perlcommunity.org/science#mailing_list

    * Science Perl Hackathon
        * Every 6 weeks
        * Last event 11/13/24, 7:00-9:00pm Central time zone, theme was Science Perl Journal Latex & Sales
            * Worked on SPJ Spider Silk latex & tech news sales blurb etc
        * Next event 12/25/24, SPECIAL TIME 7:00-7:30pm Central time zone, theme is CHRISTMAS PARTY!!!

    * Treasurer's Report  [ NO CHANGE 20241212 ]
        * We have enough money for Crossref
        * Need the Fundraising Committee to start collecting donations for 2025 activities
        * NEED PAY PRORATED CROSSREF
        * NEED BRETT RECEIPTS
        * NEED UPDATE SPREADSHEET WITH SPJ SALES & TPC RECEIPTS

    * 2nd Annual Perl Committee Convention, 2025  [ NO CHANGE 20241212 ]
        * July 3rd & July 4th, Austin, TX
        * NEED REVIEW 7/17/24 HACKATHON NOTES FOR 2024 CONVENTION DEBRIEF

    * RoboPerl Nonprofit
        * none

* New Business

    * none

* Closing, 2:42pm
