# Science Perl Committee
# Meeting Agenda & Minutes

## June 13th, 2024
## Twenty-Third Official Meeting

* Opening, 1:12pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Robbie Hatley (EE & computer programming in Perl)
    * Marc Perry, PhD (biologist, used to have wet lab, now doing bioinformatics & genomics using Perl 15 years, Senior Bioinformatician at Genomics Institute of UC Santa Cruz)
    * Christos Argyropoulos, MD, PhD (kidney physician, genomic & biomarker discovery, big data sets, EHR, methodology development for sequencing, new markers for kidney disease)
    * Mickey Macten (online security and streaming multimedia)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 5/30/24 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Robbie Hatley to be accepted as corrected, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of one officer only, not full voting quorum

* Officer Reports
    * Brett reports on the following:
        * All talks have been scheduled, we have a room comprised of 2 sections out of 6, we are in Apollo 1 and 2 all day Tuesday and Wednesday for all Science Track talks, as well as the Perl Committee Convention
        * Each room has a physical camera, Josh Turcotte is in charge of A/V and will have a camera with memory card and projector, we will also record our own talks via OBS
        * Dr. Russell may have issues with his job not allowing his video to be posted, hopefully this won't be a problem since they approved his paper

    * Will reports on the following:
        * Working hard on Perl Committee Convention (see section below)
        * Working on publication (see Proceedings Subcommittee section below)
        * Editor's Choice Award, need Dr. Perry to choose winner (see Editorial Review Subcommittee section below)

* Old Business

    * TPC 2024 Science Track

        * General Next Steps
            * Fix OJS e-mail de-authentication issue
            * Create timeline of each step of the paper submission / review / approval process  [ DONE ]
            * Publish Science CFP on perlcommunity.org/science   [ DONE ]
            * Distribute Science CFP via e-mail, Facebook, Twitter, TPF announcements, etc   [ DONE ]
            * Distribute Science CFP via IRC (Magnet #perl & #pdl, Libera #perl), Reddit, and remaining avenues [ DONE ]
            * Promoting & requesting paper submissions from those on the Paper Idea list [ DONE ]
            * Send TPC Planning Committee our list of approved science papers ASAP, so they can review & coordinate with other track scheduling [ DONE ]
            * Start receiving & reviewing paper submissions in OJS [ ONGOING ]
            * Finish reviewing papers in OJS 

        * Remote Presentations
            * Received official approval for Dr. Christos remote talk 2024
            * Did not attempt to get TPF approval for Dr. O'Neil remote talk 2024, will keep that as emergency backup plan only, may be likely
            * TPF won't approve remote attendance because it will cause the already-too-low number of physical attendees and the conference will fail

        * Paper Submission Timeline
            * Abstracts submitted by April 5th  [ DONE ]
            * Abstracts approved or declined by April 15th  [ DONE ]
            * Extended CFP, abstracts submitted by April 20th  [ DONE ]
            * Extended CFP, abstracts approved or declined by April 30th  [ DONE ]
            * Extended 1st drafts of papers & posters submitted by May 20th  [ DONE ]
            * 1st drafts feedback sent back to submitters by May 31st [ DONE ]
            * Final drafts submitted by June 7th [ PUSHED ]
            * Final drafts approved or declined by June 15th

        * Call For Papers
            * 2024 CFP now closed
            * 2025 CFP will be announced during 2024 conference to give a whole year

        * Editorial Review Subcommittee
            * NEED BYLAW, delay any formal motion to officially create a subcommittee until in-person convention
            * NEED UPDATE OJS & WEBSITE, submitters must create their presentation slides to match their paper or poster content
            * Reviewers
                * Currently reviewers are Brett Estrade, Will Braswell, Dr. Marc Perry, and Dr. Adam Russell
                * One reviewer is okay if we're not rejecting papers for 2024
                * Next year we want more reviewers for 2025, hopefully Dr. Christos & Dr. Russell
                * Also invited old PDL authors Craig DeForest & Karl Glazebrook to be reviewers for 2025
            * OJS upload errors, 2 different problems where upload buttons do or do not appear, affecting Manickam & Will & Dr. Russell, full report sent to Brett for further investigation into server logs and GitHub bug reports
            * Editor's Choice Award
                * For 2024 it will be just Dr. Perry choosing the winner, the rest of us are authors
                * Need to write a review (or letter of recommendation, or award choice) that lets you sleep at night
                * Don't make it too negative or too positive, use the same concept when choosing award winners
                * Need award selection rubric for following years
                * Need involve entire Editorial Review Subcommittee next year, hopefully not everyone will also be an author again
            * Short papers
                * Dr. Christos suggests we allow short papers next year
                * IEEE guidelines is 20 to 25 pages, no more than 35 pages including references
                * Need to make our own guidelines for short papers?

        * Proceedings Subcommittee
            * NEED CREATE PLAN FOR PRINTING BEFORE TPC
            * NEED BYLAW, same as Editorial Review Subcommittee above
            * Current authors will be offered a printed copy of SPJ at cost
            * Non-authors will be offered a printed copy of SPJ at a fair market mark-up as part of our annual fundraising, to finance printing next year
            * Those who sign up for Science Perl mailing list will get free PDF copy of SPJ 
            * Put PDF copy of SPJ online for free download, with prominent donate button for anyone who is not a student, works based on the honor system
            * Funding
                * Donation ware online copy
                * Paid physical copies
                * Special signed copies
            * Latex Formatting
                * Brett will make the final Latex edits
                * Single column, default format, only change is decrease margins
                * Want to upload everyone's Latex source to OJS as well as PDFs
            * Publication
                * Pefect binding is okay, no need for spiral
                * We will need to make Conference Preprint Edition in limited run, then fully finalized edition after all papers are pristine
                * Offer signed copies for sale at higher price
            * Comments made during the conference talks can optionally be incorporated into the papers
                * This is how the old academic groups like The Royal Society worked
            * Announce that the final edition will be released in August or September with all edits and accepted comments
            * Need to have committee representative selling SPJ copies at all Perl conferences:
                * TPC NA
                * YAPC EU
                * YAPC Asia
                * LPW
                * FOSDEM

        * Websites
            * Homepage
                * perlcommunity.org/science
                * NEED UPDATE WEBSITE WITH 2025 PAPER SUBMISSION TIMELINE DATES
            * OJS Subdomain
                * science.perlcommunity.org/spj
                * NEED E-MAIL DEAUTHENTICATION FIX

        * Mailing List  [ NO CHANGE 20240613 ]
            * One-way announcements only
            * Sendy, self-hosted by Brett
            * Request all committee members to register
                * perlcommunity.org/science#mailing_list

        * Paper Ideas  [ NO CHANGE 20240613 ]
            * none

    * 1st Annual Perl Committee (Constitutional!) Convention
        * NEED DRAFT BYLAWS
        * During TPC in Las Vegas, Tuesday 6/25/2024, 7-9pm PACIFIC TIME ZONE
        * Hybrid event, both in-person and online
        * Need to create & ratify bylaws
        * Celebrating with awards and pizza!
        * Give some gift of thanks to TPC staff, maybe signed SPJ copies?
        * Suggested Officers
            * Chairman: Brett Estrade
            * 1st Vice: Dr. Marc Perry
            * 2nd Vice: Dr. Andrew O'Neil
            * Secretary & Treasurer: Will Braswell

    * RoboPerl Nonprofit
        * NEED APPROVE CHARTERS by Board of Directors

    * Science Perl Hackathon
        * Every 6 weeks
        * Last event 5/29/24, 7-9pm Central time zone, theme was Final Paper Approval
        * Next event 7/17/24, SPECIAL DATE, 7:00-9:00pm Central time zone, theme is Science Track 2024 Debrief

    * Treasurer's Report
        * We are continuing to take in donations and expend funds for official Science Perl purposes
        * An updated accounting report will be available after the conference

* New Business

    * Marketing Subcommittee
        * Dr. Christos as potential subcommittee chair
        * Social Media
            * Facebook
            * Twitter
            * Mastodon
            * etc
        * Need workflow for jnap to build Catalyst software which can automate social media marketing redistribution

* Closing, 2:29pm
