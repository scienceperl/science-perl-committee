# Science Perl Committee
# Meeting Agenda & Minutes

## February 20th, 2025
## Thirty-Ninth Official Meeting

* Opening, 1:18pm

* Attendance & Introductions
    * Brett Estrade (high-performance computing)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Mickey Macten (online security and streaming multimedia)
    * Joshua Oliva (interested in scientific breakthroughs, physics, health topics)
    * Mohammed Zia (uses Perl for regular expressions etc)
    * Andrew Mehta (Perl programmer)

* Announcements
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 2/6/25 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Mohammed Zia to be accepted as read, voted unanimously
    * NEED SPECIAL BYLAW to allow meeting minutes approval with minimum of one officer only, not full voting quorum

* Officer Reports
    * Brett reports on the following:
        * Successfully merged list of papers from 2/6/2025 meeting minutes into spreadsheet
            * Updated as of today
            * Need contact Dimitris K. about weather application paper
            * NEED REMOVE DUPLICATES
        * Waiting on papers from Will, John, Dr. O'Neil
            * Already received 1 paper from Will, State of the Noonien Address
            * Will & John are working on their remaining papers now
            * NEED CONTACT Dr. O'Neil to confirm paper type
        * Paper Author Mentorship Program
            * Lead by Brett, assisted by Dr. Christos & Dr. Russell
            * Brett wants to work w/ Dr. Linderman on quicksort paper
            * Brett wants to work w/ Erki Ferenc on Perl Rex paper
                * Beowulf cluster experiment using Rex, ADCIRC storm surge model
            * Dr. Russell wants to work w/ Bartosz Jarzyna on Perl bitcoin paper
            * Dr. Christos wants to work w/ Michael Conrad on Perl assembly paper
        * Overleaf
            * NEED CONTACT TECH SUPPORT, REQUEST ABSOLUTE FILE PATHS
        * Papercall.io
            * STILL NEED check if we can create a new conference account for PCC Summer 2025 CFP
        * OJS issue uploading PDF, Dr. Christos unable to upload?
            * STILL NEED FOLLOW UP W/ DR. CHRISTOS
            * Brett will upload PDFs from Overleaf to OJS for now, until bug is fixed etc
        * Mohammed Zia did post flyers for SPJ B&N purchase link at SUNY Buffalo campus in at least one location
            * Posted grocery shop visited by students close to SUNY, stays for approximately 1 month
            * Unable to post any flyers on SUNY campus

    * Dr. O'Neil reports on the following:
        * no report available

    * Will reports on the following:
        * SPJ Authorship Policies moved by Will Braswell, seconded by Brett Estrade, motion passed 4 to 1
            * Ghost writing, Guest writing, Gift writing are forbidden
            * Editors & Reviewers are allowed to use AI w/out restriction
            * Authors are allowed to use AI generated source code w/out restriction
            * Authors must write first & last drafts of paper manually
            * Authors are allowed to use AI for intermediate editing only, and must disclose AI usage to editors
            * Authors must formally accept these policies in writing
            * Non-sentient AI are not allowed to be listed as co-authors
            * Our goal is to innovate, and not spend our time on non-innovation work such as grammar, editing, formatting, writing code examples, etc.
        * PCC Summer 2025
            * AI & Science Reception, Evening of July 3rd (and December 17th for PCC Winter 2025)
                * Currently working on invitations and flyers
                * Need help finding famous Perl people
                    * NEED Brett ask Michael Conrad to bring Perl Delorean
                * Need help finding donors
        * PPA, Perl Job Service
            * Starting work on ShinyCMS Docker currently
            * Then, ShinyCMS release on CPAN
        * SPJ issue #2, working on 1 paper (Perl::Types)
        * SPJ publication
            * SPJ issue #1 black & white edition, NEED FINISH
        * NEED FINISH Crossref DOI registration, $275 per year pro-rated
            * STILL WAITING FOR BARNES & NOBLE DEPOSITS to have enough money
            * Have login account in order to pay them
        * Still removing all undesirable logos
            * Need to register the trademarks for all our logos

* Old Business

    * Science Track

        * General Next Steps  [ NO CHANGE 20250220 ]
            * Create timeline of each step of the paper submission / review / approval process  [ DONE ]
            * Publish Science CFP on perlcommunity.org/science   [ DONE ]
            * Distribute Science CFP via e-mail, Facebook, Twitter, TPF announcements, etc   [ DONE ]
            * Distribute Science CFP via IRC (Magnet #perl & #pdl, Libera #perl), Reddit, and remaining avenues [ DONE ]
            * Promoting & requesting paper submissions from those on the Paper Idea list [ DONE ]
            * Start receiving & reviewing paper submissions in OJS [ DONE ]
            * Finish reviewing papers in OJS
            * Publish preprint edition
            * Finalizing papers for SPJ full edition
            * Publish SPJ full edition

        * Perl Community Conference, Summer 2025  [ NO CHANGE 20250220 ]
            * HYBRID IN-PERSON & VIRTUAL CONFERENCE
            * Registration not yet open
                * NEED MEETUP EVENT LINK
                * NEED FACEBOOK EVENT LINK
            * USA's 249th Birthday 7/3/25 - 7/4/25
            * NEED NAME as Conference MC
            * One track only, one speaker at a time
            * NEED TICKET PRICE
                * Accepting donations via Meetup PayPal
                * For those that Meetup PayPal doesn't work, we can accept payment via Square, Venmo, PayPal
            * Use Google Meet w/ maximum 100 participants, ask for feedback after conference
            * Allow 3 different talk lengths, corresponding to 3 different paper lengths
                * Full-length talks, 50 minutes
                * Short-length talks, 20 minutes
                * Lightning talks, 5 minutes
            * Use Papercall.io as registration for Normal Track conference presenters
            * Use OJS as registration for Science Track conference presenters
            * Use Meetup & Facebook as registration for conference attendees
            * Use existing mailing list to send info to conference attendees
            * Extra Activities for Summer 2025 Conference
                * AI & Science Reception, July 3rd evening
                * Lunch, probably pizza or sandwiches at Hackerspace
                * Dinner, possible after-party at nearby restaurant
                * Bad Movie Night, virtual only online meetup after dinner
                * Fireworks Family Fun, July 4th evening
            * Possible Extra Activities for Summer 2025 Conference
                * Mini Job Fair
                * Mini Investor Pitch?
            * NEED REVIEW 7/17/24 HACKATHON NOTES FOR 2024 CONVENTION DEBRIEF

        * Paper Submission Timeline, Summer 2025  [ NO CHANGE 20250220 ]
            * Abstracts submitted by NEED DATE
            * Abstracts approved or declined by NEED DATE
            * 1st drafts of papers & posters submitted by NEED DATE
            * 1st drafts feedback sent back to submitters by NEED DATE
            * Final drafts submitted by NEED DATE
            * Final drafts approved or declined by NEED DATE

        * Paper Ideas, Winter 2024 & Summer 2025
            * ONLY USE PAPER TRACKER GOOGLE SPREADSHEET

        * Editorial Review Subcommittee  [ NO CHANGE 20250220 ]
            * Reviewers
                * Currently reviewers are Dr. Adam Russell, Dr. Casiano Rodriguez Leon, Will Braswell, and Brett Estrade
                * Need more reviewers for 2025, at least 2 per paper
            * OJS
                * Need fix OJS e-mail de-authentication issue, old bug found by Brett & Zaki
                * NEED BRETT & DR. RUSSELL TO REPRODUCE THE BUGS BELOW
                * upload errors, 2 different problems where upload buttons do or do not appear, affecting Manickam & Will & Dr. Russell, full report sent to Brett for further investigation into server logs and GitHub bug reports
            * Editor's Choice Award
                * We will use Dr. Perry's spreadsheet w/ initial selection rubric for Winter 2024 and update as needed
            * Short papers
                * Yes we will accept both short papers (2 - 9 pages) and even extended abstracts (1 page)
                * IEEE guidelines for full-length is 20 to 25 pages, no more than 35 pages including references
                * NEED DECIDE NUMBER OF REFERENCES FOR SHORT PAPERS & EXTENDED ABSTRACTS

        * Proceedings Subcommittee  [ NO CHANGE 20250220 ]
            * Authors get a free printed copy of SPJ, others can purchase a printed copy
            * Science Perl mailing list new members will get free PDF copy of SPJ, others can download PDF with prominent donation button
            * Funding
                * Donation ware online copy
                * Paid physical copies
                * Special signed copies
            * Latex Formatting
                * Brett will create a Latex template for everyone to use in 2025
                * Brett will try to find a way to format the Perl code nicer than it is now
            * Publication
                * We made Conference Preprint Edition in limited run
                * We offered signed copies for sale at higher price
                * NEED FINISH FINAL EDITION
            * Barnes & Noble
                * Now approved for self-publishing nonprofit org account 
                * Price much lower per copy than any other option
            * Need to have committee representative selling SPJ copies at all Perl conferences:
                * TPC::NA
                    * http://tprc.us
                    * 6/??/2025
                    * Need revisit after TPF problems are addressed
                * YAPC::Japan
                    * https://yapcjapan.org
                    * 10/5/2024
                    * George Baugh 2025
                    * NEED JOURNAL COPIES TO SELL
                    * NEED SCIENCE PERL TRACK
                * LPW
                    * https://act.yapc.eu/lpw2024
                    * 10/26/2024
                    * Dr. O'Neil
                    * Science Perl Talks approved
                    * NEED JOURNAL COPIES TO SELL
                * FOSDEM
                    * https://fosdem.org
                    * 2/??/2025
                    * Will & Brett
                    * NEED MONEY FOR TICKET
                    * NEED PERL BOOTH
                    * NEED SUBMIT TALKS
                    * NEED SCIENCE PERL TRACK
                * YAPC::India
                    * NEED CREATE
                * YAPC::Asia (DEFUNCT)
                    * https://yapcasia.org
                    * Last conference 2015
                    * Need revive?
                * YAPC::EU (DEFUNCT)
                    * http://www.yapceurope.org/events/conferences.html
                    * Last Conference 2019
                    * Need revive?
                * YAPC::Russia (DEFUNCT)
                    * https://yapcrussia.org
                    * Last Russian Perl Workshop 2019
                    * Need revive?
                * YAPC::Australia (DEFUNCT)
                    * https://osdc.com.au/
                    * Last Conference 2004?
                    * Need revive?

        * Websites  [ NO CHANGE 20250220 ]
            * Homepage
                * perlcommunity.org/science
                * perlcommunity.org/awards  NEED FINISH
                * NEED UPDATE HACKATHON DATES 7/10 should be 7/17
                * NEED UPDATE 2025 PAPER DATES
            * OJS Subdomain
                * science.perlcommunity.org/spj
                * Updated OJS website with 2025 paper submission timeline dates
                * NEED E-MAIL DEAUTHENTICATION FIX

        * Mailing List  [ NO CHANGE 20250220 ]
            * One-way announcements only
            * Sendy, self-hosted by Brett
            * Request all committee members to register
                * perlcommunity.org/science#mailing_list

    * Science Perl Hackathon  [ NO CHANGE 20250220 ]
        * Every 6 weeks
        * Last event 2/5/25, 7:00-9:00pm Central time zone, theme is PAPER REVIEW
            * Worked on Overleaf margins and SPJ #1 black & white edition
        * Next event 3/19/25, 7:00-9:00pm Central time zone, theme is PAPER REVIEW

    * Treasurer's Report  [ NO CHANGE 20250220 ]
        * We have enough money for Crossref
        * Need the Fundraising Committee to start collecting donations for 2025 activities
        * NEED PAY PRORATED CROSSREF
        * NEED BRETT RECEIPTS
        * NEED UPDATE SPREADSHEET WITH SPJ SALES & TPC RECEIPTS

    * RoboPerl Nonprofit
        * none

* New Business

    * none

* Closing, 2:31pm
