# Science Perl Committee
# Meeting Agenda & Minutes

## November 30th, 2023
## Ninth Official Meeting

* Opening, 1:03pm

* Attendance & Introductions
    * Zakariyya Mughal (BioPerl, PDL, previously biomedical imaging, currently computational toxicology)
    * Will Braswell (computer scientist, creator of Navi AI)
    * Boyd Duffee (starting to use PDL, wants to create Perl Data Science blog & Data Science track at London Perl Workshop, Astro::Constants & Astro::ADS)
    * Jakub Pawlowski (no introduction, microphone issues)

* Announcements
    * Chairman Brett Estrade requests 1st Vice Chairman Zaki Mughal to preside
    * Meet every 2 weeks on Thursday 1:00-1:45pm Central time zone
        * Immediately followed by Perl::Types Committee and AI Perl Committee meetings

* Previous Meeting's Minutes
    * 11/16/23 meeting minutes
    * Read by Acting Secretary Will Braswell
    * Moved by Will Braswell & seconded by Zaki Mughal to be accepted as read, voted unanimously

* Officer Reports
    * Zaki reports on the following:
        * Need to set up local copy of OJS and start experimenting so we can be ready
        * Need to bug TPF for official Science Track approval
    * Will reports on recruiting & thanks Boyd Duffee for joining us

* Old Business

    * TPC 2024 Science Track

        * TPF & TPC Science Track Proposal report
            * TPF approval is pending final consideration

            * Zaki read the following report by Chairman Brett Estrade:
                * I attended the TPRF planning meeting for the 2024 TPRC. A lot of time was spent discussing the survey, which I think did its job of convincing everyone that the Science Track is going to bring in new folks and not take people away from other tracks.
                Todd Rinaldo did some back-of-the-envelope stats and determined that based on comparing questions #1 and #4, there are at least 7 individuals who would be first time attendees to the conference and also submit a Science Track paper.  I think that was enough to convince him. Peter and Bruce were also very positive about it to the point of discussing how to organize the call for papers with this new thing in mind. We also discussed what would happen if someone got rejected from the paper submission; the conclusion is that if it is sufficiently Perl or Raku related, they will be offered a spot on the regular tracks.
                Peter said he'd get in touch with me in the next couple of days with a decision and, if they decided yes, the next set of steps. It looks like all submissions are going to go through Paper Call; but they would add "paper" or "science" and "science poster" as additional types. For the science (or "academic" as they were calling it), they'd be passed on to our review committee. I told them Zaki's been leading the charge on finding a good software platform to manage reviews. I also confirmed that papers will be ushered along via a series of deadlines and that if someone falls behind, they will be eventually cut if they don't deliver.
                Peter called this "preventing 'Ingy-itis'" lol. It assured them that we will not be going into the converence worrying if all the speakers will have their paper done.

        * Call For Papers
            * Everyone should keep working on paper ideas
            * CFP will be opened when TPF gives official approval

        * Survey
            * Received 54 responses, survey now closed
            * We reviewed the responses, including the 18 specific paper ideas and lots of other specific feedback
            * We will probably recommend the Science Track is held during only 2 days of the conference
            
        * Editorial Review Subcommittee
            * We will have to use TPF's PaperCall.io platform, which will have a tag for "academic" or "science" etc
            * Those tagged papers will then be submitted through OJS in addition to already being submitted through PaperCall
            * If a paper is rejected by the Science track, then it can still be accepted to the normal Perl track

        * Proceedings
            * none

        * Websites
            * Will Braswell will update perlcommunity.org after TPF approves the Science Track

        * Paper Ideas
            * none

    * Science Perl Hackathon
        * Every 6 weeks
        * Next event 12/13/23, 7-9pm Central time zone, theme is Call for Papers & Editorial Review Subcommittee

* New Business

    * How to compete against R
        * Has very nice graphics libraries for visualization using ggplot2
            * Chart::GGPlot by Stephan Lloyd, last updated February 2023
                * https://metacpan.org/pod/Chart::GGPlot
            * Need check documentation
            * Need make a nice website for it
            * Need find the missing features & continue implementing
            * Need equivalent to ggcharts
                * https://thomas-neitmann.github.io/ggcharts/index.html
            * Need equivalent to R graph gallery
                * https://r-graph-gallery.com/ggplot2-package.html
        * Not good for OO, too many different ways
        * Good for functional coding
        * Has CRAN, similar to CPAN
        * Has Bioconductor, similar to BioPerl
        * Has tidyverse, closely designed set of packages
        * Data::Frame is a container, need during conversion of R data frame to Perl Data::Frame
            * https://metacpan.org/pod/Data::Frame
        * Statistics::NiceR
            * https://metacpan.org/pod/Statistics::NiceR
            * Need update to include "zero copy" to make sure it uses the same pieces of memory, not copying R data into Perl memory, use the R data as-is in place, gets tricky because we have to know if R or Perl will free the data, when and how etc

    * How to compete against Python
        * Need pandas
        * Need matplotlib

* Closing, 2:37pm
